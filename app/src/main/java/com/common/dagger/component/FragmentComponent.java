package com.common.dagger.component;

import android.app.Activity;

import com.utilCode.dagger.module.FragmentModule;
import com.utilCode.dagger.scope.FragmentScope;
import com.wxh.worker.ui.fragment.home.HomeFragment;
import com.wxh.worker.ui.fragment.me.MeFragment;
import com.wxh.worker.ui.fragment.message.MessageFragment;
import com.wxh.worker.ui.fragment.order.OrderFragment;

import dagger.Component;

@FragmentScope
@Component(dependencies = MyAppComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {

    Activity getActivity();

     void inject(HomeFragment homeFragment);

     void inject(MeFragment meFragment);

     void inject(MessageFragment messageFragment);

     void inject(OrderFragment orderFragment);
}
