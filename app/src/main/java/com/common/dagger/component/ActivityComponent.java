package com.common.dagger.component;

import android.app.Activity;

import com.common.debug.DebugInfoActivity;
import com.utilCode.dagger.module.ActivityModule;
import com.utilCode.dagger.scope.ActivityScope;
import com.wxh.worker.city_select.CitySelectActivity;
import com.wxh.worker.common_view.imgoriginal.OriginalPagerActivity;
import com.wxh.worker.ui.HomeActivity;
import com.wxh.worker.ui.MainActivity;
import com.wxh.worker.ui.checkup.CheckAcceptanceActivity;
import com.wxh.worker.ui.login.FindPasswordActivity;
import com.wxh.worker.ui.login.LoginActivity;
import com.wxh.worker.ui.login.PerfectMessageActivity;
import com.wxh.worker.ui.login.RegisterActivity;
import com.wxh.worker.ui.login.StartActivity;
import com.wxh.worker.ui.me.PersonalDataActivity;
import com.wxh.worker.ui.measure.MeasureAddDataActivity;
import com.wxh.worker.ui.measure.MeasureDetailActivity;
import com.wxh.worker.ui.measure.MeasureListActivity;
import com.wxh.worker.ui.message.MessageDetailActivity;
import com.wxh.worker.ui.money.CashWithdrawalActivity;
import com.wxh.worker.ui.money.PersonalBondActivity;
import com.wxh.worker.ui.order.OrderDetailActivity;
import com.wxh.worker.ui.order.OrderListActivity;
import com.wxh.worker.ui.pagelist.PageByIdListsActivity;
import com.wxh.worker.ui.pagelist.PageByPageListsActivity;
import com.wxh.worker.ui.setting.SettingActivity;
import com.wxh.worker.ui.setting.about.AboutActivity;
import com.wxh.worker.ui.setting.contact.ContactActivity;
import com.wxh.worker.ui.setting.opinion.FeedBackActivity;
import com.wxh.worker.ui.window_type.WindowTypeActivity;

import dagger.Component;

@ActivityScope
@Component(dependencies = MyAppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    Activity getActivity();

    void inject(DebugInfoActivity debugInfoActivity);

    void inject(MainActivity mainActivity);

    void inject(PageByIdListsActivity pageByIdListsActivity);

    void inject(PageByPageListsActivity pageByPageListsActivity);

    void inject(LoginActivity loginActivity);

    void inject(FindPasswordActivity findPasswordActivity);

    void inject(PerfectMessageActivity perfectMessageActivity);

    void inject(RegisterActivity registerActivity);

    void inject(StartActivity startActivity);

    void inject(HomeActivity homeActivity);

    void inject(CitySelectActivity homeActivity);

    void inject(OriginalPagerActivity originalPagerActivity);

    void inject(OrderDetailActivity orderDetailActivity);

    void inject(CheckAcceptanceActivity checkAcceptanceActivity);

    void inject(PersonalDataActivity personalDataActivity);

    void inject(MeasureAddDataActivity measureAddDataActivity);

    void inject(MeasureDetailActivity measureDetailActivity);

    void inject(MeasureListActivity measureListActivity);

    void inject(CashWithdrawalActivity cashWithdrawalActivity);

    void inject(PersonalBondActivity personalBondActivity);

    void inject(SettingActivity settingActivity);

    void inject(WindowTypeActivity windowTypeActivity);

    void inject(MessageDetailActivity messageDetailActivity);

    void inject(AboutActivity aboutActivity);

    void inject(ContactActivity contactActivity);

    void inject(OrderListActivity orderListActivity);

    void inject(FeedBackActivity feedBackActivity);
}
