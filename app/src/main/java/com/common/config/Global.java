package com.common.config;

import com.common.retrofit.MyOkhttpHelper;
import com.orhanobut.hawk.Hawk;
import com.utilCode.dagger.help.PreferencesHelper;
import com.wxh.worker.json.MasterInfoBean;
import com.wxh.worker.json.MeasureListBean;
import com.wxh.worker.json.UserInfoBean;

public class Global {

    public static String PREFERENCE_KEY_FIRST_USING = "first_using";
    public static String PREFERENCE_KEY_USER_PHONE = "user_phone";
    public static String PREFERENCE_KEY_USER_INFO = "user_info";
    public static String PREFERENCE_KEY_MEASURE_LIST = "measure_list";
    public static String PREFERENCE_KEY_MASTER_INFO = "master_info";

    public static boolean getFirstUsing() {
        return PreferencesHelper.get(PREFERENCE_KEY_FIRST_USING, false);
    }

    public static void setFirstUsing(boolean firstusing) {
        PreferencesHelper.put(PREFERENCE_KEY_FIRST_USING, firstusing);
    }

    public static String getUserPhotoNumber() {
        return PreferencesHelper.get(PREFERENCE_KEY_USER_PHONE, "");
    }

    public static void setUserPhotoNumber(String phone) {
        PreferencesHelper.put(PREFERENCE_KEY_USER_PHONE, phone);
    }

    public static UserInfoBean getUserInfoBean() {
        return Hawk.get(PREFERENCE_KEY_USER_INFO, new UserInfoBean());
    }

    public static void setUserInfoBean(UserInfoBean userInfoBean) {
        Hawk.put(PREFERENCE_KEY_USER_INFO, userInfoBean);
    }

    public static MeasureListBean getMeasureList() {
        return Hawk.get(PREFERENCE_KEY_MEASURE_LIST, new MeasureListBean());
    }

    public static void setMeasureList(MeasureListBean userInfoBean) {
        Hawk.put(PREFERENCE_KEY_MEASURE_LIST, userInfoBean);
    }

    public static MasterInfoBean getMasterInfoBean() {
        return Hawk.get(PREFERENCE_KEY_MASTER_INFO, new MasterInfoBean());
    }

    public static void setMasterInfoBean(MasterInfoBean masterInfoBean) {
        Hawk.put(PREFERENCE_KEY_MASTER_INFO, masterInfoBean);
    }

    public static void loginOutclear() {
        MyOkhttpHelper.clearCookie();
        setUserPhotoNumber("");
        setUserInfoBean(new UserInfoBean());
        setMeasureList(new MeasureListBean());
        setMasterInfoBean(new MasterInfoBean());
    }

}
