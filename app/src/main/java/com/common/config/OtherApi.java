package com.common.config;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface OtherApi {

    @Multipart
    @POST("api/v1/file/upload")
    Observable<ResponseBody> uploadFileWithPartMap(@Part("type") RequestBody type, @Part MultipartBody.Part file);
}
