package com.common.config;

import android.support.annotation.Keep;

@Keep
public class Constant {

    /**
     * api status code
     */
    public static final int WEB_SUCCESS = 200;

    public static final String SUCCESS_LOGOUT = "000000";
    /**
     * 登录超时错误码
     */
    public static final String ERROR_LOGOUT = "0000301";
    public static final String ERROR_TOKEN = "0000302";

    /**
     * long time
     */
    public static final long TIME_ONE_SECOND = 1000;
    public static final long TIME_ONE_MINITUE = 60000;
    public static final long TIME_ONE_HOUR = 3600000;

    public static final int VALIDETE_CODE_WATING_TIME_MAX = 60;

    public static final String PUSH_TYPE_TEXT = "01";
    public static final String PUSH_TYPE_URL = "02";
    public static final String PUSH_TYPE_ACTIVITY = "03";

    public static String ANDROIDVERSION = "ANDROID_CRM_V2_VERSION";

}