package com.common.config;

import com.wxh.worker.city_select.bean.CityBean;
import com.wxh.worker.city_select.bean.DistrictBean;
import com.wxh.worker.city_select.bean.ProvincesBean;
import com.wxh.worker.json.ADBean;
import com.wxh.worker.json.AndroidGitLib;
import com.wxh.worker.json.CashWithDrawalBean;
import com.wxh.worker.json.CheckUpdate;
import com.wxh.worker.json.GitUser;
import com.common.retrofit.json.Data;
import com.wxh.worker.json.HomeOrderBean;
import com.wxh.worker.json.MasterInfoBean;
import com.wxh.worker.json.MeasureListBean;
import com.wxh.worker.json.OrderDetailBean;
import com.wxh.worker.json.OrderListBean;
import com.wxh.worker.json.UserInfoBean;
import com.wxh.worker.ui.fragment.message.bean.MessageBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface MyApi {

    @FormUrlEncoded
    @POST("v1/trade/version/getVersionInfo")
    Observable<Data<CheckUpdate>> checkUpdate(@FieldMap Map<String, String> updateMap);

    @GET("https://api.github.com/users")
    Observable<List<GitUser>> getUsersPagesById(@Query("since") long userId, @Query("per_page") int perPage);

    @GET("https://api.github.com/search/repositories")
    Observable<AndroidGitLib> getUsersPagesByPage(@Query("q") String keyword, @Query("page") int page, @Query("per_page") int perPage);


    /**
     * 师傅广告位获取接口
     *
     * @return
     */
    @GET("api/v1/master/adPositionLists")
    Observable<Data<List<ADBean>>> adPositionLists();

    /**
     * 师傅获取版本信息
     *
     * @return
     */
    @GET("api/v1/master/getVersion")
    Observable<Data> getVersion();

    /**
     * 师傅意见反馈
     * @return
     */
    @FormUrlEncoded
    @POST("api/v1/master/feedbackMessages")
    Observable<Data> feedbackMessages(@Field("message") String message, @Field("type") String type);

    /**
     * 删除图片
     *
     * @return
     */
    @FormUrlEncoded
    @POST("api/v1/file/delete")
    Observable<Data> deleteImg(@Field("fileName") String fileName);

    /**
     * 省份查询接口
     *
     * @return
     */
    @GET("api/v1/common/provinces")
    Observable<Data<List<ProvincesBean>>> getProvinces();

    /**
     * 城市查询接口
     *
     * @return
     */
    @GET("api/v1/common/city")
    Observable<Data<List<CityBean>>> getCity(@Query("provinceId") String provinceId);

    /**
     * 区域查询接口
     *
     * @return
     */
    @GET("api/v1/common/district")
    Observable<Data<List<DistrictBean>>> getDistrict(@Query("cityId") String cityId);

    /**
     * 短信验证码接口
     *
     * @param phone
     * @return
     */
    @GET("api/v1/common/sendOut/sms")
    Observable<Data> sendSms(@Query("codePic") String codePic, @Query("phone") String phone);

    /**
     * 师傅短信登陆接口
     *
     * @param phone
     * @return
     */
    @GET("/api/v1/master/getSms")
    Observable<Data> sendSms(@Query("phone") String phone);

    /**
     *
     * 师傅申请订单验收-发送短信
     *
     * @param orderId
     * @return
     */
    @GET("/api/v1/master/order/applyAcceptance/sms")
    Observable<Data> sendAcceptanceSms(@Query("orderId") String orderId);

    /**
     * 师傅注册
     *
     * @return
     */
    @FormUrlEncoded
    @POST("api/v1/sys/register")
    Observable<Data> postRegister(@FieldMap Map<String, String> paramMap);

    /**
     * 登录
     *
     * @param phone
     * @param pwd
     * @return
     */
    @GET("api/v1/sys/backstage/loginOn")
    Observable<Data<UserInfoBean>> getLoginOn(@Query("params") String phone, @Query("pwd") String pwd);

    /**
     * 师傅短信登陆接口
     *
     * @param phone
     * @param smsCode
     * @return
     */
    @GET("api/v1/master/land")
    Observable<Data<UserInfoBean>> getLandLogin(@Query("phone") String phone, @Query("smsCode") String smsCode);

    /**
     * 师傅短信登陆-注册接口
     *
     * @param phone
     * @param smsCode
     * @return
     */
    @GET("api/v1/master/landAndRegister")
    Observable<Data<UserInfoBean>> getLandAndRegister(@Query("phone") String phone, @Query("picturesCode") String picturesCode, @Query("smsCode") String smsCode);

    /**
     * 图形验证码接口
     *
     * @return
     */
    @GET("api/v1/common/codePic/generate")
    Observable<Data> getGenerate();

    /**
     * 师傅找回密码(修改密码)
     *
     * @return
     */
    @POST("api/v1/master/retrievePwd")
    Observable<Data<UserInfoBean>> getRetrievePwd(@Body RequestBody requestBody);

    /**
     * 师傅-我的资料
     *
     * @return
     */
    @GET("api/v1/master/getMasterInfo")
    Observable<Data<MasterInfoBean>> getMasterInfo();

    /**
     * 完善师傅个人资料
     *
     * @return
     */
    @POST("api/v1/master/perfectMessages")
    Observable<Data> postPerfectMessages(@Body RequestBody requestBody);


    /**
     * 师傅订单查询
     *
     * @return
     */
    @GET("api/v1/master/order/listOrders")
    Observable<Data<List<OrderListBean>>> getListOrders(@Query("orderStatus") String orderStatus, @Query("pageNum") int pageNum, @Query("pageSize") int pageSize);


    /**
     * 师傅首页的服务订单数量
     *
     * @param paramMap
     * @return
     */
    @GET("api/v1/master/order/count")
    Observable<Data<List<HomeOrderBean>>> getOrderCount(@QueryMap Map<String, String> paramMap);

    /**
     * 师傅获取消息接口
     *
     * @return
     */
    @GET("api/v1/master/newsLists")
    Observable<Data<List<MessageBean>>> getNewsLists();

    /**
     * 师傅获取消息接口
     *
     * @return
     */
    @FormUrlEncoded
    @POST("api/v1/master/updateNews")
    Observable<Data<List<MessageBean>>> updateNews(@Field("newsId") String newsId);

    /**
     * 师傅订单详情
     *
     * @param orderId
     * @return
     */
    @GET("api/v1/master/order/details")
    Observable<Data<OrderDetailBean>> getOrdersDetails(@Query("orderId") String orderId);


    /**
     * 师傅接受订单
     *
     * @return
     */
    @FormUrlEncoded
    @POST("api/v1/master/order/accept")
    Observable<Data> acceptOrder(@Field("orderId") String orderId);

    /**
     * 师傅拒绝订单
     *
     * @return
     */
    @FormUrlEncoded
    @POST("api/v1/master/order/refuse")
    Observable<Data> refuseOrder(@Field("orderId") String orderId);

    /**
     * 师傅填写订单预约信息(包括变更预约时间)
     *
     * @return
     */
    @POST("api/v1/master/order/appointment")
    Observable<Data> postAppointment(@Body RequestBody requestBody);

    /**
     * 查询订单测量数据
     *
     * @param orderId
     * @return
     */
    @GET("api/v1/master/order/queryMeasureRecord")
    Observable<Data<List<MeasureListBean>>> getQueryMeasureRecord(@Query("orderId") String orderId);

    /**
     * 查询订单具体房间的测量数据
     *
     * @return
     */
    @GET("api/v1/master/order/querySingleMeasureRecord")
    Observable<Data> getQuerySingleMeasureRecord(@Query("id") String id);

    /**
     * 删除测量数据
     *
     * @return
     */
    @FormUrlEncoded
    @POST("api/v1/master/order/delMeasureRecord")
    Observable<Data> delMeasureRecord(@Field("id") String id);

    /**
     * 师傅申请订单验收-(填写验收码、备注、验收图片)
     *
     * @return
     */
    @POST("api/v1/master/order/applyAcceptance")
    Observable<Data> postApplyAcceptance(@Body RequestBody requestBody);

    /**
     * 上传订单测量数据
     *
     * @return
     */
    @POST("api/v1/master/order/uploadMeasureRecord")
    Observable<Data> uploadMeasureRecord(@Body RequestBody requestBody);

    /**
     * 师傅编辑紧急联系人
     *
     * @return
     */
    @POST("api/v1/master/editUrgent")
    Observable<Data> postEditUrgent(@Body RequestBody requestBody);

    /**
     * 师傅订单提现查询
     *
     * @return 0未提现1申请中2已提现[不传值查询全部]
     */
    @GET("api/v1/master/order/withdrawOrders")
    Observable<Data<List<CashWithDrawalBean>>> getWithdrawOrders(@Query("status") String status);



    /**
     * 师傅订单申请提现
     *
     * @return 订单id
     */
    @FormUrlEncoded
    @POST("api/v1/master/order/applyWithdraw")
    Observable<Data> postApplyWithdraw(@Field("orderId") String orderId);
}
