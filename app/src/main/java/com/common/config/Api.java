package com.common.config;

public class Api {

//    public final static String BASE_URL = "http://118.190.37.235:8080/";
//    public final static String BASE_URL = "http://10.10.20.155:8080/";
    public final static String BASE_URL = "http://api.aguogo.cn:8080/";
    public final static String OTHER_URL = "http://118.190.37.235:8080/";

    public final static String CODE_PIC = BASE_URL + "api/v1/common/codePic/generate";//图形验证码

    /**
     * 接口文档：
     * http://118.190.37.235:8080/swagger-ui.html#/master%20order%20API/listOrdersUsingGET
     */
    /**
     * 蓝湖UI设计：
     * https://lanhuapp.com/web/#/item/project/board?pid=75ba26ee-4d9d-489b-adb0-093bb47eacaa
     */
    /**
     * Git：
     * https://e.coding.net/aguogo/wxh_worker_android.git
     */

}
