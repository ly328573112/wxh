package com.common.utils;

import android.text.TextUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 *
 */
public class MoneyUtil {
    /**
     * 格式化金额
     */
    public static String parseMoney(String pattern, BigDecimal bd) {
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(bd);
    }

    public static String getMoneyFormat(String amount) {
        BigDecimal bd = new BigDecimal(amount);
        return parseMoney("######0.00", bd);
    }

    public static String parseMoney(String str) {
        if (TextUtils.isEmpty(str)) {
            return "0.00";
        }
        DecimalFormat df = new DecimalFormat("#.##");
        String string = str.replaceAll(",", "");
        return df.format(Double.parseDouble(string));
    }

    public static String parseMoneyWithComma(String str) {
        if (TextUtils.isEmpty(str)) {
            return "0.00";
        }
        DecimalFormat decimalFormat = new DecimalFormat(",###.##");
        String string = str.replaceAll(",", "");
        return decimalFormat.format(Double.parseDouble(string));
    }
}
