package com.common.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.facebook.stetho.common.LogUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FlieUtils {

    Context context;
    LoadCityDataListener loadCityDataListener;

    public FlieUtils(Context context){
        this.context = context;
    }

    public void getJson(String fileName, LoadCityDataListener loadCityDataListener) {
        this.loadCityDataListener = loadCityDataListener;
        new Thread() {
            @Override

            public void run() {
                //将json数据变成字符串
                StringBuilder stringBuilder = new StringBuilder();
                try {
                    //获取assets资源管理器
                    AssetManager assetManager = context.getAssets();
                    //通过管理器打开文件并读取
                    BufferedReader bf = new BufferedReader(new InputStreamReader(assetManager.open(fileName)));
                    String line;
                    while ((line = bf.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    LogUtil.i("WXH:" + line);
                    Message msgMessage = Message.obtain();
                    if (!TextUtils.isEmpty(line)) {
                        msgMessage.obj = line;
                        msgMessage.what = 1;
                    } else {
                        msgMessage.what = 2;
                    }
                    handler.sendMessage(msgMessage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Object result = (Object) msg.obj;
                    LogUtil.i(result.toString());
                    loadCityDataListener.requestBody();
                    break;
                case 2:
                    loadCityDataListener.requestError();
                    break;
                default:
                    break;
            }
        }
    };

    public interface LoadCityDataListener {
        void requestBody();

        void requestError();
    }
}
