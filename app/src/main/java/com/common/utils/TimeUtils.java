package com.common.utils;

import com.utilCode.utils.ToastUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author: Routee
 * @date ${DATE}
 * @mail wangc4@qianbaocard.com
 * ------------1.本类由Routee开发,阅读、修改时请勿随意修改代码排版格式后提交到git。
 * ------------2.阅读本类时，发现不合理请及时指正.
 * ------------3.如需在本类内部进行修改,请先联系Routee,若未经同意修改此类后造成损失本人概不负责。
 */
public class TimeUtils {
    public static String[] Week_DAYS = new String[]{"周日", "周一", "周二", "周三", "周四", "周五", "周六"};

    public static String stampToTime(String stamp, String pattern) {
        String sd = "";
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        sd = sdf.format(new Date(Long.parseLong(stamp))); // 时间戳转换日期
        return sd;
    }

    /**
     * /**
     *
     * @param str      需要转换的时间
     * @param pattern  转换成的时间格式
     * @param pattern1 传入的时间格式
     * @return
     */
    public static String getDate(String str, String pattern1, String pattern) {

        SimpleDateFormat sdfl = new SimpleDateFormat(pattern1);
        SimpleDateFormat sdfs = new SimpleDateFormat(pattern);
        String result = str;
        try {
            Date date = sdfl.parse(str);
            result = sdfs.format(date);
        } catch (ParseException e) {
        }
        if (result.length() == pattern.length()) {
            return result;
        }
        SimpleDateFormat sdfm = new SimpleDateFormat(pattern1);
        try {
            Date date2 = sdfm.parse(result);
            result = sdfs.format(date2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param dateType 日期格式
     * @return 当前日期的指定格式
     */
    public static String getNowDate(String dateType, int offset) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, offset);
        String day = new SimpleDateFormat(dateType).format(cal.getTime());
        return day;
    }

    /**
     * @param day    当前日期
     * @param format 日期格式
     * @return 是否为当天
     */
    public static boolean isToday(String day, String format) {
        Calendar pre = Calendar.getInstance();
        Date predate = new Date(System.currentTimeMillis());
        pre.setTime(predate);
        Calendar cal = Calendar.getInstance();
        Date date = null;
        try {
            date = new SimpleDateFormat(format).parse(day);
        } catch (ParseException e) {
            ToastUtils.showLongToast("日期解析异常");
        }
        cal.setTime(date);
        if (cal.get(Calendar.YEAR) == (pre.get(Calendar.YEAR))) {
            int diffDay = cal.get(Calendar.DAY_OF_YEAR)
                    - pre.get(Calendar.DAY_OF_YEAR);

            if (diffDay == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param n       延迟n天
     * @param pattern 要返回的日期格式
     * @return 返回n天后的字符串
     */
    public static String getDateNDaysDelay(int n, String pattern) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, n);
        Date date = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String result = sdf.format(date);
        return result;
    }


    /**
     * @param startDate  选择的日期
     * @param mStartDate 日期格式
     * @return 和当天时间间隔
     */
    public static int getNumOfDays(String startDate, String endDate, String mStartDate) {
        boolean isBefore = true;

        SimpleDateFormat sdf = new SimpleDateFormat(mStartDate);
        try {
            isBefore = sdf.parse(startDate).after(sdf.parse(endDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return isBefore ? -1 : 1;
    }

    /**
     * 判断开始时间是否小于结束时间
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static boolean isCorrectTimeDiff(String startTime, String endTime) {
        boolean isCorrect = false;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//年-月-日 时-分
        try {
            Date mStartTime = dateFormat.parse(startTime);//开始时间
            Date mEndTime = dateFormat.parse(endTime);//结束时间
            if (mEndTime.getTime() <= mStartTime.getTime()) {
                isCorrect = false;
            } else {
                isCorrect = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isCorrect;
    }

    /**
     * 计算两个日期相差的天数
     *
     * @param startDateTime
     * @param endDateTime
     * @param format
     * @return
     */
    public static long getDateDiff(String startDateTime, String endDateTime, String format) {
        // 按照传入的格式生成一个simpledateformate对象
        SimpleDateFormat sd = new SimpleDateFormat(format);
        long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
        long diff = 0;
        long day = 0;
        try {
            diff = sd.parse(endDateTime).getTime() - sd.parse(startDateTime).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        day = diff / nd;// 计算差多少天
        return day;
    }

    /**
     * 查询当前时间间隔i个月日期
     *
     * @param i       正数为以后，负数为以前
     * @param pattern 日期格式
     * @return
     */
    public static String beforeMothDate(int i, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, i);
        Date m = c.getTime();
        String mon = format.format(m);
        return mon;
    }

    /**
     * 获取当前月份的第一天
     *
     * @return
     */
    public static String theFirstDayOfMonth(String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天
        return format.format(c.getTime());
    }

    /**
     * 日期转星期
     *
     * @param date
     * @return
     */
    public static String DateToWeek(String date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date parse = null;
        try {
            parse = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parse);
        int dayIndex = calendar.get(Calendar.DAY_OF_WEEK);
        if (dayIndex < 1 || dayIndex > Week_DAYS.length) {
            return null;
        }

        return Week_DAYS[dayIndex - 1];
    }

    /**
     * 获取指定日期的前一个月的日期
     *
     * @param date    指定日期
     * @param pattern 日期格式
     * @return 前一个月的日期
     */
    public static String getPreMonthDate(String date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //amount设置为-1即为前一个月
        cal.add(Calendar.MONTH, -1);
        return format.format(cal.getTime());
    }

    /**
     * 获取指定日期的后一个月的日期
     *
     * @param date    指定日期
     * @param pattern 日期格式
     * @return 后一个月的日期
     */
    public static String getNextMonthDate(String date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //amount设置为1即为后一个月
        cal.add(Calendar.MONTH, 1);
        return format.format(cal.getTime());
    }

    /**
     * 获取指定日期月份内的最后一天日期
     *
     * @param date    指定日期
     * @param pattern 日期格式
     * @return 后一个月的日期
     */
    public static String getLastDayOfMonth(String date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return format.format(cal.getTime());
    }

    /**
     * 获取指定日期月份内的第一天日期
     *
     * @param date    指定日期
     * @param pattern 日期格式
     * @return 后一个月的日期
     */
    public static String getFirstDayOfMonth(String date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //设置为1号
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return format.format(cal.getTime());
    }

    /**
     * 获取指定日期的前一天的日期
     *
     * @param date    指定日期
     * @param pattern 日期格式
     * @return 前一天的日期
     */
    public static String getPreDate(String date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //amount设置为-1即为前1天
        cal.add(Calendar.DATE, -1);
        return format.format(cal.getTime());
    }

    /**
     * 获取指定日期的后一天的日期
     *
     * @param date    指定日期
     * @param pattern 日期格式
     * @return 后一个天的日期
     */
    public static String getNextDate(String date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //amount设置为1即为后1天
        cal.add(Calendar.DATE, 1);
        return format.format(cal.getTime());
    }

    /**
     * 返回当前时间的前monthNum个月
     *
     * @param date
     * @param pattern
     * @param monthNum
     * @return
     */
    public static String getNextSomeMonthDateTime(String date, String pattern, int monthNum) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date nowDateTime = null;
        try {
            nowDateTime = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date nextSomeMonthDateTime;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(nowDateTime);
        calendar.add(Calendar.MONTH, monthNum);
        nextSomeMonthDateTime = calendar.getTime();
        return sdf.format(nextSomeMonthDateTime);
    }


    /**
     * 查询指定时间间隔n天的日期
     *
     * @param n       正数为以后，负数为以前
     * @param pattern 日期格式
     * @return
     */
    public static String beforeDaysDate(int n, String date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(format.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, n);
        Date day = c.getTime();
        return format.format(day);
    }

    /**
     * 获取当前时间：年月日时分秒
     * @return
     */
    public static String getSystemTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyMMddHHmmss", Locale.getDefault());
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        return formatter.format(curDate);
    }

    /**
     * 获取当前时间：年月日
     * @return
     */
    public static String getSystemTimes() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyy-MM-dd", Locale.getDefault());
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        return formatter.format(curDate);
    }


}