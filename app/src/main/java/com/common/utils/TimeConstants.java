package com.common.utils;

import android.os.SystemClock;

public class TimeConstants {
    private long appLaunchTS;
    private long deviceOnTS = System.currentTimeMillis() - SystemClock.elapsedRealtimeNanos() / 1000000;
    private static TimeConstants sGlobalTimeConstants = null;

    private TimeConstants() {
    }

    public static TimeConstants getInstance() {
        synchronized (TimeConstants.class) {
            if (sGlobalTimeConstants == null) {
                sGlobalTimeConstants = new TimeConstants();
            }
        }
        return sGlobalTimeConstants;
    }

    public long getAppLaunchTS() {
        return appLaunchTS;
    }

    public void setAppLaunchTS(long appLaunchTS) {
        this.appLaunchTS = appLaunchTS;
    }

    public long getDeviceOnTS() {
        return deviceOnTS;
    }

}