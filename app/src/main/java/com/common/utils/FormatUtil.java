package com.common.utils;

import android.text.TextUtils;
import android.widget.EditText;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatUtil {

    public static void addEditSpace(CharSequence s, int start, int before, EditText _text) {
        if (s == null || s.length() == 0) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (i != 3 && i != 8 && s.charAt(i) == ' ') {
                continue;
            } else {
                sb.append(s.charAt(i));
                if ((sb.length() == 4 || sb.length() == 9) && sb.charAt(sb.length() - 1) != ' ') {
                    sb.insert(sb.length() - 1, ' ');
                }
            }
        }
        if (!sb.toString().equals(s.toString())) {
            int index = start + 1;
            if (start < sb.length()) {
                if (sb.charAt(start) == ' ') {
                    if (before == 0) {
                        index++;
                    } else {
                        index--;
                    }
                } else {
                    if (before == 1) {
                        index--;
                    }
                }
                _text.setText(sb.toString());
                _text.setSelection(index);
            }
        }
    }

    public static String formatDate(String date) {
        StringBuffer sb = new StringBuffer();
        if (date.length() == 8) {
            sb.append(date.substring(0, 4))
                    .append("-")
                    .append(date.substring(4, 6))
                    .append("-")
                    .append(date.substring(6, 8));
        } else if (date.equals("长期")) {
            return "长期";
        }
        return sb.toString();
    }

    /**
     * 传入银行卡号，将卡号中间隐藏
     *
     * @param card
     * @return
     */
    public static String changeBankCard(String card) {
        return card.replaceAll("(\\d{6})(\\d+)(\\d{4})", "$1**********$3");
    }

    /**
     * 将手机号脱敏
     *
     * @param phone 未脱敏手机号
     * @return 脱敏手机号
     */
    public static String hidePhoneNumber(String phone) {
        return phone.replaceAll("(\\d{3})(\\d+)(\\d{4})", "$1****$3");
    }

    public static String getMoneyFormat(String amount) {
        if (TextUtils.isEmpty(amount)) {
            return "0";
        }
        BigDecimal bd = new BigDecimal(amount);
        return parseMoney("######0.00", bd);
    }

    /**
     * yyyyMMddHHmmss 转 yyyy/MM/dd HH:mm
     *
     * @param time
     * @return
     */
    public static String changeTime(String time, String timeFormat) {
        if (TextUtils.isEmpty(time)) {
            return "";
        }
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat newFormat = new SimpleDateFormat(timeFormat);
        Date currentTime = new Date();
        try {
            currentTime = oldFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newFormat.format(currentTime);
    }

    /**
     * yyyyMMddHHmmss 转 yyyy/MM/dd HH:mm
     *
     * @param time
     * @return
     */
    public static String changeTime1(String time, String timeFormat) {
        if (TextUtils.isEmpty(time)) {
            return "";
        }
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat newFormat = new SimpleDateFormat(timeFormat);
        Date currentTime = new Date();
        try {
            currentTime = oldFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newFormat.format(currentTime);
    }

    /**
     * 格式化金额
     */
    public static String parseMoney(String pattern, BigDecimal bd) {
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(bd);
    }

    /**
     *      * 计算中文字符
     *      *
     *      * @param sequence
     *      * @return
     *     
     */
    public static int countChineseChar(CharSequence sequence) {
        if (TextUtils.isEmpty(sequence)) {
            return 0;
        }
        int charNum = 0;
        for (int i = 0; i < sequence.length(); i++) {
            char word = sequence.charAt(i);
            if (isChineseChar(word)) {//中文
                charNum++;
            }
        }
        return charNum;
    }

    /**
     * 判断是否是中文
     *
     * @param c
     * @return
     */
    public static boolean isChineseChar(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION) {
            return true;
        }
        return false;
    }
}
