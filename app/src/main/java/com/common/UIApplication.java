package com.common;

import android.content.Context;
import android.util.DisplayMetrics;

import com.common.dagger.component.DaggerMyAppComponent;
import com.common.dagger.component.MyAppComponent;
import com.common.dagger.module.MyAppModule;
import com.common.utils.LogUtil;
import com.utilCode.base.BaseApplication;

public class UIApplication extends BaseApplication {

    private static MyAppComponent mMyAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initMyAppInject(this);
        BuglyInitEvent initEvent = new BuglyInitEvent(this);
        initEvent.initBugly();
    }

    public void initMyAppInject(Context context) {
        mMyAppComponent = DaggerMyAppComponent.builder().appComponent(getAppComponent()).myAppModule(new MyAppModule()).build();
    }

    public static MyAppComponent getMyAppComponent() {
        return mMyAppComponent;
    }

}
