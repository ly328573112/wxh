package com.common.logupload;

import android.support.annotation.Keep;

@Keep
public class UploadRequestBean {

    public String code;
    public String message;
    public ResultBean result;
    public PageBean page;

    public class ResultBean {
        /**
         * uid : 1565103474725
         * name : https://oss.aguogo.cn/images/20190806/15651034727485177.png
         * status : done
         * response : success
         */

        public String uid;
        public String name;
        public String status;
        public String response;
    }


    public class PageBean {
        public int pageNum;
        public int pageSize;
        public int total;
    }

}
