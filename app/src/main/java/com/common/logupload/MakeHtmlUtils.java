package com.common.logupload;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class MakeHtmlUtils {

    private MakeHtmlUtils() throws InstantiationException {
        throw new InstantiationException("This class is not for instantiation");
    }

    public static String toBase64(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] bytes = baos.toByteArray();

        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public static Bitmap toBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        int width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : 1;
        int height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : 1;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static Bitmap decodeResource(Context context, int resId) {
        return BitmapFactory.decodeResource(context.getResources(), resId);
    }

    public static long getCurrentTime() {
        return System.currentTimeMillis();
    }

    public static String convertHexColorString(int color) {
        return String.format("#%06X", (0xFFFFFF & color));
    }

    public static long[] getBitmapSize(String url) {
        BitmapFactory.Options op = new BitmapFactory.Options();
        op.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(url, op);
        return new long[]{op.outWidth, op.outHeight};
    }

    public static String makeRitchEditorHtml(Context context, String assetname, String htmlcontent) {
        StringBuilder stringBuilder = new StringBuilder();
        //read head
        stringBuilder.append("<!DOCTYPE html>\n" + "<html lang=\"en\">\n" + "<head>\n" + "    <meta charset=\"UTF-8\">\n" + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" + "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n");
        try {
            //read css
            stringBuilder.append("    <style type=\"text/css\">\n");
            InputStream inputStream = context.getAssets().open(assetname);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            stringBuilder.append(new String(buffer, "UTF-8"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        stringBuilder.append("    </style>\n" + "</head>\n");
        // readbody
        stringBuilder.append("<body>\n" + "    <div class=\"content\">\n" + "        <header>\n" + "            <input hidden='content' type=\"text\" placeholder=\"请输入标题\" class=\"title\" id=\"title\">\n" + "        </header>\n" + "        <div hidden='content' class=\"line\"></div>\n" + "        <div id=\"editor\" placeholder=\"请输入正文\">\n");
        stringBuilder.append(htmlcontent).append("\n");
        stringBuilder.append("    </div>\n" + "</div>\n" + "</body>\n" + "</html>");
        return stringBuilder.toString();
    }

    public static void writrStringtoHtml(Context context, String destfilepath, String datastring) throws
            IOException {
        FileOutputStream out = null;
        try {
            String htmlContent = makeRitchEditorHtml(context, "makehtml.css", datastring);
            out = new FileOutputStream(destfilepath, false);
            out.write(htmlContent.getBytes("UTF-8"));
        } finally {
            out.close();
        }
    }

}
