package com.common.logupload;

import android.os.Handler;
import android.os.Message;

import com.common.UIApplication;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.common.utils.LogUtil;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

public class UploadLogUtils {

    public static final String MULTIPART_FORM_DATA = "multipart/form-data";

    private static MultipartBody.Part prepareFilePart(String partName, File file) {
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), file);
        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    private static MultipartBody.Part filePart(String partName, File file) {
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/png"), file);
        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    private ResponseBodyListener responseBodyListener;

    public void setResponseBodyListener(ResponseBodyListener responseBodyListener) {
        this.responseBodyListener = responseBodyListener;
    }

    public void uploadImg(RequestBody type, File file) {

        List<String> filePathList = new ArrayList<>();
        filePathList.add(file.getAbsolutePath());

        List<Observable<ResponseBody>> observableList = new ArrayList<>();
        for (int i = 0; i < filePathList.size(); i++) {
            MultipartBody.Part body = filePart("file", new File(filePathList.get(i)));
            observableList.add(UIApplication.getMyAppComponent().retrofitOtherApiHelper().uploadFileWithPartMap(type, body));
        }

        Observable.mergeArray(observableList.toArray(new Observable[observableList.size()]))
                .flatMap(new Function<Object, Observable<Boolean>>() {
                    @Override
                    public Observable<Boolean> apply(Object o) throws Exception {
                        return Observable.create(new ObservableOnSubscribe<Boolean>() {
                            @Override
                            public void subscribe(ObservableEmitter<Boolean> emitter) throws Exception {
                                try {
                                    ResponseBody responseBody = (ResponseBody) o;
                                    String repone = responseBody.string();
                                    Gson gson = new Gson();
                                    UploadRequestBean uploadResult = gson.fromJson(repone, UploadRequestBean.class);
                                    LogUtil.d(uploadResult.toString());
                                    UploadRequestBean.ResultBean result = uploadResult.result;
                                    new Thread() {
                                        @Override

                                        public void run() {
                                            Message msgMessage = Message.obtain();
                                            if (result != null) {
                                                LogUtil.i(result.toString());
                                                msgMessage.obj = result;
                                                msgMessage.what = 1;
                                            } else {
                                                msgMessage.what = 2;
                                            }
                                            handler.sendMessage(msgMessage);
                                        }
                                    }.start();
                                    emitter.onNext(true);
                                    emitter.onComplete();
                                } catch (IOException e) {
                                    emitter.onError(e);
                                }
                            }
                        });
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RxObserver<Boolean>() {
                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        Message msgMessage = Message.obtain();
                        msgMessage.what = 2;
                        handler.sendMessage(msgMessage);
                    }
                });
    }

    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    UploadRequestBean.ResultBean result = (UploadRequestBean.ResultBean) msg.obj;
                    responseBodyListener.requestBody(result);
                    break;
                case 2:
                    responseBodyListener.requestError();
                    break;
                default:
                    break;
            }
        }
    };

    public interface ResponseBodyListener {
        void requestBody(UploadRequestBean.ResultBean result);

        void requestError();
    }

}
