package com.common.retrofit.json;

import android.support.annotation.Keep;

@Keep
public class Data<T> {

    private String code;
    private String message;
    private T result;
    private PageBean page;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public PageBean getPage() {
        return page;
    }

    public void setPage(PageBean page) {
        this.page = page;
    }

    public class PageBean{

        public int pageNum = 1;
        public int pageSize;
        public int total;

        @Override
        public String toString() {
            return "PageBean{" +
                    "pageNum=" + pageNum +
                    ", pageSize=" + pageSize +
                    ", total=" + total +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Data{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", result=" + result +
                ", page=" + page +
                '}';
    }
}
