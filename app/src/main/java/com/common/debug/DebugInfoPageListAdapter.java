package com.common.debug;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.utilCode.room.DebugInfo;
import com.wxh.worker.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DebugInfoPageListAdapter extends PagedListAdapter<DebugInfo, DebugInfoPageListAdapter.DebugPageListHolder> {

    public DebugInfoPageListAdapter() {
        super(DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public DebugPageListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DebugPageListHolder(View.inflate(parent.getContext(), R.layout.debug_list_item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull DebugPageListHolder holder, int position) {
        ((DebugPageListHolder) holder).tv_debug_info_id.setText(getItem(position).getDebuginfo());
    }

    public class DebugPageListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_debug_info_id)
        TextView tv_debug_info_id;

        DebugPageListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setupView();
        }

        private void setupView() {

        }
    }

    public static final DiffUtil.ItemCallback<DebugInfo> DIFF_CALLBACK = new DiffUtil.ItemCallback<DebugInfo>() {

        @Override
        public boolean areItemsTheSame(DebugInfo oldItem, DebugInfo newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(DebugInfo oldItem, DebugInfo newItem) {
            return oldItem.getDebuginfo().equals(newItem.getDebuginfo());
        }
    };

}
