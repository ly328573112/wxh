package com.common.debug;

import android.arch.paging.PagedList;
import android.arch.paging.RxPagedListBuilder;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.utilCode.base.mvp.MvpView;
import com.utilCode.room.DebugInfo;
import com.utilCode.room.DebugInfoDataSource;
import com.utilCode.room.RoomDataBase;
import com.utilCode.utils.AndroidUtils;
import com.utilCode.utils.RxUtils;
import com.utilCode.utils.StatusBarHelper;
import com.common.mvp.BaseMvpActivity;
import com.common.retrofit.RxObserver;
import com.wxh.worker.R;

import butterknife.BindView;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;

public class DebugInfoActivity extends BaseMvpActivity<DebugInfoView, DebugInfoRxPresent> implements DebugInfoView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    View toolbar_back;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.clear_data)
    TextView clear_data;
    @BindView(R.id.rcv_list_id)
    RecyclerView rcv_list_id;

    // DebugInfoListAdapter mDebugInfoListAdapter;
    DebugInfoPageListAdapter mDebugInfoPageListAdapter;

    private static final int pageSize = 20;
    Flowable<PagedList<DebugInfo>> mFlowablePagedList;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_debuginfo_layout;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(DebugInfoActivity.this);
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar_title.setText("调试信息");

        clear_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DebugInfoDataSource.getInstance(AndroidUtils.getContext())
                        .deleteAllDebugInfo();
            }
        });

        PagedList.Config config = new PagedList.Config.Builder().setPageSize(pageSize)
                .setInitialLoadSizeHint(pageSize * 2)
                .setEnablePlaceholders(false)
                .build();

        mFlowablePagedList = new RxPagedListBuilder<>(RoomDataBase.getInstance(AndroidUtils
                .getContext())
                .debugInfoDao()
                .loadDataSource(), config).buildFlowable(BackpressureStrategy.LATEST);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DebugInfoActivity.this);
        rcv_list_id.setLayoutManager(linearLayoutManager);
        // mDebugInfoListAdapter = new DebugInfoListAdapter(DebugInfoActivity.this);
        // rcv_list_id.setAdapter(mDebugInfoListAdapter);
        mDebugInfoPageListAdapter = new DebugInfoPageListAdapter();
        rcv_list_id.setAdapter(mDebugInfoPageListAdapter);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        /*DebugInfoDataSource.getInstance(Global.getContext())
                .getDebugInfo()
                .toObservable()
                .compose(RxUtils.<List<DebugInfo>>applySchedulersLifeCycle((MvpView) this))
                .subscribe(new RxObserver<List<DebugInfo>>() {
                    @Override
                    public void onNext(List<DebugInfo> debugInfos) {
                        mDebugInfoListAdapter.resetData(debugInfos);
                    }
                });*/

        mFlowablePagedList.toObservable()
                .compose(RxUtils.<PagedList<DebugInfo>>applySchedulersLifeCycle((MvpView) this))
                .subscribe(new RxObserver<PagedList<DebugInfo>>() {
                    @Override
                    public void onNext(PagedList<DebugInfo> debugInfos) {
                        mDebugInfoPageListAdapter.submitList(debugInfos);
                    }
                });

    }

    @Override
    public void onClick(View v) {

    }
}
