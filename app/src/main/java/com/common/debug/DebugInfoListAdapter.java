package com.common.debug;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.utilCode.room.DebugInfo;
import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DebugInfoListAdapter extends BaseRecycleViewAdapter<DebugInfo> {

    Context mContext;

    public DebugInfoListAdapter(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, DebugInfo item) {
        ((DebugListHolder) holder).tv_debug_info_id.setText(item.getDebuginfo());
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new DebugListHolder(View.inflate(mContext, R.layout.debug_list_item, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class DebugListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_debug_info_id)
        TextView tv_debug_info_id;

        DebugListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setupView();
        }

        private void setupView() {

        }
    }

}
