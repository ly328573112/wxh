package com.common.debug;

import com.utilCode.base.mvp.RxMvpPresenter;
import com.common.config.MyApi;

import javax.inject.Inject;

public class DebugInfoRxPresent extends RxMvpPresenter<DebugInfoView> {

    protected MyApi mMyApi;

    @Inject
    public DebugInfoRxPresent(MyApi myApi) {
        mMyApi = myApi;
    }
}
