package com.common.apputil;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;

import com.utilCode.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class AppStoreDownload {

    private static String xiaomi = "Xiaomi";               //小米
    private static String huawei = "HUAWEI";               //华为
    private static String meizu = "Meizu";                 //魅族
    private static String oppo = "OPPO";                   //oppo
    private static String vivo = "vivo";                   //vivo
    private static String chuizi = "SMARTISAN";            //锤子

    private Context context;
    private String brand;

    public AppStoreDownload(Context context) {
        this.context = context;
        brand = Build.BRAND;
    }

    public boolean marketLogic() {
        boolean downloadError = false;
        String appStorePackage = getBrandAppPkg();
        downloadError = GoToScoreUtils.launchAppDetail(context, getAppProcessName(context), appStorePackage);
        return downloadError;
    }

    public String getBrandAppPkg() {
        if (StringUtils.equals(brand, xiaomi)) {
            return GoToScoreUtils.APPSTORE_XIAOMI;
        } else if (StringUtils.equals(brand, huawei)) {
            return GoToScoreUtils.APPSTORE_HUAWEI;
        } else if (StringUtils.equals(brand, meizu)) {
            return GoToScoreUtils.APPSTORE_MEIZU;
        } else if (StringUtils.equals(brand, oppo)) {
            return GoToScoreUtils.APPSTORE_OPPO;
        } else if (StringUtils.equals(brand, vivo)) {
            return GoToScoreUtils.APPSTORE_BBK;
        } else {
            String appPkg = "";
            ArrayList<String> marketPkgList = GoToScoreUtils.queryInstalledMarketPkgs(context);
            for (String marketPkg : marketPkgList) {
                if (StringUtils.equals(marketPkg, GoToScoreUtils.APPSTORE_TENCENT)) {
                    appPkg = GoToScoreUtils.APPSTORE_TENCENT;
                    break;
                }
            }
            return appPkg;
        }
    }


    /**
     * 获取当前应用程序的包名
     *
     * @param context 上下文对象
     * @return 返回包名
     */
    public String getAppProcessName(Context context) {
        //当前应用pid
        int pid = android.os.Process.myPid();
        //任务管理类
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        //遍历所有应用
        List<ActivityManager.RunningAppProcessInfo> infos = manager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo info : infos) {
            if (info.pid == pid) {//得到当前应用
                return info.processName;//返回包名
            }
        }
        return "";
    }

}
