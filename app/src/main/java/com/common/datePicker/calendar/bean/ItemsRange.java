/*
 *  Android Wheel Control.
 *  https://code.google.com/p/android-wheel/
 *
 *  Copyright 2011 Yuri Kanivets
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.common.datePicker.calendar.bean;

import android.support.annotation.Keep;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Range for visible items.
 */
@Keep
public class ItemsRange {
    // First item number
    private int first;

    // Items count
    private int count;

    /**
     * Default constructor. Creates an empty range
     */
    public ItemsRange() {
        this(0, 0);
    }

    /**
     * Constructor
     * @param first the number of first item
     * @param count the count of items
     */
    public ItemsRange(int first, int count) {
        this.first = first;
        this.count = count;
    }

    /**
     * Gets number of first item
     * @return the number of the first item
     */
    public int getFirst() {
        return first;
    }

    /**
     * Gets number of last item
     * @return the number of last item
     */
    public int getLast() {
        return getFirst() + getCount() - 1;
    }

    /**
     * Get items count
     * @return the count of items
     */
    public int getCount() {
        return count;
    }

    /**
     * Tests whether item is contained by range
     * @param index the item number
     * @return true if item is contained
     */
    public boolean contains(int index) {
        return index >= getFirst() && index <= getLast();
    }

    public static int getYear() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.YEAR);
    }

    public static int getMonth() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.MONTH) + 1;
    }

    public static int getDay() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.DATE);
    }

    public static int getHH() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.HOUR_OF_DAY);
    }

    public static int getMM() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.MINUTE);
    }

    public static String getToday() {
        String month = getMonth() + "";
        if (month.length() == 1) {
            month = "0" + month;
        }
        String day = getDay() + "";
        if (day.length() == 1) {
            day = "0" + day;
        }
        return getYear() + month + day;
    }

    public static String getYesterDay(boolean isTransverse) {
        String currentTime = "";
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        if (isTransverse) {
            currentTime = "yyyy/MM/dd";
        } else {
            currentTime = "yyyyMMdd";
        }
        String yesterday = new SimpleDateFormat(currentTime).format(cal.getTime());
        return yesterday;
    }

    public static String getOneDay(boolean isTransverse, int offset) {
        String currentTime = "";
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, offset);
        if (isTransverse) {
            currentTime = "yyyy-MM-dd";
        } else {
            currentTime = "yyyyMMdd";
        }
        String day = new SimpleDateFormat(currentTime).format(cal.getTime());
        return day;
    }
}