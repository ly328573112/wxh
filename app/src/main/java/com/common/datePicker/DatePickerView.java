package com.common.datePicker;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.common.datePicker.calendar.adapter.AbstractWheelTextAdapter;
import com.common.datePicker.calendar.bean.ItemsRange;
import com.common.datePicker.calendar.listener.OnWheelChangedListener;
import com.common.datePicker.calendar.listener.OnWheelScrollListener;
import com.common.datePicker.calendar.view.WheelView;
import com.utilCode.utils.StringUtils;
import com.wxh.worker.R;

import java.util.ArrayList;


/**
 * 年、月、日选择对话框
 */
public class DatePickerView extends BottomSheetDialog implements View.OnClickListener {

    public static final int SHOWTYPE_YY_MM_DD = 0x10001;
    public static final int SHOWTYPE_YY_MM = 0x10002;
    public static final int SHOWTYPE_MM_DD = 0x10003;
    public static final int SHOWTYPE_MM_DD_hh_mm = 0x10004;

    private static final int MIN_YEAR = 1970;
    private static final int MAX_YEAR = 2100;

    private Context context;

    private TextView btn_dialog_sure;
    private TextView btn_dialog_cancel;
    private WheelView wv_year_id;
    private WheelView wv_month_id;
    private WheelView wv_day_id;
    private WheelView wv_hh_id;
    private WheelView wv_mm_id;

    private ArrayList<String> arry_years = new ArrayList<>();
    private ArrayList<String> arry_months = new ArrayList<>();
    private ArrayList<String> arry_days = new ArrayList<>();
    private ArrayList<String> arry_hh = new ArrayList<>();
    private ArrayList<String> arry_mm = new ArrayList<>();

    private CalendarTextAdapter mYearAdapter;
    private CalendarTextAdapter mMonthAdapter;
    private CalendarTextAdapter mDaydapter;
    private CalendarTextAdapter mmAdapter;
    private CalendarTextAdapter hhAdapter;

    private int currentYear = ItemsRange.getYear();
    private int currentMonth = ItemsRange.getMonth();
    private int currentDay = ItemsRange.getDay();
    private int currentHH = ItemsRange.getHH();
    private int currentMM = ItemsRange.getMM();

    private int maxTextSize = 18;
    private int minTextSize = 14;

    private String selectYear;
    private String selectMonth;
    private String selectDay;
    private String selectHH;
    private String selectMM;

    // must be 2018-08-08 AND 20180808 standard format
    private String date;

    private int showtype = SHOWTYPE_YY_MM_DD;
    private boolean showYMD = false;

    private OnDatePickListener onDatePickListener;

    public DatePickerView(Context context) {
        super(context);
        this.context = context;
        setupView();
        setupData();
    }

    public DatePickerView(Context context, int showtype) {
        super(context);
        this.context = context;
        this.showtype = showtype;
        setupView();
        setupData();
    }

    public DatePickerView(Context context, int showtype, boolean showYMD) {
        super(context);
        this.context = context;
        this.showtype = showtype;
        this.showYMD = showYMD;
        setupView();
        setupData();
    }

    public DatePickerView(Context context, String date) {
        super(context);
        this.context = context;
        this.date = date;
        setupView();
        setupData();
    }

    public DatePickerView(Context context, String date, int showtype) {
        super(context);
        this.context = context;
        this.date = date;
        this.showtype = showtype;
        setupView();
        setupData();
    }

    public DatePickerView(Context context, String date, boolean showYMD) {
        super(context);
        this.context = context;
        this.date = date;
        this.showYMD = showYMD;
        setupView();
        setupData();
    }

    public DatePickerView(Context context, String date, int showtype, boolean showYMD) {
        super(context);
        this.context = context;
        this.date = date;
        this.showtype = showtype;
        this.showYMD = showYMD;
        setupView();
        setupData();
    }

    private void setupView() {
        setContentView(R.layout.data_pick_layout);
        btn_dialog_sure = findViewById(R.id.btn_dialog_sure);
        btn_dialog_cancel = findViewById(R.id.btn_dialog_cancel);
        btn_dialog_sure.setOnClickListener(this);
        btn_dialog_cancel.setOnClickListener(this);
        wv_year_id = findViewById(R.id.wv_year_id);
        wv_month_id = findViewById(R.id.wv_month_id);
        wv_day_id = findViewById(R.id.wv_day_id);
        wv_hh_id = findViewById(R.id.wv_hh_id);
        wv_mm_id = findViewById(R.id.wv_mm_id);
        setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
            }
        });

        if (showtype == SHOWTYPE_YY_MM_DD) {
            wv_year_id.setVisibility(View.VISIBLE);
            wv_month_id.setVisibility(View.VISIBLE);
            wv_day_id.setVisibility(View.VISIBLE);
            wv_hh_id.setVisibility(View.GONE);
            wv_mm_id.setVisibility(View.GONE);
        } else if (showtype == SHOWTYPE_YY_MM) {
            wv_year_id.setVisibility(View.VISIBLE);
            wv_month_id.setVisibility(View.VISIBLE);
            wv_day_id.setVisibility(View.GONE);
            wv_hh_id.setVisibility(View.GONE);
            wv_mm_id.setVisibility(View.GONE);
        } else if (showtype == SHOWTYPE_MM_DD) {
            wv_year_id.setVisibility(View.GONE);
            wv_month_id.setVisibility(View.VISIBLE);
            wv_day_id.setVisibility(View.VISIBLE);
            wv_hh_id.setVisibility(View.GONE);
            wv_mm_id.setVisibility(View.GONE);
        } else if(showtype == SHOWTYPE_MM_DD_hh_mm){
            wv_year_id.setVisibility(View.VISIBLE);
            wv_month_id.setVisibility(View.VISIBLE);
            wv_day_id.setVisibility(View.VISIBLE);
            wv_hh_id.setVisibility(View.VISIBLE);
            wv_mm_id.setVisibility(View.VISIBLE);
        }
    }

    private void setupData() {
        if (!StringUtils.isEmpty(date)) {
            setDate(date);
        } else {
            setDate(ItemsRange.getYear(), ItemsRange.getMonth(), ItemsRange.getDay() - 1, ItemsRange.getHH(), ItemsRange.getMM());
        }

        initYears();
        mYearAdapter = new CalendarTextAdapter(context, arry_years, setYear(currentYear), maxTextSize, minTextSize);
        wv_year_id.setVisibleItems(5);
        wv_year_id.setViewAdapter(mYearAdapter);
        wv_year_id.setCurrentItem(setYear(currentYear));

        initMonths();
        mMonthAdapter = new CalendarTextAdapter(context, arry_months, setMonth(currentMonth), maxTextSize, minTextSize);
        wv_month_id.setVisibleItems(5);
        wv_month_id.setCyclic(true);
        wv_month_id.setViewAdapter(mMonthAdapter);
        wv_month_id.setCurrentItem(setMonth(currentMonth));

        initDays(calDays(currentYear, currentMonth));
        mDaydapter = new CalendarTextAdapter(context, arry_days, currentDay - 1, maxTextSize, minTextSize);
        wv_day_id.setVisibleItems(5);
        wv_day_id.setCyclic(true);
        wv_day_id.setViewAdapter(mDaydapter);
        wv_day_id.setCurrentItem(currentDay - 1);

        initHH();
        hhAdapter = new CalendarTextAdapter(context, arry_hh, setMonth(currentHH), maxTextSize, minTextSize);
        wv_hh_id.setVisibleItems(5);
        wv_hh_id.setCyclic(true);
        wv_hh_id.setViewAdapter(hhAdapter);
        wv_hh_id.setCurrentItem(setMonth(currentHH));

        initMM();
        mmAdapter = new CalendarTextAdapter(context, arry_mm, setMonth(currentMM), maxTextSize, minTextSize);
        wv_mm_id.setVisibleItems(5);
        wv_mm_id.setCyclic(true);
        wv_mm_id.setViewAdapter(mmAdapter);
        wv_mm_id.setCurrentItem(setMonth(currentMM));

        wv_year_id.addChangingListener(yearChangedListener);
        wv_year_id.addScrollingListener(yearScrollListener);

        wv_month_id.addChangingListener(monthChangedListener);
        wv_month_id.addScrollingListener(monthScrollListener);

        wv_day_id.addChangingListener(dayChangedListener);
        wv_day_id.addScrollingListener(dayScrollListener);

        wv_hh_id.addChangingListener(hhChangedListener);
        wv_hh_id.addScrollingListener(hhScrollListener);

        wv_mm_id.addChangingListener(mmChangedListener);
        wv_mm_id.addScrollingListener(mmScrollListener);
    }

    private OnWheelChangedListener yearChangedListener = new OnWheelChangedListener(){
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            String currentText = (String) mYearAdapter.getItemText(wheel.getCurrentItem());
            currentText = !showYMD ? currentText : currentText.substring(0, currentText.length() - 1);
            selectYear = currentText;
            setTextviewSize(currentText, mYearAdapter);
            currentYear = Integer.parseInt(currentText);
            initDays(calDays(currentYear, currentMonth));
            if (calDays(currentYear, currentMonth) < currentDay) {
                currentDay = calDays(currentYear, currentMonth);
            }
            mDaydapter = new CalendarTextAdapter(context, arry_days, currentDay - 1, maxTextSize, minTextSize);
            wv_day_id.setVisibleItems(5);
            wv_day_id.setViewAdapter(mDaydapter);
            wv_day_id.setCurrentItem(currentDay - 1);
        }
    };

    private OnWheelScrollListener yearScrollListener = new OnWheelScrollListener(){
        @Override
        public void onScrollingStarted(WheelView wheel) {

        }

        @Override
        public void onScrollingFinished(WheelView wheel) {
            String currentText = (String) mYearAdapter.getItemText(wheel.getCurrentItem());
            setTextviewSize(currentText, mYearAdapter);
//                if (currentYear > ItemsRange.getYear()) {
//                    wv_year_id.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            wv_year_id.setCurrentItem(setYear(ItemsRange.getYear()), true);
//                        }
//                    }, 50);
//                }
        }
    };

    private OnWheelChangedListener monthChangedListener = new OnWheelChangedListener(){
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            String currentText = (String) mMonthAdapter.getItemText(wheel.getCurrentItem());
            currentText = !showYMD ? currentText : currentText.substring(0, currentText.length() - 1);
            selectMonth = currentText;
            setTextviewSize(currentText, mMonthAdapter);
            currentMonth = Integer.parseInt(currentText);
            initDays(calDays(currentYear, currentMonth));
            if (calDays(currentYear, currentMonth) < currentDay) {
                currentDay = calDays(currentYear, currentMonth);
            }
            mDaydapter = new CalendarTextAdapter(context, arry_days, currentDay - 1, maxTextSize, minTextSize);
            wv_day_id.setVisibleItems(5);
            wv_day_id.setViewAdapter(mDaydapter);
            wv_day_id.setCurrentItem(currentDay - 1);
        }
    };

    private OnWheelScrollListener monthScrollListener = new OnWheelScrollListener(){
        @Override
        public void onScrollingStarted(WheelView wheel) {
            wv_day_id.postDelayed(new Runnable() {
                @Override
                public void run() {
                    int calDays = calDays(Integer.parseInt(selectYear), Integer.parseInt(selectMonth));
                    wv_day_id.setCurrentItem(calDays, true);
                }
            }, 50);
        }

        @Override
        public void onScrollingFinished(WheelView wheel) {
            String currentText = (String) mMonthAdapter.getItemText(wheel.getCurrentItem());
            setTextviewSize(currentText, mMonthAdapter);
        }
    };

    private OnWheelChangedListener dayChangedListener = new OnWheelChangedListener(){
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            String currentText = (String) mDaydapter.getItemText(wheel.getCurrentItem());
            currentText = !showYMD ? currentText : currentText.substring(0, currentText.length() - 1);
            selectDay = currentText;
            setTextviewSize(currentText, mDaydapter);
            currentDay = Integer.parseInt(currentText);
        }
    };

    private OnWheelScrollListener dayScrollListener = new OnWheelScrollListener(){
        @Override
        public void onScrollingStarted(WheelView wheel) {

        }

        @Override
        public void onScrollingFinished(WheelView wheel) {
            String currentText = (String) mDaydapter.getItemText(wheel.getCurrentItem());
            setTextviewSize(currentText, mDaydapter);
        }
    };

    private OnWheelChangedListener hhChangedListener = new OnWheelChangedListener(){
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            String currentText = (String) hhAdapter.getItemText(wheel.getCurrentItem());
            currentText = !showYMD ? currentText : currentText.substring(0, currentText.length() - 1);
            selectHH = currentText;
            setTextviewSize(currentText, hhAdapter);
            currentHH = Integer.parseInt(currentText);
        }
    };

    private OnWheelScrollListener hhScrollListener = new OnWheelScrollListener(){
        @Override
        public void onScrollingStarted(WheelView wheel) {

        }

        @Override
        public void onScrollingFinished(WheelView wheel) {
            String currentText = (String) hhAdapter.getItemText(wheel.getCurrentItem());
            setTextviewSize(currentText, hhAdapter);
        }
    };

    private OnWheelChangedListener mmChangedListener = new OnWheelChangedListener(){
        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            String currentText = (String) mmAdapter.getItemText(wheel.getCurrentItem());
            currentText = !showYMD ? currentText : currentText.substring(0, currentText.length() - 1);
            selectMM = currentText;
            setTextviewSize(currentText, mmAdapter);
            currentMM = Integer.parseInt(currentText);
        }
    };

    private OnWheelScrollListener mmScrollListener = new OnWheelScrollListener(){
        @Override
        public void onScrollingStarted(WheelView wheel) {

        }

        @Override
        public void onScrollingFinished(WheelView wheel) {
            String currentText = (String) mmAdapter.getItemText(wheel.getCurrentItem());
            setTextviewSize(currentText, mmAdapter);
        }
    };

    public void initYears() {
        for (int i = MIN_YEAR; i < MAX_YEAR; i++) {
            arry_years.add(i + (!showYMD ? "" : "年"));
        }
    }

    public void initMonths() {
        arry_months.clear();
        for (int i = 1; i <= 12; i++) {
            arry_months.add(i + (!showYMD ? "" : "月"));
        }
    }

    public void initDays(int days) {
        arry_days.clear();
        for (int i = 1; i <= days; i++) {
            arry_days.add(i + (!showYMD ? "" : "日"));
        }
    }

    public void initHH() {
        arry_hh.clear();
        for (int i = 1; i <= 24; i++) {
            arry_hh.add(i + (!showYMD ? "" : "时"));
        }
    }

    public void initMM() {
        arry_mm.clear();
        for (int i = 1; i <= 60; i++) {
            arry_mm.add(i + (!showYMD ? "" : "分"));
        }
    }

    public void setDatePickListener(OnDatePickListener onDatePickListener) {
        this.onDatePickListener = onDatePickListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_dialog_sure:
                if (onDatePickListener != null) {
                    if (selectMonth.length() == 1) {
                        selectMonth = "0" + selectMonth;
                    }
                    if (selectDay.length() == 1) {
                        selectDay = "0" + selectDay;
                    }
                    if (selectHH.length() == 1) {
                        selectHH = "0" + selectHH;
                    }
                    if (selectMM.length() == 1) {
                        selectMM = "0" + selectMM;
                    }
                    onDatePickListener.onClick(true, selectYear, selectMonth, selectDay, selectHH, selectMM);
                }
                dismiss();
                break;

            case R.id.btn_dialog_cancel:
                onDatePickListener.onClick(false, null, null, null, null, null);
                dismiss();
                break;
        }
    }

    public interface OnDatePickListener {
        void onClick(boolean sureCancel, String year, String month, String day, String hh, String mm);
    }

    public void setTextviewSize(String curriteItemText, CalendarTextAdapter adapter) {
        ArrayList<View> arrayList = adapter.getTestViews();
        int size = arrayList.size();
        String currentText;
        for (int i = 0; i < size; i++) {
            TextView textvew = (TextView) arrayList.get(i);
            currentText = textvew.getText().toString();
            if (curriteItemText.equals(currentText)) {
                textvew.setTextSize(maxTextSize);
            } else {
                textvew.setTextSize(minTextSize);
            }
        }
    }

    public void setDate(int year, int month, int day, int hh, int mm) {
        selectYear = year + "";
        selectMonth = month + "";
        selectDay = day + "";
        selectHH = hh + "";
        selectMM = mm + "";
        this.currentYear = year;
        this.currentMonth = month;
        this.currentDay = day;
        this.currentHH = hh;
        this.currentMM = mm;
    }

    public void setDate(String date) {
        int year = Integer.valueOf(date.replace("-", "").substring(0, 4));
        int month = Integer.valueOf(date.replace("-", "").substring(4, 6));
        int day = Integer.valueOf(date.replace("-", "").substring(6, 8));
        setDate(year, month, day, 0, 0);
    }

    private int setYear(int year) {
        int yearIndex = 0;
        for (int i = MIN_YEAR; i < MAX_YEAR; i++) {
            if (i == year) {
                return yearIndex;
            }
            yearIndex++;
        }
        return yearIndex;
    }

    private int setMonth(int month) {
        int monthIndex = 0;
        for (int i = 1; i < 12; i++) {
            if (month == i) {
                return monthIndex;
            } else {
                monthIndex++;
            }
        }
        return monthIndex;
    }

    public int calDays(int year, int month) {
        int day = 0;
        boolean leayyear = false;
        if (year % 4 == 0 && year % 100 != 0) {
            leayyear = true;
        } else {
            leayyear = false;
        }
        for (int i = 1; i <= 12; i++) {
            switch (month) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    day = 31;
                    break;
                case 2:
                    if (leayyear) {
                        day = 29;
                    } else {
                        day = 28;
                    }
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    day = 30;
                    break;
            }
        }
        return day;
    }

    private int setHH(int hh) {
        int hhIndex = 0;
        for (int i = 1; i < 24; i++) {
            if (hh == i) {
                return hhIndex;
            } else {
                hhIndex++;
            }
        }
        return hhIndex;
    }

    private int setMM(int mm) {
        int mmIndex = 0;
        for (int i = 1; i < 60; i++) {
            if (mm == i) {
                return mmIndex;
            } else {
                mmIndex++;
            }
        }
        return mmIndex;
    }

    private class CalendarTextAdapter extends AbstractWheelTextAdapter {
        ArrayList<String> list;

        protected CalendarTextAdapter(Context context, ArrayList<String> list, int currentItem, int maxsize, int minsize) {
            super(context, R.layout.calendar_item_birth_year, NO_RESOURCE, currentItem, maxsize, minsize);
            this.list = list;
            setItemTextResource(R.id.tempValue);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            View view = super.getItem(index, cachedView, parent);
            return view;
        }

        @Override
        public int getItemsCount() {
            return list.size();
        }

        @Override
        protected CharSequence getItemText(int index) {
            return list.get(index) + "";
        }
    }

}