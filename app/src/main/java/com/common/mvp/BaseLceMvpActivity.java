package com.common.mvp;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.view.View;

import com.utilCode.base.mvp.MvpPresenter;
import com.utilCode.base.mvp.MvpView;

public abstract class BaseLceMvpActivity<V extends MvpView, P extends MvpPresenter<V>> extends BaseMvpActivity<V, P> {

    protected View contentView;
    protected View loadingView;
    protected View emptyView;
    protected View errorView;

    @CallSuper
    @Override
    public void onContentChanged() {
        super.onContentChanged();
        contentView = createContentView();
        loadingView = createLoadingView();
        emptyView = createEmptyView();
        errorView = createErrorView();

        if (loadingView == null) {
            throw new NullPointerException("Loading view is null! Have you specified a loading view in your layout xml file?" + " You have to give your loading View the id R.id.loadingView");
        }

        if (contentView == null) {
            throw new NullPointerException("Content view is null! Have you specified a content view in your layout xml file?" + " You have to give your content View the id R.id.contentView");
        }

        if (emptyView == null) {
            throw new NullPointerException("emptyView view is null! Have you specified an emptyView view in your layout xml file?" + " You have to give your emptyView View the id R.id.emptyView");
        }

        if (errorView == null) {
            throw new NullPointerException("Error view is null! Have you specified an error view in your layout xml file?" + " You have to give your error View the id R.id.errorView");
        }

        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onErrorViewClicked();
            }
        });
    }

    @NonNull
    protected View createContentView() {
        return findViewById(com.utilCode.R.id.contentView);
    }

    @NonNull
    protected View createLoadingView() {
        return findViewById(com.utilCode.R.id.loadingView);
    }

    protected View createEmptyView() {
        return findViewById(com.utilCode.R.id.emptyView);
    }

    @NonNull
    protected View createErrorView() {
        return findViewById(com.utilCode.R.id.errorView);
    }

    protected abstract void onErrorViewClicked();

    public void showLoading(boolean pullToRefresh) {
        if (!pullToRefresh) {
            animateLoadingViewIn();
        }
        // otherwise the pull to refresh widget will already display a loading animation
    }

    protected void animateLoadingViewIn() {
        BaseLceAnimator.showLoading(loadingView, contentView, emptyView, errorView);
    }

    public void showContent() {
        animateContentViewIn();
    }

    protected void animateContentViewIn() {
        BaseLceAnimator.showContent(loadingView, contentView, emptyView, errorView);
    }

    public void showEmpty(boolean pullToRefresh) {
        if (!pullToRefresh) {
            animateEmptyViewIn();
        }
    }

    protected void animateEmptyViewIn() {
        BaseLceAnimator.showEmptyView(loadingView, contentView, emptyView, errorView);
    }

    public void showError(boolean pullToRefresh) {
        if (!pullToRefresh) {
            animateErrorViewIn();
        }
    }

    protected void animateErrorViewIn() {
        BaseLceAnimator.showErrorView(loadingView, contentView, emptyView, errorView);
    }
}
