package com.common.imageloader;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.utilCode.dagger.help.GlideApp;
import com.utilCode.utils.DisplayUtils;
import com.wxh.worker.R;
import com.youth.banner.loader.ImageLoader;

public class GlideImageLoader extends ImageLoader {

    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        imageView.setPadding(DisplayUtils.dp2px(15), 0, DisplayUtils.dp2px(15), 0);
        if(path instanceof Integer){
            GlideApp.with(context).asBitmap().load(path).centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .placeholder(R.drawable.img_place_adverts).transform(new GlideRoundTransform(5)).into(imageView);
        }else{
            GlideApp.with(context).asBitmap().load((String) path).centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .placeholder(R.drawable.img_place_adverts).transform(new GlideRoundTransform(5)).into(imageView);
        }
    }
}
