package com.wxh.worker.common_view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wxh.worker.common_view.bean.PhotoInfo;
import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by hdb on 2017/7/10.
 */

public class PhotoImageAdapter extends BaseRecycleViewAdapter<PhotoInfo> {

    private ImageClickListener imageClickListener;

    public PhotoImageAdapter(Context context) {
        super(context);
    }


    public void setImageClickListener(ImageClickListener imageClickListener) {
        this.imageClickListener = imageClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(final RecyclerView.ViewHolder holder, PhotoInfo item) {
        if (holder instanceof ImageHolder) {
            ImageHolder imageHolder = (ImageHolder) holder;
            /* 是否处于可删除状态 */
            boolean isDelete = item.delete;
            if (isDelete) {
                imageHolder.ivDelete.setVisibility(View.VISIBLE);
            } else {
                imageHolder.ivDelete.setVisibility(View.GONE);
            }
            String imgUri = item.photoUri;
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.error(R.drawable.ic_split_graph);
            if (TextUtils.isEmpty(imgUri)) {
                Glide.with(mContext).load(R.drawable.icon_photo_camera).apply(requestOptions).into(imageHolder.ivPhoto);
            } else {
                Glide.with(mContext).load(imgUri).apply(requestOptions).into(imageHolder.ivPhoto);
            }
            imageHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageClickListener.openCamera(holder.getLayoutPosition());
                }
            });

            imageHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageClickListener.onDeleteImage(holder.getLayoutPosition());
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(View.inflate(mContext, R.layout.comment_image_item, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }


    class ImageHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_photo)
        ImageView ivPhoto;
        @BindView(R.id.iv_delete)
        ImageView ivDelete;


        ImageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
