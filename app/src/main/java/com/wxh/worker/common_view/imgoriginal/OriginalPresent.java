package com.wxh.worker.common_view.imgoriginal;

import com.common.config.MyApi;
import com.utilCode.base.mvp.RxMvpPresenter;

import javax.inject.Inject;

public class OriginalPresent extends RxMvpPresenter<OriginalView> {

    protected MyApi mMyApi;

    @Inject
    public OriginalPresent(MyApi myApi) {
        mMyApi = myApi;
    }

}
