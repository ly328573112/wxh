package com.wxh.worker.common_view.imgoriginal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.photocameralib.media.widget.PreviewerViewPager;
import com.wxh.worker.event.OriginalDeleteEvent;
import com.common.mvp.BaseMvpActivity;
import com.wxh.worker.R;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author LuoYi
 * Time: 2017 10:15 2017/10/30 10
 */

public class OriginalPagerActivity extends BaseMvpActivity<OriginalView, OriginalPresent> implements OriginalView, View.OnClickListener, ViewPager.OnPageChangeListener,
        OriginalContract.PagerAdapterView {

    public static final String KEY_IMAGE = "images";
    public static final String KEY_POSITION = "position";
    public static final String KEY_DELETE = "delete";

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.pager_delete_rl)
    RelativeLayout pagerDeleteRl;
    @BindView(R.id.original_pager_pv)
    PreviewerViewPager originalPagerView;
    @BindView(R.id.original_index_tv)
    TextView originalIndexTv;

    private List<String> mImageSources;
    private int mCurPosition = 0;
    private boolean isDelete = true;
    private OriginalPagerAdapter originalPagerAdapter;

    public static void start(Activity activity, ArrayList<String> imageList, int pos) {
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(KEY_IMAGE, imageList);
        bundle.putInt(KEY_POSITION, pos);
        Intent intent = new Intent(activity, OriginalPagerActivity.class);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public static void start(Activity activity, ArrayList<String> imageList, int pos, boolean isDelete) {
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(KEY_IMAGE, imageList);
        bundle.putInt(KEY_POSITION, pos);
        bundle.putBoolean(KEY_DELETE, isDelete);
        Intent intent = new Intent(activity, OriginalPagerActivity.class);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.original_pager_view;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        toolbarBack.setOnClickListener(this);
        pagerDeleteRl.setOnClickListener(this);
        originalPagerView.addOnPageChangeListener(this);

        toolbarTitle.setVisibility(View.INVISIBLE);
    }


    @Override
    protected void setupData(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        mImageSources = bundle.getStringArrayList(KEY_IMAGE);
        mCurPosition = bundle.getInt(KEY_POSITION, 0);
        isDelete = bundle.getBoolean(KEY_DELETE, true);

        if (!isDelete) {
            pagerDeleteRl.setVisibility(View.INVISIBLE);
        } else {
            pagerDeleteRl.setVisibility(View.VISIBLE);
        }

        originalPagerAdapter = new OriginalPagerAdapter(this, mImageSources, this);
        originalPagerView.setAdapter(originalPagerAdapter);
        originalPagerView.setCurrentItem(mCurPosition);
        onPageSelected(mCurPosition);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_back:
                finish();
                break;

            case R.id.pager_delete_rl:
                mImageSources.remove(mCurPosition);

                OriginalDeleteEvent deleteEvent = new OriginalDeleteEvent();
                deleteEvent.curPosition = mCurPosition;
                EventBus.getDefault().post(deleteEvent);

                if (mImageSources.size() == 0) {
                    finish();
                    return;
                }

                originalPagerAdapter = new OriginalPagerAdapter(this, mImageSources, this);
                originalPagerView.setAdapter(originalPagerAdapter);
                originalPagerView.setCurrentItem(mCurPosition);
                break;

            default:
                break;
        }
    }

    private boolean first = true;

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (first && position == 0) {
            onPageSelected(0);
            first = false;
        } else {
            first = true;
        }
    }

    @Override
    public void onPageSelected(int position) {
        mCurPosition = position;
        originalIndexTv.setText(String.format("%s/%s", (position + 1), mImageSources.size()));
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onReached(boolean reached) {
        originalPagerView.isInterceptable(reached);
    }
}
