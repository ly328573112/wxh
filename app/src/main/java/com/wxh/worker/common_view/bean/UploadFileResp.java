package com.wxh.worker.common_view.bean;

import android.support.annotation.Keep;

import com.amap.api.services.nearby.UploadInfo;


@Keep
public class UploadFileResp {

    private UploadInfo uploadFile;

	public UploadFileResp() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UploadInfo getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(UploadInfo uploadFile) {
		this.uploadFile = uploadFile;
	}

	@Override
	public String toString() {
		return "UploadFileResp{" +
				"uploadFile=" + uploadFile +
				'}';
	}
}