package com.wxh.worker.common_view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.wxh.worker.common_view.bean.PhotoInfo;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class CommonUtilView implements CommonInterface {

    private RxAppCompatActivity activity;

    private CameraSelectView selectView;
    private CameraUtil cameraUtil;


    /***************************************************分享部分
     * *************************************************************/
    @Override
    public void showShareDialog(Bundle bundle) {
    }

    /***************************************************拍照部分
     * **************************************************************/

    private int COLUMN = 1;//图片可选数量
    private List<PhotoInfo> photoList = new ArrayList<>();// 照片uri列表
    private CameraListener cameraListener;

    @Override
    public void showCameraDiaog(CameraListener cameraListener) {
        if (selectView == null) {
            return;
        }
        this.cameraListener = cameraListener;
//        int optionalNumber = COLUMN - photoList.size();
        selectView.showCameraDialog(cameraUtil, COLUMN, View.VISIBLE);
        selectView.setCameraPotoListener(cameraPotoListener);

    }

    private CameraSelectView.CameraPotoListener cameraPotoListener = new CameraSelectView.CameraPotoListener() {
        @Override
        public void onResultPotoList(List<PhotoInfo> imgList) {
            if (imgList.size() == 0) {
                return;
            }
            photoList.addAll(imgList);
            cameraListener.requestCamera(imgList.get(0).photoUri);
        }
    };


    public void onHandleActivityResult(int requestCode, int resultCode, Intent data) {
        cameraUtil.onHandleActivityResult(requestCode, resultCode, data);
    }
}
