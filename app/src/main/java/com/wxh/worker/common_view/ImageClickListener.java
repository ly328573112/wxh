package com.wxh.worker.common_view;

/**
 * Created by hdb on 2017/7/11.
 */

public interface ImageClickListener {
    void openCamera(int position);

    void onDeleteImage(int position);
}

