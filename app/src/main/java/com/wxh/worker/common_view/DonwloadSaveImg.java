package com.wxh.worker.common_view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.utilCode.utils.ToastUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class DonwloadSaveImg {
    public static String FILEPATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    public static String FILENAME = "qianbao_";

    private  Context context;
    private  String filePath;
    private  String mSaveMessage = "失败";
    private  DonwloadImageListener donwloadImageListener;
    private  int saveStatus; // 0: success, 1: fail

    public DonwloadSaveImg(Context context){
        this.context = context;
    }

    public void donwloadImg(String filePaths, DonwloadImageListener donwloadImageListener) {
        this.donwloadImageListener = donwloadImageListener;
        filePath = filePaths;
        donwloadImageListener.startSave();
        new Thread(saveFileRunnable).start();
    }

    private Runnable saveFileRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.fitCenter();
                Bitmap myBitmap = Glide.with(context).asBitmap().load(filePath).apply(requestOptions).submit(1000, 1000).get();
                saveImageToGallery(myBitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
            messageHandler.sendMessage(messageHandler.obtainMessage());
        }
    };


    public void saveImageToGallery(Bitmap bmp) {
        // 首先保存图片
        File appDir = new File(Environment.getExternalStorageDirectory(), "Qianbao");
        if (!appDir.exists()) {
            appDir.mkdirs();
        }
        String fileName = FILENAME + System.currentTimeMillis() + ".jpg";
        File file = new File(appDir, fileName);
        try {
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            mSaveMessage = "保存失败!";
            saveStatus = 1;
            e.printStackTrace();
        } catch (IOException e) {
            mSaveMessage = "保存失败!";
            saveStatus = 1;
            e.printStackTrace();
        }

        // 其次把文件插入到系统图库
        try {
            MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), fileName, null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // 最后通知图库更新
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file" + "://" + file.getPath())));
        mSaveMessage = "保存成功!";
        saveStatus = 0;
    }

    private Handler messageHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(saveStatus == 0){
                donwloadImageListener.saveSuceess();
            } else {
                donwloadImageListener.saveError();
            }
            ToastUtils.showLongToast(mSaveMessage);
        }
    };

    public interface DonwloadImageListener {
        void startSave();

        void saveSuceess();

        void saveError();
    }
}