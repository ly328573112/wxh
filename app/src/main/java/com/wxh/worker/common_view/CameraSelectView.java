package com.wxh.worker.common_view;

import android.os.Environment;
import android.support.design.widget.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.wxh.worker.common_view.bean.PhotoInfo;
import com.common.utils.TimeUtils;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 开发者：LuoYi
 * Time: 2017 14:47 2017/9/21 09
 */

public class CameraSelectView implements CameraView, View.OnClickListener {
    public static String FILEPATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    public static String FILENAME = "wxh_";
    public static String SUFFIX = ".png";

    private final RxAppCompatActivity activity;
    private BottomSheetDialog sheetDialog;

    private static int COLUMN = 1;//图片可选数量
    private File file;
    private CameraUtil cameraUtil;
    private int optionalNumber;
    private List<PhotoInfo> photoList = new ArrayList<>();// 照片uri列表
    private boolean needResize = false;

    private CameraPotoListener cameraPotoListener;

    public CameraSelectView(RxAppCompatActivity evaluateView) {
        activity = evaluateView;
    }

    public void setCameraPotoListener(CameraPotoListener cameraPotoListener) {
        this.cameraPotoListener = cameraPotoListener;
    }

    public void setPictureCut(boolean needResize) {
        this.needResize = needResize;
    }

    @Override
    public void showCameraDialog(CameraUtil cameraUtil, int optionalNumber, int visibility) {
        this.cameraUtil = cameraUtil;
        this.optionalNumber = optionalNumber;
        this.COLUMN = optionalNumber;
        photoList.clear();

        View selectView = LayoutInflater.from(activity).inflate(R.layout.dialog_select, null);
        selectView.findViewById(R.id.btn_take_photo).setOnClickListener(this);
        selectView.findViewById(R.id.btn_pick_photo).setOnClickListener(this);
        selectView.findViewById(R.id.btn_pick_photo).setVisibility(visibility);
        selectView.findViewById(R.id.btn_cancel).setOnClickListener(this);
        sheetDialog = new BottomSheetDialog(activity);
        sheetDialog.setContentView(selectView);
        sheetDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_take_photo:
                if (optionalNumber == 0) {
                    ToastUtils.showShortToastSafe("抱歉！最多只能选择" + COLUMN + "张图片", 1000);
                } else {
                    file = new File(FILEPATH + "/" + FILENAME + TimeUtils.getSystemTime() + SUFFIX);
                    cameraUtil.openCamera(needResize, new CameraUtil.ResultCallback() {
                        @Override
                        public void getResult(List<File> saveFile) {
                            PhotoInfo info = new PhotoInfo(0, saveFile.get(0).toString(), true);
                            photoList.add(info);
                            cameraPotoListener.onResultPotoList(photoList);
                        }

                        @Override
                        public void onError(int code, String message) {
                            ToastUtils.showShortToastSafe(message);
                        }
                    }, file);
                }
                sheetDialog.dismiss();
                break;
            case R.id.btn_pick_photo:
                if (optionalNumber == 0) {
                    ToastUtils.showShortToastSafe("抱歉！最多只能选择" + COLUMN + "张图片", 1000);
                } else {
                    file = new File(FILEPATH + "/" + FILENAME + TimeUtils.getSystemTime() + SUFFIX);
                    cameraUtil.openPicture(needResize, 1, new CameraUtil.ResultCallback() {
                        @Override
                        public void getResult(List<File> saveFile) {
                            for (File file : saveFile) {
                                PhotoInfo info = new PhotoInfo(0, file.toString(), true);
                                photoList.add(info);
                            }
                            cameraPotoListener.onResultPotoList(photoList);
                        }

                        @Override
                        public void onError(int code, String message) {
                            ToastUtils.showShortToastSafe(message);
                        }
                    }, file);
                }
                sheetDialog.dismiss();
                break;
            case R.id.btn_cancel:
                sheetDialog.dismiss();
                break;
            default:
                break;
        }
    }

    public interface CameraPotoListener {

        void onResultPotoList(List<PhotoInfo> photoList);

    }
}
