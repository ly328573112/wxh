package com.wxh.worker.common_view.bean;

import android.support.annotation.Keep;

/**
 * 照片信息实体类
 */
@Keep
public class PhotoInfo {

    public int photoNumber;
    public String photoUri;// uri路径
    public boolean delete = false;// 是否处于可编辑状态
    public String failUrl;//上传失败的图片
    public String networkUrL;//网络图片地址Https

    @Keep
    public UploadFileResp resp;

    public PhotoInfo(int photoNumber, String photoUri, String networkUrL, boolean delete) {
        this.photoNumber = photoNumber;
        this.photoUri = photoUri;
        this.networkUrL = networkUrL;
        this.delete = delete;
    }


    public PhotoInfo(int photoNumber, String photoUri, boolean delete) {
        this.photoNumber = photoNumber;
        this.photoUri = photoUri;
        this.delete = delete;
    }

    @Override
    public String toString() {
        return "PhotoInfo{" +
                "photoNumber=" + photoNumber +
                ", photoUri='" + photoUri + '\'' +
                ", delete=" + delete +
                ", failUrl='" + failUrl + '\'' +
                ", resp=" + resp +
                '}';
    }
}