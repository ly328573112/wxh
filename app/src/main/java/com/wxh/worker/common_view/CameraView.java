package com.wxh.worker.common_view;


interface CameraView {

    void showCameraDialog(CameraUtil cameraUtil, int optionalNumber, int visibility);

}