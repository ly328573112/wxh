package com.wxh.worker.common_view.imgoriginal;

/**
 * 开发者：LuoYi
 * Time: 2017 15:51 2017/10/31 10
 */

public interface OriginalContract {

    interface DoOverrideSizeCallback {
        void onDone(int overrideW, int overrideH, boolean isTrue);
    }

    interface PagerAdapterView {
        void onReached(boolean reached);
    }

}
