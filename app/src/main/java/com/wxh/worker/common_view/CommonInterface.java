package com.wxh.worker.common_view;

import android.os.Bundle;

public interface CommonInterface {

    void showShareDialog(Bundle bundle);

    void showCameraDiaog(CameraListener cameraListener);

    interface CameraListener{

        void requestCamera(String uri);

    }

}
