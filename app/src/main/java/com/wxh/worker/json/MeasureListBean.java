package com.wxh.worker.json;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Keep
public class MeasureListBean implements Serializable {


    /**
     * measureId : 1159781031552499700
     * orderId : 1150796434747703300
     * homeName : vbhh
     * windowType : 2
     * width : 25
     * leftLength : 36
     * rightLength : 36
     * trackType : 3
     * levelType : 1
     * fixType : 1
     * ocType : 1
     * height : 0.0
     * openFlag : 1
     * fullFlag : 1
     * boxFlag : 0
     * boxWidth : 0
     * materialQuality : 3
     * linePosition : 2
     * remarks : 哈哈哈姐姐家
     * measureTime : 2019-08-09 18:58:40
     * status : 1
     * images : [{"url":"https://oss.aguogo.cn/images/20190809/15653483132474519.jpeg"},{"url":"https://oss.aguogo.cn/images/20190809/15653483133425351.jpeg"}]
     */

    public String measureId;//测量id
    public String orderId;//订单ID
    public String homeName;//房间名称
    public String windowType;//窗户类型
    public String windowNames;
    public String width;//宽度
    public String leftLength;//左侧边长
    public String rightLength;//右侧边长
    public String trackType;//轨道类型(1电动直轨2电动弯轨3电动L轨4电动升降直轨5电动升降弯轨)
    public String levelType;//单双轨类型1单轨2双轨
    public String fixType;//单双轨类型1顶装2侧装
    public String ocType;//开合方式类型1单开2双开
    public String heightFlag;
    public String height;//轨道安装高度，默认0小于3.5米，大于输入数值
    public String openFlag;//是否断开 1是 2否
    public String fullFlag;//是否满墙 1是 2否
    public String boxFlag;//是否有窗帘 1有 0无
    public String boxWidth;//窗帘盒宽度：厘米
    public String materialQuality;//安装面材质：1木板、2混凝土、3大理石、4石膏板
    public String linePosition;//电源线位置：1左侧、2右侧、3两侧
    public String remarks;//备注信息
    public String measureTime;//测量时间
    public String status;//1有效0删除
    public List<ImagesBean> images;//现场图片url
    public List<Object> arrayList = new ArrayList<>();//现场图片url

    @Keep
    public static class ImagesBean implements Serializable {
        /**
         * url : https://oss.aguogo.cn/images/20190809/15653483132474519.jpeg
         */

        public String url;

        @Override
        public String toString() {
            return "ImagesBean{" +
                    "url='" + url + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "MeasureListBean{" +
                "measureId='" + measureId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", homeName='" + homeName + '\'' +
                ", windowType='" + windowType + '\'' +
                ", windowNames='" + windowNames + '\'' +
                ", width='" + width + '\'' +
                ", leftLength='" + leftLength + '\'' +
                ", rightLength='" + rightLength + '\'' +
                ", trackType='" + trackType + '\'' +
                ", levelType='" + levelType + '\'' +
                ", fixType='" + fixType + '\'' +
                ", ocType='" + ocType + '\'' +
                ", heightFlag='" + heightFlag + '\'' +
                ", height='" + height + '\'' +
                ", openFlag='" + openFlag + '\'' +
                ", fullFlag='" + fullFlag + '\'' +
                ", boxFlag='" + boxFlag + '\'' +
                ", boxWidth='" + boxWidth + '\'' +
                ", materialQuality='" + materialQuality + '\'' +
                ", linePosition='" + linePosition + '\'' +
                ", remarks='" + remarks + '\'' +
                ", measureTime='" + measureTime + '\'' +
                ", status='" + status + '\'' +
                ", images=" + images +
                ", arrayList=" + arrayList +
                '}';
    }
}
