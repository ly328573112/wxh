package com.wxh.worker.json;

import android.support.annotation.Keep;

@Keep
public class HomeOrderBean {


    /**
     * dfOrderNum : 待服务订单数量
     * djOrderNum : 待接受订单
     * dyOrderNum : 待预约订单
     * dtOrderNum : 待提现订单
     */

    public String dfOrderNum = "0";
    public String djOrderNum = "0";
    public String dyOrderNum = "0";
    public String dtOrderNum = "0";


}
