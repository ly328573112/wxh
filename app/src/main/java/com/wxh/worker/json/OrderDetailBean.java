package com.wxh.worker.json;

import android.support.annotation.Keep;

import java.util.List;

@Keep
public class OrderDetailBean {


    /**
     * id : 1153295074670166016
     * orderNo : 201907229124
     * externalOrderno : null
     * provinceId : 9
     * cityId : 73
     * districtId : 721
     * address : 石龙路300号
     * addressDetail : 上海市上海市徐汇区石龙路300号
     * addressId : null
     * appointmentId : null
     * masterId : 1151020815830761500
     * storeId : 1155003607002398700
     * customerName : 海风23
     * customerPhone : 13472656224
     * totalAmt : 29500
     * otherAmt : 8800
     * appendAmt : 0
     * type : 2
     * picturesDTOS : []
     * urgentFlag : null
     * urgentAmt : null
     * windowNum : null
     * locationGps : 121.441527,31.156708
     * status : 4
     * remark :
     * description : null
     * createdTime : 2019-07-22 21:25:48
     * updatedTime : 2019-07-23 00:14:06
     * orderExtendDTOs :
     * acceptorId : null
     * acceptorName : null
     * acceptorPhone : null
     * acceptorWechatqq : null
     * acceptorReceiveTime : null
     * acceptorApplyTime : null
     * acceptorDealTime : null
     * acceptorCheckStatus : 0
     * appointmentTime : null
     */

    public String id;
    public String orderNo;//订单号
    public String externalOrderno;//外部订单号(天猫、京东等)
    public String provinceId;//省id
    public String cityId;//市id
    public String districtId;//区域id
    public String address;//具体地址(省市后面的地址)
    public String addressDetail;// 详细地址(包括省市区)
    public String addressId;//客户地址id(对应的具体人员)
    public String appointmentId;//预约id
    public String masterId;//师傅id
    public String storeId;//商家用户id(订单发起人员)
    public String customerName;//客户姓名
    public String customerPhone;//客户电话
    public String totalAmt;//订单总价(到分)
    public String otherAmt;//师傅提成(到分)
    public String appendAmt;//追加总费用(到分)
    public String type;//增值服务类型 1表示测量 2表示安装
    public String urgentFlag;//是否加急 0为正常 1为加急
    public String urgentAmt;//加急价格
    public String windowNum;//测量工单窗户数量
    public String orderMeasureRecordNum = "0";//测量数据 数量
    public String locationGps;//经纬度
    public String status;//状态 0=草稿 1->已撤销 2=>待派单 3已派单(待确定) 4待预约 5服务中 6待验收 7已冻结(验收未通过)
    public String remark;//
    public String description;//
    public String createdTime;//
    public String updatedTime;//
    public String acceptorId;//
    public String acceptorName;//
    public String acceptorPhone;//
    public String acceptorWechatqq;//
    public String acceptorReceiveTime;//
    public String acceptorApplyTime;//
    public String acceptorDealTime;//
    public String acceptorCheckStatus;//
    public String appointmentTime;//
    public List<PicturesDTOSBean> picturesDTOS;//现场图片url 多个用逗号隔开
    public List<OrderExtendDTOsBean> orderExtendDTOs;//

    @Keep
    public class PicturesDTOSBean{

        /**
         * url : https://oss.aguogo.cn/images/20190722/15638074967988683.png
         */

        public String url;

    }

    @Keep
    public class OrderExtendDTOsBean {
        /**
         * shopName : 次卧北
         * trackWidth : 5
         * trackNum : 1
         * windowHight : 1
         * trackType : 3
         * layerType : 1
         * curtainType : 0
         * oldFlag : 0
         * wallType : 1
         * orderTotalAmt : 19500
         */
        public int listIndex = 0;//列表编号
        public String shopName; //商户名称
        public String trackWidth;//轨道宽度
        public String trackNum;//轨道数量1单层2双层
        public String windowHight;//窗户高度(1、小于等于3.5米2、大于3.5米)
        public String trackType;//轨道类型(1电动直轨2电动弯轨3电动L轨4电动升降直轨5电动升降弯轨)
        public String layerType;//安装方式(1顶装2侧装)
        public String curtainType;//窗幔类型(0无1独立幔2一体幔)
        public String oldFlag;//是否拆旧0否1是
        public String wallType;//墙体材质(0普通1大理石2木质3混泥土)
        public String orderTotalAmt;//安装费用

        @Override
        public String toString() {
            return "OrderExtendDTOsBean{" +
                    "shopName='" + shopName + '\'' +
                    ", trackWidth='" + trackWidth + '\'' +
                    ", trackNum='" + trackNum + '\'' +
                    ", windowHight='" + windowHight + '\'' +
                    ", trackType='" + trackType + '\'' +
                    ", layerType='" + layerType + '\'' +
                    ", curtainType='" + curtainType + '\'' +
                    ", oldFlag='" + oldFlag + '\'' +
                    ", wallType='" + wallType + '\'' +
                    ", orderTotalAmt='" + orderTotalAmt + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "OrderDetailBean{" +
                "id='" + id + '\'' +
                ", orderNo='" + orderNo + '\'' +
                ", externalOrderno='" + externalOrderno + '\'' +
                ", provinceId='" + provinceId + '\'' +
                ", cityId='" + cityId + '\'' +
                ", districtId='" + districtId + '\'' +
                ", address='" + address + '\'' +
                ", addressDetail='" + addressDetail + '\'' +
                ", addressId='" + addressId + '\'' +
                ", appointmentId='" + appointmentId + '\'' +
                ", masterId='" + masterId + '\'' +
                ", storeId='" + storeId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerPhone='" + customerPhone + '\'' +
                ", totalAmt='" + totalAmt + '\'' +
                ", otherAmt='" + otherAmt + '\'' +
                ", appendAmt='" + appendAmt + '\'' +
                ", type='" + type + '\'' +
                ", urgentFlag='" + urgentFlag + '\'' +
                ", urgentAmt='" + urgentAmt + '\'' +
                ", windowNum='" + windowNum + '\'' +
                ", orderMeasureRecordNum='" + orderMeasureRecordNum + '\'' +
                ", locationGps='" + locationGps + '\'' +
                ", status='" + status + '\'' +
                ", remark='" + remark + '\'' +
                ", description='" + description + '\'' +
                ", createdTime='" + createdTime + '\'' +
                ", updatedTime='" + updatedTime + '\'' +
                ", acceptorId='" + acceptorId + '\'' +
                ", acceptorName='" + acceptorName + '\'' +
                ", acceptorPhone='" + acceptorPhone + '\'' +
                ", acceptorWechatqq='" + acceptorWechatqq + '\'' +
                ", acceptorReceiveTime='" + acceptorReceiveTime + '\'' +
                ", acceptorApplyTime='" + acceptorApplyTime + '\'' +
                ", acceptorDealTime='" + acceptorDealTime + '\'' +
                ", acceptorCheckStatus='" + acceptorCheckStatus + '\'' +
                ", appointmentTime='" + appointmentTime + '\'' +
                ", picturesDTOS=" + picturesDTOS +
                ", orderExtendDTOs=" + orderExtendDTOs +
                '}';
    }
}
