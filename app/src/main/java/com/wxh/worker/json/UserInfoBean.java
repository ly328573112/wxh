package com.wxh.worker.json;

import android.support.annotation.Keep;

@Keep
public class UserInfoBean {
    /**
     * id : 1164370391484612608
     * enabled : 1
     * username : 紫枫0
     * phone : 13167119257
     * type : 1
     * status : 3
     * userToken : 454e5d434d5e444f5241575a474a5d434a5a4d495e4356e7b580e69f9f4b46454a5d40485c444852454f53404d5a464d5b4c
     * userRoles : null
     */

    public String id;
    public String enabled;
    public String username;
    public String phone;
    public String type;
    public String status;
    public String userToken;
    public String userRoles;

//    public String id;
//    public String avatar;
//    public String email;
//    public String enabled;
//    public String password;
//    public String username;
//    public String deptId;
//    public String deptName;
//    public String phone;
//    public String contactPhone;
//    public String contactsQq;
//    public String postId;
//    public String postName;
//    public String roleId;
//    public String roleName;
//    public String type;
//    /**
//     * 状态（0正常 1删除 2待完善）
//     */
//    public String status;
//    public String createBy;
//    public String createDate;
//    public String updateBy;
//    public String updateDate;
//    public String lastPasswordResetTime;
//    public String lastLoginip;
//    public String remarks;
//    public String userToken;
//    public String userRoles;


}
