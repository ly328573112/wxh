package com.wxh.worker.json;

import android.support.annotation.Keep;

@Keep
public class OrderListBean {


    /**
     * id : 1153295076813455360
     * orderNo : 201907222451
     * externalOrderno : null
     * provinceId : 9
     * cityId : 73
     * districtId : 721
     * address : 石龙路300号
     * addressDetail : 上海市上海市徐汇区石龙路300号
     * addressId : null
     * appointmentId : null
     * masterId : 1151020815830761500
     * storeId : 1155003607002398700
     * customerName : 海风23
     * customerPhone : 13472656224
     * totalAmt : 29500
     * otherAmt : 8800
     * appendAmt : 0
     * type : 2
     * picturesDTOS : null
     * urgentFlag : null
     * urgentAmt : null
     * windowNum : null
     * locationGps : 121.441527,31.156708
     * status : 5
     * remark : 
     * description : null
     * createdTime : 2019-07-22 21:25:48
     * updatedTime : 2019-07-23 00:13:03
     * orderExtendDTOs : null
     * acceptorId : null
     * acceptorName : null
     * acceptorPhone : null
     * acceptorWechatqq : null
     * acceptorReceiveTime : null
     * acceptorApplyTime : null
     * acceptorDealTime : null
     * acceptorCheckStatus : 0
     * appointmentTime : null
     */

    public String id;
    public String orderNo;
    public String externalOrderno;
    public String provinceId;
    public String cityId;
    public String districtId;
    public String address;
    public String addressDetail;
    public String addressId;
    public String appointmentId;
    public String masterId;
    public String storeId;
    public String customerName;
    public String customerPhone;
    public String totalAmt;
    public String otherAmt;
    public String appendAmt;
    public String type;//1表示测量 2表示安装
    public String picturesDTOS;
    public String urgentFlag;
    public String urgentAmt;
    public String windowNum;
    public String locationGps;
    public String status;
    public String remark;
    public String description;
    public String createdTime;
    public String updatedTime;
    public String orderExtendDTOs;
    public String acceptorId;
    public String acceptorName;
    public String acceptorPhone;
    public String acceptorWechatqq;
    public String acceptorReceiveTime;
    public String acceptorApplyTime;
    public String acceptorDealTime;
    public String acceptorCheckStatus;
    public String appointmentTime;

    @Override
    public String toString() {
        return "OrderListBean{" +
                "id='" + id + '\'' +
                ", orderNo='" + orderNo + '\'' +
                ", externalOrderno='" + externalOrderno + '\'' +
                ", provinceId='" + provinceId + '\'' +
                ", cityId='" + cityId + '\'' +
                ", districtId='" + districtId + '\'' +
                ", address='" + address + '\'' +
                ", addressDetail='" + addressDetail + '\'' +
                ", addressId='" + addressId + '\'' +
                ", appointmentId='" + appointmentId + '\'' +
                ", masterId='" + masterId + '\'' +
                ", storeId='" + storeId + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerPhone='" + customerPhone + '\'' +
                ", totalAmt='" + totalAmt + '\'' +
                ", otherAmt='" + otherAmt + '\'' +
                ", appendAmt='" + appendAmt + '\'' +
                ", type='" + type + '\'' +
                ", picturesDTOS='" + picturesDTOS + '\'' +
                ", urgentFlag='" + urgentFlag + '\'' +
                ", urgentAmt='" + urgentAmt + '\'' +
                ", windowNum='" + windowNum + '\'' +
                ", locationGps='" + locationGps + '\'' +
                ", status='" + status + '\'' +
                ", remark='" + remark + '\'' +
                ", description='" + description + '\'' +
                ", createdTime='" + createdTime + '\'' +
                ", updatedTime='" + updatedTime + '\'' +
                ", orderExtendDTOs='" + orderExtendDTOs + '\'' +
                ", acceptorId='" + acceptorId + '\'' +
                ", acceptorName='" + acceptorName + '\'' +
                ", acceptorPhone='" + acceptorPhone + '\'' +
                ", acceptorWechatqq='" + acceptorWechatqq + '\'' +
                ", acceptorReceiveTime='" + acceptorReceiveTime + '\'' +
                ", acceptorApplyTime='" + acceptorApplyTime + '\'' +
                ", acceptorDealTime='" + acceptorDealTime + '\'' +
                ", acceptorCheckStatus='" + acceptorCheckStatus + '\'' +
                ", appointmentTime='" + appointmentTime + '\'' +
                '}';
    }
}
