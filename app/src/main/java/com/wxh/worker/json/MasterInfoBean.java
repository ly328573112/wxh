package com.wxh.worker.json;

import android.support.annotation.Keep;

import java.util.List;

@Keep
public class MasterInfoBean {

    public String masterAddress;
    public String emergencyContactsName;
    public String emergencyContactsPhone;
    public String emergencyContactsRelation;
    public List<ServiceAddressDTOListBean> serviceAddressDTOList;

    @Keep
    public class ServiceAddressDTOListBean {
        public String serviceRegion;
    }
}
