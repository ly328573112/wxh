package com.wxh.worker.json;

import android.support.annotation.Keep;

@Keep
public class ADBean {


    /**
     * id : 1
     * positionId : 0
     * mediaType : 0
     * adName : 该条广告记录的广告名称
     * adLink : https://www.hao123.com/
     * startTime : 2019-07-15 14:32:53
     * endTime : 2019-07-20 14:32:58
     * linkMan : 广告联系人
     * linkEmail : 广告联系人的邮箱
     * linkPhone : 111111
     * clickCount : 0
     * enabled : 1
     * adCode : https://t12.baidu.com/it/u=271720925,2472875224&fm=76
     * adRemark : 点击次数统计
     */

    public String id;
    public int positionId;
    public String mediaType;
    public String adName;
    public String adLink;
    public String startTime;
    public String endTime;
    public String linkMan;
    public String linkEmail;
    public String linkPhone;
    public int clickCount;
    public String enabled;
    public String adCode;
    public String adRemark;

}
