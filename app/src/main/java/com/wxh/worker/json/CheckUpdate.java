package com.wxh.worker.json;

import android.support.annotation.Keep;

@Keep
public class CheckUpdate {
    private String version_desc;
    private String latest_version;// 最新版本号V0.0.1
    private String force_update_version;// 强制更新版本号，该版本(含)及以下版本需强制更新为最新版本V0.0.0
    private String size;// 安装包大小（MB）
    private String update_log;// 更新说明

    public String getVersion_desc() {
        return version_desc;
    }

    public void setVersion_desc(String version_desc) {
        this.version_desc = version_desc;
    }

    public String getLatest_version() {
        return latest_version;
    }

    public void setLatest_version(String latest_version) {
        this.latest_version = latest_version;
    }

    public String getForce_update_version() {
        return force_update_version;
    }

    public void setForce_update_version(String force_update_version) {
        this.force_update_version = force_update_version;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUpdate_log() {
        return update_log;
    }

    public void setUpdate_log(String update_log) {
        this.update_log = update_log;
    }

}