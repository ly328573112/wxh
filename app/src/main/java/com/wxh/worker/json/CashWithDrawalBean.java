package com.wxh.worker.json;

import android.support.annotation.Keep;

@Keep
public class CashWithDrawalBean {

    public String id;
    public String orderNo;
    public String type;
    public int otherAmt;
    public String successTime;
    public String addressDetail;
    public String status = "0";

    @Override
    public String toString() {
        return "CashWithDrawalBean{" +
                "id='" + id + '\'' +
                ", orderNo='" + orderNo + '\'' +
                ", type='" + type + '\'' +
                ", otherAmt=" + otherAmt +
                ", successTime='" + successTime + '\'' +
                ", addressDetail='" + addressDetail + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
