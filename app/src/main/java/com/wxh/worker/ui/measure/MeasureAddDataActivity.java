package com.wxh.worker.ui.measure;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.common.config.Global;
import com.wxh.worker.event.MeasureAddEvent;
import com.wxh.worker.event.OriginalDeleteEvent;
import com.wxh.worker.event.WindowTypeEvent;
import com.common.mvp.BaseMvpActivity;
import com.common.utils.LogUtil;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.json.MeasureListBean;
import com.wxh.worker.ui.measure.module_view.OtherView;
import com.wxh.worker.ui.measure.module_view.RemarksImageView;
import com.wxh.worker.ui.measure.module_view.TrackView;
import com.wxh.worker.ui.window_type.WindowTypeActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;

public class MeasureAddDataActivity extends BaseMvpActivity<MeasureView, MeasurePresent> implements MeasureView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.et_add_shop_name)
    EditText etAddShopName;
    @BindView(R.id.ll_select_window_type)
    LinearLayout llSelectWindowType;
    @BindView(R.id.tv_window_value)
    TextView tvWindowValue;
    @BindView(R.id.ll_measure_layout)
    LinearLayout llMeasureLayout;
    @BindView(R.id.tv_button_red_e)
    TextView tvButtonRedE;

    private LoadingDialog mLoadingDialog;
    private TrackView trackView;
    private OtherView otherView;
    private RemarksImageView remarksImageView;

    private MeasureListBean measureListBean;

    public static void startActivity(Activity activity, String orderId) {
        Intent intent = new Intent(activity, MeasureAddDataActivity.class);
        intent.putExtra("orderId", orderId);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return R.layout.activity_measure_add_data;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarBack.setOnClickListener(this);
        toolbarTitle.setText("添加测量数据");
        tvButtonRedE.setOnClickListener(this);
        tvButtonRedE.setVisibility(View.VISIBLE);
        llSelectWindowType.setOnClickListener(this);

        trackView = new TrackView(this, trackLisener);
        llMeasureLayout.addView(trackView.getView());
        otherView = new OtherView(this, otherListener);
        llMeasureLayout.addView(otherView.getView());
        remarksImageView = new RemarksImageView(this, remarksListener);
        llMeasureLayout.addView(remarksImageView.getView());
        llMeasureLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                // TODO Auto-generated method stub
                llMeasureLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                LogUtil.i("Height:" + llMeasureLayout.getMeasuredHeight());
            }
        });

        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }
        measureListBean = new MeasureListBean();
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        Intent intent = getIntent();
        measureListBean.orderId = intent.getStringExtra("orderId");

        MeasureListBean measureList = Global.getMeasureList();
        if (measureList != null && TextUtils.equals(measureList.orderId, measureListBean.orderId)) {
            etAddShopName.setText(measureList.homeName);
            tvWindowValue.setText(measureList.windowNames);
            trackView.setMeasureListBean(measureList);
            otherView.setMeasureListBean(measureList);
            remarksImageView.setMeasureListBean(measureList);
        }
    }

    private TrackView.TrackLisener trackLisener = new TrackView.TrackLisener() {
        @Override
        public void onWidth(String widths) {
            measureListBean.width = widths;
        }

        @Override
        public void onLeftLength(String leftLengths) {
            measureListBean.leftLength = leftLengths;
        }

        @Override
        public void onRightLength(String rightLengths) {
            measureListBean.rightLength = rightLengths;
        }

        @Override
        public void onTrackType(String trackTypes) {
            measureListBean.trackType = trackTypes;
        }

        @Override
        public void onLevelType(String levelTypes) {
            measureListBean.levelType = levelTypes;
        }

        @Override
        public void onFixType(String fixTypes) {
            measureListBean.fixType = fixTypes;
        }

        @Override
        public void onOcType(String ocTypes) {
            measureListBean.ocType = ocTypes;
        }

        @Override
        public void onHeightFlag(String heightFlags, String heights) {
            measureListBean.heightFlag = heightFlags;
        }
    };

    private OtherView.OtherListener otherListener = new OtherView.OtherListener() {
        @Override
        public void onOpenFlag(String openFlags) {
            measureListBean.openFlag = openFlags;
        }

        @Override
        public void onFullFlag(String fullFlags) {
            measureListBean.fullFlag = fullFlags;
        }

        @Override
        public void onBoxFlag(String boxFlags, String boxWidths) {
            measureListBean.boxFlag = boxFlags;
        }

        @Override
        public void onMaterialQuality(String materialQualitys) {
            measureListBean.materialQuality = materialQualitys;
        }

        @Override
        public void onLinePosition(String linePositions) {
            measureListBean.linePosition = linePositions;
        }
    };

    private RemarksImageView.RemarksListener remarksListener = new RemarksImageView.RemarksListener() {
        @Override
        public void onRemarksData(String remarkss) {
            measureListBean.remarks = remarkss;
        }

        @Override
        public void onImageData(String url) {
            JSONObject urlObject = new JSONObject();
            urlObject.put("url", url);
            measureListBean.arrayList.add(urlObject);
        }

        @Override
        public void onDelete(String networkUrL) {
            for (int i = 0; i < measureListBean.arrayList.size(); i++) {
                JSONObject jsonObject = JSONObject.parseObject(measureListBean.arrayList.get(i).toString());
                String url = jsonObject.getString("url");
                if (TextUtils.equals(url, networkUrL)) {
                    measureListBean.arrayList.remove(i);
                }
            }
            networkUrL = networkUrL.replaceAll("https://oss.aguogo.cn/", "");
            getPresenter().deleteImg(networkUrL);
        }
    };

    @Override
    public void onMeasureList(List<MeasureListBean> measureListBeanList) {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showLongToastSafe("添加测量数据成功");
//        Global.setMeasureList(new MeasureListBean());
        EventBus.getDefault().post(new MeasureAddEvent());
        finish();
    }

    @Override
    public void onFail() {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showLongToastSafe("添加测量数据失败");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
//                setDataCache();
                finish();
                break;
            case R.id.ll_select_window_type:
                if (TextUtils.isEmpty(measureListBean.windowType)) {
                    WindowTypeActivity.startActivity(this, 0);
                } else {
                    WindowTypeActivity.startActivity(this, Integer.parseInt(measureListBean.windowType));
                }
                break;
            case R.id.tv_button_red_e:
                measureListBean.homeName = etAddShopName.getText().toString().trim();
                measureListBean.width = trackView.getWidth();
                measureListBean.leftLength = trackView.getLeftLength();
                measureListBean.rightLength = trackView.getRightLength();
                measureListBean.height = trackView.getZiDingYi();
                measureListBean.boxWidth = otherView.getBoxWidth();
                measureListBean.remarks = remarksImageView.getRemarks();
                boolean checkMeasure = checkMeasure();
                if (checkMeasure) {
                    if (!mLoadingDialog.isVisible()) {
                        mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                    }
                    setMeasureData();
                }
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
//                setDataCache();
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void setDataCache() {
        MeasureListBean measureList = Global.getMeasureList();
        if (measureList != null && TextUtils.equals(measureList.orderId, measureListBean.orderId) || TextUtils.isEmpty(measureList.orderId)) {
            measureListBean.homeName = etAddShopName.getText().toString().trim();
            measureListBean.width = trackView.getWidth();
            measureListBean.leftLength = trackView.getLeftLength();
            measureListBean.rightLength = trackView.getRightLength();
            measureListBean.height = trackView.getZiDingYi();
            measureListBean.boxWidth = otherView.getBoxWidth();
            measureListBean.remarks = remarksImageView.getRemarks();
            Global.setMeasureList(measureListBean);
        }
    }

    private boolean checkMeasure() {
        if (TextUtils.isEmpty(measureListBean.homeName)) {
            ToastUtils.showLongToastSafe("请输入房间名称");
            return false;
        }
        if (TextUtils.isEmpty(measureListBean.windowType) || TextUtils.isEmpty(tvWindowValue.getText())) {
            ToastUtils.showLongToastSafe("请选择窗户类型");
            return false;
        }
        if (!TextUtils.isEmpty(measureListBean.windowType)) {
            switch (measureListBean.windowType) {
                case "1":
                case "2":
                case "3":
                case "4":
                    if (TextUtils.isEmpty(measureListBean.width)) {
                        ToastUtils.showLongToastSafe("请输入轨道宽度");
                        return false;
                    }
                    if (TextUtils.isEmpty(measureListBean.leftLength)) {
                        ToastUtils.showLongToastSafe("请输入左侧边长");
                        return false;
                    }
                    if (TextUtils.isEmpty(measureListBean.rightLength)) {
                        ToastUtils.showLongToastSafe("请输入右侧边长");
                        return false;
                    }
                    break;
                case "5":
                case "7":
                    if (TextUtils.isEmpty(measureListBean.width)) {
                        ToastUtils.showLongToastSafe("请输入轨道宽度");
                        return false;
                    }
                    if (TextUtils.isEmpty(measureListBean.rightLength)) {
                        ToastUtils.showLongToastSafe("请输入右侧边长");
                        return false;
                    }
                    break;
                case "6":
                case "8":
                    if (TextUtils.isEmpty(measureListBean.width)) {
                        ToastUtils.showLongToastSafe("请输入轨道宽度");
                        return false;
                    }
                    if (TextUtils.isEmpty(measureListBean.leftLength)) {
                        ToastUtils.showLongToastSafe("请输入左侧边长");
                        return false;
                    }
                    break;
                case "9":
                    if (TextUtils.isEmpty(measureListBean.width)) {
                        ToastUtils.showLongToastSafe("请输入轨道宽度");
                        return false;
                    }
                    break;
            }
        }
        if (TextUtils.isEmpty(measureListBean.trackType)) {
            ToastUtils.showLongToastSafe("请选择轨道类型");
            return false;
        }
        if (TextUtils.isEmpty(measureListBean.levelType)) {
            ToastUtils.showLongToastSafe("请选择单双轨");
            return false;
        }
        if (TextUtils.isEmpty(measureListBean.fixType)) {
            ToastUtils.showLongToastSafe("请选择安装方式");
            return false;
        }
        if (TextUtils.isEmpty(measureListBean.ocType)) {
            ToastUtils.showLongToastSafe("请选择开合方式");
            return false;
        }
        if (TextUtils.isEmpty(measureListBean.heightFlag)) {
            ToastUtils.showLongToastSafe("请选择轨道安装高度");
            return false;
        }
        if (!TextUtils.isEmpty(measureListBean.heightFlag) && TextUtils.equals(measureListBean.heightFlag, "1") && TextUtils.isEmpty(measureListBean.height)) {
            ToastUtils.showLongToastSafe("请输入轨道安装高度");
            return false;
        }
        if (TextUtils.isEmpty(measureListBean.openFlag)) {
            ToastUtils.showLongToastSafe("请选择是否断开");
            return false;
        }
        if (TextUtils.isEmpty(measureListBean.fullFlag)) {
            ToastUtils.showLongToastSafe("请选择是否满墙");
            return false;
        }
        if (TextUtils.isEmpty(measureListBean.boxFlag)) {
            ToastUtils.showLongToastSafe("请选择是否有无窗帘盒");
            return false;
        }
        if (!TextUtils.isEmpty(measureListBean.boxFlag) && TextUtils.equals(measureListBean.boxFlag, "1") && TextUtils.isEmpty(measureListBean.boxWidth)) {
            ToastUtils.showLongToastSafe("请输入窗帘盒宽度");
            return false;
        }
        if (TextUtils.isEmpty(measureListBean.materialQuality)) {
            ToastUtils.showLongToastSafe("请选择安装面材质");
            return false;
        }
        if (TextUtils.isEmpty(measureListBean.linePosition)) {
            ToastUtils.showLongToastSafe("请选择电源线位置");
            return false;
        }
        if (measureListBean.arrayList.size() == 0) {
            ToastUtils.showLongToastSafe("请上传安装效果图");
            return false;
        }
        return true;
    }

    private void setMeasureData() {
        JSONObject requestAttributes = new JSONObject();
        requestAttributes.put("orderId", measureListBean.orderId);
        requestAttributes.put("homeName", measureListBean.homeName);
        requestAttributes.put("windowType", measureListBean.windowType);
        requestAttributes.put("width", measureListBean.width);
        requestAttributes.put("leftLength", measureListBean.leftLength);
        requestAttributes.put("rightLength", measureListBean.rightLength);
        requestAttributes.put("trackType", measureListBean.trackType);
        requestAttributes.put("levelType", measureListBean.levelType);
        requestAttributes.put("fixType", measureListBean.fixType);
        requestAttributes.put("ocType", measureListBean.ocType);
        requestAttributes.put("heightFlag", measureListBean.heightFlag);
        requestAttributes.put("height", measureListBean.height);
        requestAttributes.put("openFlag", measureListBean.openFlag);
        requestAttributes.put("fullFlag", measureListBean.fullFlag);
        requestAttributes.put("boxFlag", measureListBean.boxFlag);
        requestAttributes.put("boxWidth", measureListBean.boxWidth);
        requestAttributes.put("materialQuality", measureListBean.materialQuality);
        requestAttributes.put("linePosition", measureListBean.linePosition);
        requestAttributes.put("remarks", measureListBean.remarks);
        requestAttributes.put("images", measureListBean.arrayList);
        String dataString = requestAttributes.toString();
        getPresenter().uploadMeasureRecord(dataString);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        remarksImageView.onActivityResult(requestCode, resultCode, data);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(OriginalDeleteEvent event) {
        remarksImageView.onDeleteImage(event.curPosition);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(WindowTypeEvent event) {
        measureListBean.windowType = event.windowType + "";
        tvWindowValue.setText(event.windowName);
        measureListBean.windowNames = event.windowName;
        switch (event.windowType) {
            case 1:
            case 2:
            case 3:
            case 4:
                trackView.setEdittextVisibility(View.VISIBLE, View.VISIBLE, View.VISIBLE);
                break;
            case 5:
            case 7:
                trackView.setEdittextVisibility(View.VISIBLE, View.GONE, View.VISIBLE);
                break;
            case 6:
            case 8:
                trackView.setEdittextVisibility(View.VISIBLE, View.VISIBLE, View.GONE);
                break;
            case 9:
                trackView.setEdittextVisibility(View.VISIBLE, View.GONE, View.GONE);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
