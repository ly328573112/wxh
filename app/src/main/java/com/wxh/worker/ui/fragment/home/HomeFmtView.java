package com.wxh.worker.ui.fragment.home;

import com.utilCode.base.mvp.MvpView;
import com.wxh.worker.json.ADBean;
import com.wxh.worker.json.HomeOrderBean;

import java.util.List;

public interface HomeFmtView extends MvpView {

    void onOrderListSuccess(List<HomeOrderBean> homeOrderBeans);

    void onADSuccess(List<ADBean> adBeanList);

    void onFail();

    interface PerformClickListener{

        void onPerformClick(String orderStatus);

    }

}
