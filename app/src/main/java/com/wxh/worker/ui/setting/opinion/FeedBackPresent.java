package com.wxh.worker.ui.setting.opinion;

import android.support.annotation.NonNull;

import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.common.utils.LogUtil;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class FeedBackPresent extends RxMvpPresenter<FeedBackView> {

    MyApi mMyApi;

    @Inject
    public FeedBackPresent(MyApi mMyApi) {
        this.mMyApi = mMyApi;
    }

    public void feedbackMessages(String message) {
//        JSONObject requestAttributes = new JSONObject();
//        try {
//            requestAttributes.put("type", "0");//反馈问题类型:0其它1功能异常2体验问题3新功能建议
//            requestAttributes.put("message", message);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), requestAttributes.toString());
        mMyApi.feedbackMessages(message, "0")
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        LogUtil.d(data.toString());
                        if (checkJsonCode(data, false)) {
                            getView().onSuccess();
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }
}
