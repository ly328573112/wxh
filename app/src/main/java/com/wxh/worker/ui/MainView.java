package com.wxh.worker.ui;

import com.utilCode.base.mvp.MvpView;
import com.wxh.worker.json.CheckUpdate;

public interface MainView extends MvpView {


    void OnVersionUpdate(CheckUpdate result);

    void OnVersionUpdateError();
}
