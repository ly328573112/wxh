package com.wxh.worker.ui.measure;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;
import com.wxh.worker.json.MeasureListBean;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeasureListAdapter extends BaseRecycleViewAdapter<MeasureListBean> {

    private MeasureClickListener measureClickListener;

    public MeasureListAdapter(Context context, MeasureClickListener measureClickListener) {
        super(context);
        this.measureClickListener = measureClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, MeasureListBean item) {
        if (holder instanceof MeasureHolder) {
            MeasureHolder measureHolder = (MeasureHolder) holder;
            measureHolder.tvItemShopName.setText(item.homeName);
            measureHolder.tvItemTime.setText("测量时间：" + item.measureTime);
            measureHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (measureClickListener != null) {
                        measureClickListener.onMeasureItemClick(item);
                    }
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new MeasureHolder(View.inflate(mContext, R.layout.item_measure_view, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class MeasureHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_item_img)
        ImageView ivItemImg;
        @BindView(R.id.tv_item_shop_name)
        TextView tvItemShopName;
        @BindView(R.id.tv_item_time)
        TextView tvItemTime;

        MeasureHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public interface MeasureClickListener {
        void onMeasureItemClick(MeasureListBean item);
    }

}
