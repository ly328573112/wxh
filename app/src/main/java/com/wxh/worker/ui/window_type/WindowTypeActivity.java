package com.wxh.worker.ui.window_type;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wxh.worker.event.WindowTypeEvent;
import com.common.mvp.BaseMvpActivity;
import com.wxh.worker.R;
import com.wxh.worker.ui.window_type.bean.WindowTypeBean;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;

public class WindowTypeActivity extends BaseMvpActivity<WindowView, WindowPresent> implements WindowView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.rv_window_type)
    RecyclerView rvWindowType;

    private WindowAdapter windowAdapter;
    private int windowType = 0;

    public static void startActivity(Activity activity, int windowType) {
        Intent intent = new Intent(activity, WindowTypeActivity.class);
        intent.putExtra("windowType", windowType);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_window_type;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarBack.setOnClickListener(this);
        toolbarTitle.setText("选择窗户类型");

        GridLayoutManager layoutManager = new GridLayoutManager(this, 3, LinearLayoutManager.VERTICAL, false);
        rvWindowType.setLayoutManager(layoutManager);
        rvWindowType.setHasFixedSize(true);
        windowAdapter = new WindowAdapter(this, windowClickListener);
        rvWindowType.setAdapter(windowAdapter);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        windowType = intent.getIntExtra("windowType", 0);
        List<WindowTypeBean> windowList = WindowTypeBean.getWindowList();
        if(windowType > 0){
            for (int i = 0; i < windowList.size(); i++){
                if(windowList.get(i).windowIndex == windowType){
                    windowList.get(i).windowSeelct = true;
                }
            }
        }
        windowAdapter.resetData(windowList);
    }


    private WindowAdapter.WindowClickListener windowClickListener = new WindowAdapter.WindowClickListener() {
        @Override
        public void onItemWindowType(WindowTypeBean item) {
            EventBus.getDefault().post(new WindowTypeEvent(item.windowIndex, item.windowName));
            finish();
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
        }
    }
}
