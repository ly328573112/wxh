package com.wxh.worker.ui.fragment.order;

import android.support.annotation.NonNull;

import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.common.utils.LogUtil;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.json.OrderListBean;

import java.util.List;

import javax.inject.Inject;

public class OrderFmtPresent extends RxMvpPresenter<OrderFmtView> {

    MyApi mMyApi;

    @Inject
    public OrderFmtPresent(MyApi mMyApi) {
        this.mMyApi = mMyApi;
    }

    public void getListOrders(String orderStatus, int pageNum, int pageSize) {
        /**
         * 0 草稿
         * 1 商户已撤回
         * 2 已发布-待派单
         * 3 已指定师傅-待接收
         * 4 师傅接单-待预约
         * 5 已预约-服务中
         * 6 师傅申请验收-待验收
         * 7 商户验收-未通过
         * 8 验收通过已完成-未提现
         * 9 商户申请-二次上门
         * 10 平台删除订单
         * 11 后台撤销
         * 12 验收通过已完成-已提现
         * 13 平台验收冻结-未通过
         * 14 商户删除订单
         */
        mMyApi.getListOrders(orderStatus, pageNum, pageSize)
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<OrderListBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<OrderListBean>> data) {
                        LogUtil.d(data.toString());
                        if (checkJsonCode(data, false) && data.getResult() != null) {
                            getView().onOrderListData(data.getResult(), data.getPage().pageNum);
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

}
