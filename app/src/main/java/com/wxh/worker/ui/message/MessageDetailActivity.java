package com.wxh.worker.ui.message;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.mvp.BaseMvpActivity;
import com.wxh.worker.R;

import butterknife.BindView;

public class MessageDetailActivity extends BaseMvpActivity<MessageView, MessagePresent> implements MessageView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.tv_msg_title)
    TextView tvMsgTitle;
    @BindView(R.id.tv_msg_content)
    TextView tvMsgContent;

    private String msgTitle;
    private String msgContent;

    public static void startActivity(Activity activity, String msgTitle, String msgContent) {
        Intent intent = new Intent(activity, MessageDetailActivity.class);
        intent.putExtra("msgTitle", msgTitle);
        intent.putExtra("msgContent", msgContent);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_message_detail;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }


    @Override
    protected void setupView() {
        toolbarBack.setOnClickListener(this);
        toolbarTitle.setText("消息详情");
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        msgTitle = intent.getStringExtra("msgTitle");
        msgContent = intent.getStringExtra("msgContent");
        tvMsgTitle.setText(msgTitle);
        tvMsgContent.setText(msgContent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
        }
    }
}
