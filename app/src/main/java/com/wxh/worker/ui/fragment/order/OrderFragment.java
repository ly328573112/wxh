package com.wxh.worker.ui.fragment.order;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.wxh.worker.event.CheckAcceptanceEvent;
import com.wxh.worker.event.RefreshOrderEvent;
import com.common.mvp.BaseMvpFragment;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;
import com.wxh.worker.json.OrderListBean;
import com.wxh.worker.ui.HomeActivity;
import com.wxh.worker.ui.fragment.order.bean.OrderLabelBean;
import com.wxh.worker.ui.order.OrderDetailActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;

public class OrderFragment extends BaseMvpFragment<OrderFmtView, OrderFmtPresent> implements OrderFmtView {

    @BindView(R.id.rv_order_top_gridview)
    RecyclerView rvOrderTopGridview;
    @BindView(R.id.srl)
    SwipeRefreshLayout mSrl;
    @BindView(R.id.rv_order_list)
    RecyclerView rvOrderList;

    private OrderAdapter orderAdapter;
    private OrderLabelAdapter labelAdapter;
    private List<OrderLabelBean> orderLabelBeans;
    private LoadingDialog mLoadingDialog;

    private String orderStatus = "3";
    private int pageNum = 1;
    private int pageSize = 20;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_order;
    }

    @Override
    protected void initInject() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected void setupView(View rootView) {
        mSrl.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.c_E60000));
        mSrl.setProgressViewOffset(true, -20, 100);
        mSrl.setOnRefreshListener(refreshListener);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 5, LinearLayoutManager.VERTICAL, false);
        rvOrderTopGridview.setLayoutManager(layoutManager);
        labelAdapter = new OrderLabelAdapter(getContext(), labelClickListener);
        rvOrderTopGridview.setAdapter(labelAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvOrderList.setLayoutManager(linearLayoutManager);
        orderAdapter = new OrderAdapter(getContext(), orderClickListener);
        rvOrderList.setAdapter(orderAdapter);
        orderAdapter.setOnLoadMoreListener(onLoadMoreListener, rvOrderList);

        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        orderStatus = HomeActivity.oStatus;
        orderLabelBeans = OrderLabelBean.setLabelData(orderStatus);
        labelAdapter.resetData(orderLabelBeans);
        if (!mLoadingDialog.isVisible()) {
            mLoadingDialog.show(getFragmentManager(), "loadingdialog");
        }
        pageNum = 1;
        getPresenter().getListOrders(orderStatus, pageNum, pageSize);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener(){

        @Override
        public void onRefresh() {
            pageNum = 1;
            getPresenter().getListOrders(orderStatus, pageNum, pageSize);
        }
    };

    private BaseRecycleViewAdapter.OnLoadMoreListener onLoadMoreListener = new BaseRecycleViewAdapter.OnLoadMoreListener(){

        @Override
        public void onLoadMore() {
            getPresenter().getListOrders(orderStatus, pageNum, pageSize);
        }
    };

    private OrderAdapter.OrderClickListener orderClickListener = new OrderAdapter.OrderClickListener() {
        @Override
        public void onOrderItemClick(OrderListBean item) {
            OrderDetailActivity.startActivity(getActivity(), item.id, orderStatus);
        }
    };

    private OrderLabelAdapter.LabelClickListener labelClickListener = new OrderLabelAdapter.LabelClickListener(){

        @Override
        public void onLabelItemClick(OrderLabelBean item) {
            labelAdapter.getData().clear();
            for (OrderLabelBean labelBean : orderLabelBeans){
                labelBean.labelSelect = false;
                if(labelBean.labelIndex == item.labelIndex){
                    labelBean.labelSelect = true;
                }
            }
            labelAdapter.resetData(orderLabelBeans);
            if (!mLoadingDialog.isVisible()) {
                mLoadingDialog.show(getFragmentManager(), "loadingdialog");
            }
            orderStatus = item.orderStatus;
            pageNum = 1;
            getPresenter().getListOrders(orderStatus, pageNum, pageSize);
        }
    };

    @Override
    public void onOrderListData(List<OrderListBean> orderListBeans, int pageNumber) {
        if (mSrl.isRefreshing()) {
            mSrl.setRefreshing(false);
        }
        mLoadingDialog.dismissAllowingStateLoss();
        if (pageNum == 1 && orderListBeans == null || orderListBeans.size() == 0) {
            orderAdapter.resetData(orderListBeans);
            orderAdapter.setEmptyView(R.layout.empty_view);
            orderAdapter.loadMoreEnd(false);
            orderAdapter.loadMoreComplete();
        } else if(orderListBeans != null && orderListBeans.size() < pageSize) {
            if( pageNum == 1 ){
                orderAdapter.resetData(orderListBeans);
            } else {
                orderAdapter.addData(orderListBeans);
            }
            orderAdapter.loadMoreEnd(false);
            pageNum = pageNumber;
            orderAdapter.loadMoreComplete();
        } else {
            if( pageNum == 1 ){
                orderAdapter.resetData(orderListBeans);
            } else {
                orderAdapter.addData(orderListBeans);
            }
            pageNum = pageNumber + 1;
        }
    }

    @Override
    public void onFail() {
        if (mSrl.isRefreshing()) {
            mSrl.setRefreshing(false);
        }
        mLoadingDialog.dismissAllowingStateLoss();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CheckAcceptanceEvent event) {
        pageNum = 1;
        getPresenter().getListOrders(orderStatus, pageNum, pageSize);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RefreshOrderEvent event) {
        this.orderStatus = event.orderStatus;
        orderLabelBeans = OrderLabelBean.setLabelData(orderStatus);
        labelAdapter.resetData(orderLabelBeans);
        pageNum = 1;
        if (!mLoadingDialog.isVisible()) {
            mLoadingDialog.show(getFragmentManager(), "loadingdialog");
        }
        getPresenter().getListOrders(orderStatus, pageNum, pageSize);
    }
}
