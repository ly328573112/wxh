package com.wxh.worker.ui.setting.about;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.mvp.BaseMvpActivity;
import com.wxh.worker.BuildConfig;
import com.wxh.worker.R;
import com.wxh.worker.ui.setting.SettingActivity;
import com.wxh.worker.ui.setting.contact.ContactActivity;

import butterknife.BindView;

public class AboutActivity extends BaseMvpActivity<AboutView, AboutPresenter> implements AboutView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarNack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.tv_version_number)
    TextView tvVersionNumber;
    @BindView(R.id.rl_contact)
    RelativeLayout rlContact;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, AboutActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_about;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarNack.setOnClickListener(this);
        rlContact.setOnClickListener(this);
        toolbarTitle.setText("关于我们");
        tvVersionNumber.setText("版本号：v" + BuildConfig.VERSION_NAME);

    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.rl_contact:
                ContactActivity.startActivity(this);
                break;

        }
    }
}
