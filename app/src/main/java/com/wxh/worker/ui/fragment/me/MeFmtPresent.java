package com.wxh.worker.ui.fragment.me;

import com.common.config.MyApi;
import com.utilCode.base.mvp.RxMvpPresenter;

import javax.inject.Inject;

public class MeFmtPresent extends RxMvpPresenter<MeFmtView> {

    MyApi mMyApi;

    @Inject
    public MeFmtPresent(MyApi mMyApi){
        this.mMyApi = mMyApi;
    }

}
