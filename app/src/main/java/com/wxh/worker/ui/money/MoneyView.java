package com.wxh.worker.ui.money;

import com.utilCode.base.mvp.MvpView;
import com.wxh.worker.json.CashWithDrawalBean;

import java.util.List;

public interface MoneyView extends MvpView {

    void onSuccess(List<CashWithDrawalBean> cashWithDrawalBeanList);

    void onWithdrawalSuccess();

    void onFail();

    public interface WithDrawalListener{

        void onItemDrawal(CashWithDrawalBean item);

    }

}
