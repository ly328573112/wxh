package com.wxh.worker.ui.fragment.message;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;
import com.wxh.worker.event.MsgReadEvent;
import com.wxh.worker.ui.fragment.message.bean.MessageBean;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageAdapter extends BaseRecycleViewAdapter<MessageBean> {

    MessageClickListener messageClickListener;

    public MessageAdapter(Context context, MessageClickListener messageClickListener) {
        super(context);
        this.messageClickListener = messageClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, MessageBean item) {
        if (holder instanceof MessageHolder) {
            MessageHolder messageHolder = (MessageHolder) holder;
            messageHolder.msgContentTv.setText(item.title);
            messageHolder.msgTimeTv.setText(item.publishTime);
            messageHolder.tvContentId.setText(item.messages);
            if (TextUtils.equals("0", item.newsStatus)) {
//                messageHolder.msgUnreadIv.setVisibility(View.VISIBLE);
                messageHolder.msgUnreadIv.setVisibility(View.GONE);
            } else {
                messageHolder.msgUnreadIv.setVisibility(View.GONE);
            }
            messageHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (messageClickListener != null) {
                        messageClickListener.onMessageItemClick(item);
                        item.newsStatus = "1";
                        EventBus.getDefault().post(new MsgReadEvent());
                        notifyDataSetChanged();
                    }
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new MessageHolder(View.inflate(mContext, R.layout.message_list_item, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class MessageHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.msg_title_tv)
        TextView msgContentTv;
        @BindView(R.id.msg_time_tv)
        TextView msgTimeTv;
        @BindView(R.id.tv_content_id)
        TextView tvContentId;
        @BindView(R.id.msg_unread_iv)
        ImageView msgUnreadIv;

        MessageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public interface MessageClickListener {
        void onMessageItemClick(MessageBean item);
    }

}
