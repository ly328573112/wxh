package com.wxh.worker.ui.fragment.message;

import com.utilCode.base.mvp.MvpView;
import com.wxh.worker.ui.fragment.message.bean.MessageBean;

import java.util.List;

public interface MessageFmtView extends MvpView {

    void onMessageSuccess(List<MessageBean> messageBeanList);

    void onFail();

}
