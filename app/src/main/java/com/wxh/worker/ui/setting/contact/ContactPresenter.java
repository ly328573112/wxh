package com.wxh.worker.ui.setting.contact;

import com.common.config.MyApi;
import com.utilCode.base.mvp.RxMvpPresenter;

import javax.inject.Inject;

public class ContactPresenter extends RxMvpPresenter<ContactView> {
    MyApi mApi;

    @Inject
    public ContactPresenter( MyApi api) {
        mApi = api;
    }
}
