package com.wxh.worker.ui.fragment.home.bean;

import android.support.annotation.Keep;

import java.util.ArrayList;
import java.util.List;

@Keep
public class HomeListBean {

    public int index;
    public String orderStatus;
    public String name;
    public String numberOfstatus;
    public String type;

    public HomeListBean(int index, String orderStatus, String name, String numberOfstatus, String type) {
        this.index = index;
        this.orderStatus = orderStatus;
        this.name = name;
        this.numberOfstatus = numberOfstatus;
        this.type = type;
    }

    @Override
    public String toString() {
        return "HomeListBean{" +
                "index=" + index +
                ", orderStatus='" + orderStatus + '\'' +
                ", name='" + name + '\'' +
                ", numberOfstatus='" + numberOfstatus + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public static List<HomeListBean> getHomeListBean(){
        List<HomeListBean> homeListBeans = new ArrayList<>();
        homeListBeans.add(new HomeListBean(1, "5", "待服务订单", "", "1"));
        homeListBeans.add(new HomeListBean(2, "3", "待接受订单", "", "1"));
        homeListBeans.add(new HomeListBean(3, "4", "待预约订单", "", "1"));
//        homeListBeans.add(new HomeListBean(4, "3", "待提现订单", "", "1"));
        return homeListBeans;
    }

}
