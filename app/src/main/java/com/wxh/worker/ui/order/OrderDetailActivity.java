package com.wxh.worker.ui.order;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.common.datePicker.DatePickerView;
import com.wxh.worker.event.CheckAcceptanceEvent;
import com.wxh.worker.event.MeasureAddEvent;
import com.wxh.worker.event.MeasureDeleteEvent;
import com.common.mvp.BaseMvpActivity;
import com.common.retrofit.RxObserver;
import com.common.utils.MoneyUtil;
import com.common.utils.Utils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.utils.RxUtils;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.common_view.ImageClickListener;
import com.wxh.worker.common_view.PhotoImageAdapter;
import com.wxh.worker.common_view.bean.PhotoInfo;
import com.wxh.worker.common_view.imgoriginal.OriginalPagerActivity;
import com.wxh.worker.json.OrderDetailBean;
import com.wxh.worker.ui.checkup.CheckAcceptanceActivity;
import com.wxh.worker.ui.measure.MeasureAddDataActivity;
import com.wxh.worker.ui.measure.MeasureListActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class OrderDetailActivity extends BaseMvpActivity<OrderDetailView, OrderDetailPresent> implements OrderDetailView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.sv_view)
    ScrollView svView;
    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;
    @BindView(R.id.tv_order_amount)
    TextView tvOrderAmount;
    @BindView(R.id.tv_item_time)
    TextView tvItemTime;
    @BindView(R.id.tv_order_remarks)
    TextView tvOrderRemarks;
    @BindView(R.id.rv_order_detail)
    RecyclerView rvOrderDetail;
    @BindView(R.id.tv_owner_name)
    TextView tvOwnerName;
    @BindView(R.id.tv_owner_phone)
    TextView tvOwnerPhone;
    @BindView(R.id.tv_owner_address)
    TextView tvOwnerAddress;
    @BindView(R.id.tv_order_detailed_number)
    TextView tvOrderDetailedNumber;
    @BindView(R.id.rv_order_detail_list)
    RecyclerView rvOrderDetailList;
    @BindView(R.id.ll_install_view)
    LinearLayout llInstallView;
    @BindView(R.id.rl_measure_number)
    RelativeLayout rlMeasureNumber;
    @BindView(R.id.tv_measure_number)
    TextView tvMeasureNumber;
    @BindView(R.id.ll_copy_text)
    LinearLayout llCopyText;
    /**
     * 底部Button
     */
    @BindView(R.id.rl_button)
    RelativeLayout rl_button;
    @BindView(R.id.rl_button_a)
    RelativeLayout rl_button_a;
    @BindView(R.id.rl_button_b)
    RelativeLayout rl_button_b;
    @BindView(R.id.rl_button_c)
    RelativeLayout rl_button_c;
    @BindView(R.id.tv_button_gray)
    TextView tvButtonGray;
    @BindView(R.id.tv_button_red)
    TextView tvButtonRed;
    @BindView(R.id.tv_button_gray_a)
    TextView tvButtonGrayA;
    @BindView(R.id.tv_button_red_a)
    TextView tvButtonRedA;
    @BindView(R.id.tv_button_gray_b)
    TextView tvButtonGrayB;
    @BindView(R.id.tv_button_red_b)
    TextView tvButtonRedB;
    @BindView(R.id.tv_button_gray_c)
    TextView tvButtonGrayC;
    @BindView(R.id.tv_button_red_d)
    TextView tvButtonRedD;

    private String orderId;
    private String orderStatus;
    LoadingDialog mLoadingDialog;
    private OrderInstallAdpater installAdpater;
    private PhotoImageAdapter imageAdapter;
    private List<PhotoInfo> photoList = new ArrayList<>();// 照片uri列表
    private String customerPhone;
    private String orderNo;

    public static void startActivity(Activity activity, String orderId, String orderStatus) {
        Intent intent = new Intent(activity, OrderDetailActivity.class);
        intent.putExtra("orderId", orderId);
        intent.putExtra("orderStatus", orderStatus);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_order_detail;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        setButtonClick();
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }

        GridLayoutManager layoutManager = new GridLayoutManager(this, 5, LinearLayoutManager.VERTICAL, false);
        rvOrderDetail.setLayoutManager(layoutManager);
        rvOrderDetail.setHasFixedSize(true);
        imageAdapter = new PhotoImageAdapter(this);
        rvOrderDetail.setAdapter(imageAdapter);
        rvOrderDetail.setItemAnimator(new DefaultItemAnimator());
        imageAdapter.setImageClickListener(imageClickListener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvOrderDetailList.setLayoutManager(linearLayoutManager);
        installAdpater = new OrderInstallAdpater(this, installClickListener);
        rvOrderDetailList.setAdapter(installAdpater);
    }

    private ImageClickListener imageClickListener = new ImageClickListener() {
        @Override
        public void openCamera(int position) {
            ArrayList<String> arrayList = new ArrayList<>();
            for (int i = 0; i < photoList.size(); i++) {
                arrayList.add(photoList.get(i).photoUri);
            }
            OriginalPagerActivity.start(OrderDetailActivity.this, arrayList, position, false);
        }

        @Override
        public void onDeleteImage(int position) {

        }
    };

    private OrderInstallAdpater.InstallClickListener installClickListener = new OrderInstallAdpater.InstallClickListener() {
                @Override
                public void onInstallItemClick(OrderDetailBean.OrderExtendDTOsBean item) {

                }
            };

    @Override
    protected void setupData(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        Intent intent = getIntent();
        orderId = intent.getStringExtra("orderId");
        orderStatus = intent.getStringExtra("orderStatus");
        if (TextUtils.isEmpty(orderStatus)) {
            toolbarTitle.setText("订单详情");
        } else {
            toolbarTitle.setText("订单详情(" + getOrderStatus(orderStatus) + ")");
        }
        if (!mLoadingDialog.isVisible()) {
            mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
        }
        getPresenter().getOrdersDetails(orderId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private String getOrderStatus(String orderStatus) {
        switch (orderStatus) {
            case "3":
                rl_button_a.setVisibility(View.VISIBLE);
                return "待接受";
            case "4":
                rl_button_b.setVisibility(View.VISIBLE);
                return "待预约";
            case "5":
                rl_button_c.setVisibility(View.VISIBLE);
                return "服务中";
            case "6":
                return "待验收";
            case "8":
                rl_button.setVisibility(View.GONE);
                setScrollViewHeight();
                return "已完成";
            default:
                return "待接受";
        }
    }

    private void setScrollViewHeight() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) svView.getLayoutParams();
        layoutParams.bottomMargin = 0;//将默认的距离底部60dp，改为0，这样底部区域全被scrollview填满。
        svView.setLayoutParams(layoutParams);
    }

    private void setButtonClick() {
        toolbarBack.setOnClickListener(this);
        tvButtonGray.setOnClickListener(this);
        tvButtonRed.setOnClickListener(this);
        tvButtonGrayA.setOnClickListener(this);
        tvButtonRedA.setOnClickListener(this);
        tvButtonGrayB.setOnClickListener(this);
        tvButtonRedB.setOnClickListener(this);
        tvButtonGrayC.setOnClickListener(this);
        rlMeasureNumber.setOnClickListener(this);
        tvButtonRedD.setOnClickListener(this);
        llCopyText.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.rl_measure_number://查看测量数据
                MeasureListActivity.startActivity(this, orderId);
                break;
            case R.id.tv_button_gray://拒绝订单
                if (!mLoadingDialog.isVisible()) {
                    mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                }
                getPresenter().refuseOrder(orderId);
                break;
            case R.id.tv_button_red://接受订单
                if (!mLoadingDialog.isVisible()) {
                    mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                }
                getPresenter().acceptOrder(orderId);
                break;
            case R.id.tv_button_gray_a:
            case R.id.tv_button_gray_c://修改预约时间
                DatePickerView datePickerDialog = new DatePickerView(this, null, DatePickerView.SHOWTYPE_MM_DD_hh_mm, true);
                datePickerDialog.show();
                datePickerDialog.setDatePickListener(onDatePickListener);
                break;
            case R.id.tv_button_red_a://联系业主
                if (TextUtils.isEmpty(customerPhone)) {
                    return;
                }
                try {
                    new RxPermissions(this).request(Manifest.permission.CALL_PHONE)
                            .compose(RxUtils.applySchedulersLifeCycle(getMvpView()))
                            .subscribe(new RxObserver<Boolean>() {
                                @Override
                                public void onComplete() {
                                    Utils.callPhone(OrderDetailActivity.this, customerPhone);
                                }
                            });
                } catch (Exception excp) {
                    excp.printStackTrace();
                }

                break;
            case R.id.tv_button_gray_b:
            case R.id.tv_button_red_d://申请验收
                CheckAcceptanceActivity.startActivity(this, orderId);
                break;
            case R.id.tv_button_red_b:
                MeasureAddDataActivity.startActivity(this, orderId);
                break;
            case R.id.ll_copy_text://复制订单号
                copyText(orderNo);
                break;
            default:
                break;
        }
    }

    private void copyText(String copiedText) {
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.setPrimaryClip(ClipData.newPlainText(null, copiedText));
        ToastUtils.showLongToastSafe("已复制订单号：" + copiedText);
    }

    private DatePickerView.OnDatePickListener onDatePickListener = new DatePickerView.OnDatePickListener() {

                @Override
                public void onClick(boolean sureCancel, String year, String month, String day, String hh, String mm) {
                    if (!sureCancel) {
                        return;
                    }
                    if (!mLoadingDialog.isVisible()) {
                        mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                    }
                    String yyyyMMDDbbmm = year + "-" + month + "-" + day + hh + ":" + mm + ":00";
                    getPresenter().postAppointment(orderId, yyyyMMDDbbmm, "");
                }
            };

    @Override
    public void onOrderDetailSuccess(OrderDetailBean orderDetailBean) {
        mLoadingDialog.dismissAllowingStateLoss();
        if (orderDetailBean == null) {
            return;
        }
        orderStatus = orderDetailBean.status;
        toolbarTitle.setText("订单详情(" + getOrderStatus(orderStatus) + ")");
        customerPhone = orderDetailBean.customerPhone;
        setTextData(orderDetailBean);
        for (int i = 0; i < orderDetailBean.picturesDTOS.size(); i++) {
            /**
             * 目前使用Glide（4.7.1）加载图片，需要通过ssl来信任所有网络图片访问的证书，后期再处理，暂时改成http
             */
            PhotoInfo photoInfo = null;
            String url = orderDetailBean.picturesDTOS.get(i).url;
            if (url.startsWith("https")) {
                photoInfo = new PhotoInfo(0, url.replace("https", "http"), false);
            } else {
                photoInfo = new PhotoInfo(0, url, false);
            }
            photoList.add(photoInfo);
        }
        imageAdapter.resetData(photoList);
        if (orderDetailBean.orderExtendDTOs == null || orderDetailBean.orderExtendDTOs.size() == 0) {
            llInstallView.setVisibility(View.GONE);
        } else {
            llInstallView.setVisibility(View.VISIBLE);
            for (int i = 0; i < orderDetailBean.orderExtendDTOs.size(); i++) {
                orderDetailBean.orderExtendDTOs.get(i).listIndex = i + 1;
            }
            installAdpater.resetData(orderDetailBean.orderExtendDTOs);
        }
    }

    @Override
    public void onAcceptSuccess() {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showLongToastSafe("接单成功");
        rl_button.setVisibility(View.GONE);
        EventBus.getDefault().post(new CheckAcceptanceEvent());
        setScrollViewHeight();
    }

    @Override
    public void onRefuseSuccess() {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showLongToastSafe("拒绝成功");
        rl_button.setVisibility(View.GONE);
        setScrollViewHeight();
    }

    @Override
    public void onAppointmentSuccess() {
        ToastUtils.showLongToastSafe(" 预约成功");
        getPresenter().getOrdersDetails(orderId);
    }

    @Override
    public void onFail(int error) {
        mLoadingDialog.dismissAllowingStateLoss();
        switch (error) {
            case 1:
                break;
            case 2:
                ToastUtils.showLongToastSafe("接单失败");
                break;
            case 3:
                ToastUtils.showLongToastSafe("拒绝失败");
                break;
            case 4:
                ToastUtils.showLongToastSafe("预约失败");
                break;
        }
    }


    private void setTextData(OrderDetailBean orderDetail) {
        setTextDrawable(tvOrderNumber, orderDetail);
        orderNo = orderDetail.orderNo;
        tvOrderNumber.setText("订单编号：" + orderNo);
        tvItemTime.setText(orderDetail.appointmentTime);
        tvOrderRemarks.setText(orderDetail.remark);
        tvOwnerName.setText(orderDetail.customerName);
        tvOwnerPhone.setText(orderDetail.customerPhone);
        tvOwnerAddress.setText(orderDetail.addressDetail);
    }

    public void setTextDrawable(TextView textView, OrderDetailBean orderDetail) {
        String orderAmount = "";
        if (TextUtils.equals(orderDetail.type, "1")) {
            setMeasireButtonVisible(View.VISIBLE);
            Drawable drawableLeft = getResources().getDrawable(R.drawable.ic_order_measure);
            textView.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, null, null, null);
            orderAmount = "测量费: " + MoneyUtil.parseMoneyWithComma(orderDetail.otherAmt) + " 元";
            if (TextUtils.isEmpty(orderDetail.windowNum) || TextUtils.equals(orderDetail.windowNum, "0")) {
                rlMeasureNumber.setVisibility(View.GONE);
                tvButtonGrayC.setVisibility(View.VISIBLE);
                tvButtonGrayB.setVisibility(View.GONE);
            } else {
                rlMeasureNumber.setVisibility(View.VISIBLE);
                tvButtonGrayC.setVisibility(View.GONE);
                tvButtonGrayB.setVisibility(View.VISIBLE);
                String s = TextUtils.isEmpty(orderDetail.orderMeasureRecordNum) ? "0" : orderDetail.orderMeasureRecordNum;
                String windowNum = "测量数据:   " + s;
                tvMeasureNumber.setText(windowNum);
                setOrderAmount(tvMeasureNumber, windowNum, 5, windowNum.length());
            }
        } else if (TextUtils.equals(orderDetail.type, "2")) {
            setInstallButtonVisible(View.VISIBLE);
            Drawable drawableLeft = getResources().getDrawable(R.drawable.ic_order_install);
            textView.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, null, null, null);
            orderAmount = "安装费：" + MoneyUtil.parseMoneyWithComma(orderDetail.otherAmt) + " 元";
            String detailNumber = "安装清单: 共 " + orderDetail.orderExtendDTOs.size() + " 个窗户";
            tvOrderDetailedNumber.setText(detailNumber);
            setOrderAmount(tvOrderDetailedNumber, detailNumber, 7, detailNumber.length() - 3);
        }
        textView.setCompoundDrawablePadding(10);
        tvOrderAmount.setText(orderAmount);
        setOrderAmount(tvOrderAmount, orderAmount, 4, orderAmount.length() - 1);
    }

    private void setMeasireButtonVisible(int visible) {
        tvButtonGrayB.setVisibility(visible);
        tvButtonRedB.setVisibility(visible);
    }

    private void setInstallButtonVisible(int visible) {
        tvButtonGrayC.setVisibility(visible);
        tvButtonRedD.setVisibility(visible);
    }

    public void setOrderAmount(TextView textView, String keyName, int start, int end) {
        SpannableStringBuilder style = new SpannableStringBuilder();
        //设置文字
        style.append(keyName);
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#E60000"));
        style.setSpan(colorSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //配置给TextView
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(style);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MeasureDeleteEvent event) {
        getPresenter().getOrdersDetails(orderId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MeasureAddEvent event) {
        getPresenter().getOrdersDetails(orderId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CheckAcceptanceEvent event) {
        finish();
    }
}
