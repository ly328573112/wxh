package com.wxh.worker.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wxh.worker.event.OriginalDeleteEvent;
import com.common.logupload.BitmapThreedTask;
import com.common.logupload.UploadLogUtils;
import com.common.logupload.UploadRequestBean;
import com.common.utils.LogUtil;
import com.utilCode.dialog.LoadingDialog;
import com.wxh.worker.common_view.CameraSelectView;
import com.wxh.worker.common_view.CameraUtil;
import com.wxh.worker.common_view.ImageClickListener;
import com.wxh.worker.common_view.PhotoImageAdapter;
import com.wxh.worker.common_view.bean.PhotoInfo;
import com.common.config.Global;
import com.wxh.worker.event.CitySelectEvent;
import com.common.mvp.BaseMvpActivity;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.city_select.CitySelectActivity;
import com.wxh.worker.city_select.bean.CityBean;
import com.wxh.worker.city_select.bean.DistrictBean;
import com.wxh.worker.city_select.bean.ProvincesBean;
import com.wxh.worker.common_view.imgoriginal.OriginalPagerActivity;
import com.wxh.worker.ui.HomeActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class PerfectMessageActivity extends BaseMvpActivity<LRView, LRPresent> implements LRView, View.OnClickListener, ImageClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    /**
     * 现住地址
     */
    @BindView(R.id.ll_select_city)
    LinearLayout llSelectCity;
    @BindView(R.id.tv_city_value)
    TextView tvCityValue;
    @BindView(R.id.et_detail_address)
    EditText etDetailAddress;

    /**
     * 紧急联系人
     */
    @BindView(R.id.et_urgent_name)
    EditText etUrgentName;
    @BindView(R.id.et_urgent_phone)
    EditText etUrgentPhone;
    @BindView(R.id.et_urgent_relationship)
    EditText etUrgentRelationship;

    /**
     * 身份证信息
     */
    @BindView(R.id.et_identity_name)
    EditText etIdentityName;
    @BindView(R.id.et_identity_id_number)
    EditText etIdentityIdNumber;
    @BindView(R.id.rv_identity_gridview)
    RecyclerView rvIdentityGridview;

    /**
     * 提交
     */
    @BindView(R.id.tv_submit)
    TextView tvSubmit;

    private ProvincesBean provinces;
    private CityBean city;
    private DistrictBean district;

    private CameraUtil cameraUtil;
    private CameraSelectView selectView;
    private PhotoImageAdapter imageAdapter;
    private List<PhotoInfo> photoList = new ArrayList<>();// 照片uri列表
    private int currentItemId = 1;// 加号
    private int COLUMN = 2;//图片可选数量
    private int imageColumn = 1;//图片集合是否带有加号

    private LoadingDialog mLoadingDialog;

    private UploadLogUtils uploadLogUtils;


    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, PerfectMessageActivity.class);
        activity.startActivity(intent);
    }


    @Override
    protected int getLayoutId() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return R.layout.activity_perfect_message;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarTitle.setText("完善个人信息");
        toolbarBack.setOnClickListener(this);
        toolbarBack.setVisibility(View.GONE);
        tvSubmit.setOnClickListener(this);
        llSelectCity.setOnClickListener(this);

        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }

        uploadLogUtils = new UploadLogUtils();
        initListView();
    }

    private void initListView() {
        selectView = new CameraSelectView(this);
        selectView.setPictureCut(false);
        cameraUtil = new CameraUtil(this);

        photoList.clear();
        PhotoInfo info = new PhotoInfo(currentItemId, null, false);
        photoList.add(info);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false);
        rvIdentityGridview.setLayoutManager(layoutManager);
        rvIdentityGridview.setHasFixedSize(true);
        imageAdapter = new PhotoImageAdapter(this);
        rvIdentityGridview.setAdapter(imageAdapter);
        rvIdentityGridview.setItemAnimator(new DefaultItemAnimator());
        imageAdapter.setImageClickListener(this);
        imageAdapter.addData(photoList);
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
    }

    @Override
    public void onSMS_Success() {

    }

    @Override
    public void onRegister() {

    }

    @Override
    public void onLoginSuccess(String status) {
        mLoadingDialog.dismissAllowingStateLoss();
        HomeActivity.startActivity(this);
        finish();
    }

    @Override
    public void onFail(int type, String message) {
        mLoadingDialog.dismissAllowingStateLoss();
    }

    @Override
    public void openCamera(int position) {
        int photoNumber = photoList.get(position).photoNumber;
        if (photoNumber == currentItemId) {
            int optionalNumber = COLUMN - (photoList.size() - imageColumn);
            selectView.showCameraDialog(cameraUtil, optionalNumber, View.VISIBLE);
            selectView.setCameraPotoListener(potoListener);
        } else if (photoNumber == 0) {
            ArrayList<String> arrayList = new ArrayList<>();
            for (int i = 0; i < photoList.size() - imageColumn; i++) {
                arrayList.add(photoList.get(i).photoUri);
            }
            OriginalPagerActivity.start(this, arrayList, position);
        }
    }

    @Override
    public void onDeleteImage(int position) {
        String networkUrL = photoList.get(position).networkUrL.replaceAll("https://oss.aguogo.cn/", "");
        if(!TextUtils.isEmpty(networkUrL)){
            getPresenter().deleteImg(networkUrL);
        }
        photoList.remove(position);
        confineListSize();
        imageAdapter.resetData(photoList);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(OriginalDeleteEvent event) {
        onDeleteImage(event.curPosition);
    }

    /**
     * 加号始终放在List最后
     */
    private void confineListSize() {
        for (PhotoInfo photoInfo : photoList) {
            if (photoInfo.photoNumber == currentItemId) {
                photoList.remove(photoInfo);
                break;
            }
        }
        if (photoList.size() < COLUMN) {
            PhotoInfo info = new PhotoInfo(currentItemId, null, false);
            photoList.add(info);
            imageColumn = 1;
        } else {
            imageColumn = 0;
        }
    }


    private CameraSelectView.CameraPotoListener potoListener = new CameraSelectView.CameraPotoListener() {

        @Override
        public void onResultPotoList(List<PhotoInfo> imgList) {
            if (!mLoadingDialog.isVisible()) {
                mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
            }
            new BitmapThreedTask().new LoadlocalBitmapThread(imgList, handler).start();
        }
    };

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            // 可能有反复调用相册的情况，所以需要多重迭代增加
            @SuppressWarnings("unchecked")
            ArrayList<File> callBacklist = (ArrayList<File>) msg.obj;
            for (int i = 0; i < callBacklist.size(); i++) {
                LogUtil.i("压缩后图片大小：" + BitmapThreedTask.formatFileSize(callBacklist.get(i)));
                PhotoInfo photoInfo = new PhotoInfo(0, callBacklist.get(i).toString(), false);

                File file = new File(photoInfo.photoUri);
                if (!file.exists()) {
                    return;
                }
                RequestBody type = RequestBody.create(MediaType.parse("form-data"), "1");
                uploadLogUtils.uploadImg(type, file);
                uploadLogUtils.setResponseBodyListener(new UploadImage(photoInfo.photoUri));
            }
        }
    };

   class UploadImage implements UploadLogUtils.ResponseBodyListener {

       String photoUri;

       UploadImage(String photoUri){
           this.photoUri = photoUri;
       }


        @Override
        public void requestBody(UploadRequestBean.ResultBean result) {
            mLoadingDialog.dismissAllowingStateLoss();
            PhotoInfo info = new PhotoInfo(0, photoUri, result.name, true);
            photoList.add(info);
            confineListSize();
            imageAdapter.resetData(photoList);
        }

        @Override
        public void requestError() {
            mLoadingDialog.dismissAllowingStateLoss();
            ToastUtils.showLongToast("图片上传失败!");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        cameraUtil.onHandleActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;

            case R.id.ll_select_city:
                CitySelectActivity.startActivity(this, provinces, city, district);
                break;

            case R.id.tv_submit:
                checkElement();
                break;
        }
    }

    private void checkElement() {
        String masterAddress = etDetailAddress.getText().toString().trim();
        String emergencyContactsName = etUrgentName.getText().toString().trim();
        String emergencyContactsPhone = etUrgentPhone.getText().toString().trim();
        String emergencyContactsRelation = etUrgentRelationship.getText().toString().trim();
        String masterName = etIdentityName.getText().toString().trim();
        String masterCardNo = etIdentityIdNumber.getText().toString().trim();
        int photoSize = photoList.size();
        for (int i = 0; i < photoList.size(); i++){
            if(photoList.get(i).photoNumber == currentItemId){
                photoSize = photoList.size() - 1;
            }
        }
        if (null == provinces) {
            ToastUtils.showLongToastSafe("请选择您所在区域地址");
        } else if (checkEditText(masterAddress)) {
            ToastUtils.showLongToastSafe("请填写居住地址");
        } else if (checkEditText(emergencyContactsName)) {
            ToastUtils.showLongToastSafe("请填写紧急联系人");
        } else if (checkPhoneEditText(emergencyContactsPhone)) {
            ToastUtils.showLongToastSafe("请填写紧急联系方式");
        } else if (checkEditText(emergencyContactsRelation)) {
            ToastUtils.showLongToastSafe("请填写与紧急联系人关系");
        } else if (checkEditText(masterName)) {
            ToastUtils.showLongToastSafe("请填写身份证姓名");
        } else if (checkEditText(masterCardNo) || masterCardNo.length() != 18) {
            ToastUtils.showLongToastSafe("请填写正确的身份证号码");
        } else if (photoSize != 2) {
            ToastUtils.showLongToastSafe("请上传身份证（正/反面）照片");
        } else {
            if (!mLoadingDialog.isVisible()) {
                mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
            }
            setPerfectData(masterAddress,
                    emergencyContactsName,
                    emergencyContactsPhone,
                    emergencyContactsRelation,
                    masterName,
                    masterCardNo);
        }
    }

    private void setPerfectData(String masterAddress,
                                String emergencyContactsName,
                                String emergencyContactsPhone,
                                String emergencyContactsRelation,
                                String masterName,
                                String masterCardNo) {
        JSONObject requestAttributes = new JSONObject();
        try {
            requestAttributes.put("uid", Global.getUserInfoBean().id);//uid
            requestAttributes.put("companyProvinceId", provinces.provinceId);//省ID
            requestAttributes.put("companyCityId", city.cityId);//市ID
            requestAttributes.put("companyDistrictId", district.districtId);//所在区域id
            requestAttributes.put("masterAddress", masterAddress);//现居住地址(包括省市)
            requestAttributes.put("emergencyContactsName", emergencyContactsName);//紧急联系人姓名
            requestAttributes.put("emergencyContactsPhone", emergencyContactsPhone);//紧急联系人电话
            requestAttributes.put("emergencyContactsRelation", emergencyContactsRelation);//紧急联系人关系
            requestAttributes.put("masterName", masterName);//申请人姓名
            requestAttributes.put("masterCardNo", masterCardNo);//申请人身份证号
            requestAttributes.put("masterCardElectronic1", "");//申请人手持身份证电子版
            requestAttributes.put("masterCardElectronic2", photoList.get(0).networkUrL);//申请人身份证正面
            requestAttributes.put("masterCardElectronic3", photoList.get(1).networkUrL);//申请人身份证反面
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), requestAttributes.toString());
        getPresenter().postPerfectMessages(requestBody);
    }

    private boolean checkEditText(String text) {
        if (TextUtils.isEmpty(text)) {
            return true;
        }
        return false;
    }

    public boolean checkPhoneEditText(String phone) {
        return TextUtils.isEmpty(phone) || phone.replace(" ", "").length() != 11;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CitySelectEvent event) {
        if (event == null) {
            return;
        }
        provinces = event.provincesBean;
        city = event.cityBean;
        district = event.districtBean;
        tvCityValue.setText(provinces.provinceName + city.cityName + district.districtName);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}
