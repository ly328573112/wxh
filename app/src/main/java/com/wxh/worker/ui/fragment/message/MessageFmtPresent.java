package com.wxh.worker.ui.fragment.message;

import android.support.annotation.NonNull;

import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.ui.fragment.message.bean.MessageBean;

import java.util.List;

import javax.inject.Inject;

public class MessageFmtPresent extends RxMvpPresenter<MessageFmtView> {

    MyApi mMyApi;

    @Inject
    public MessageFmtPresent(MyApi mMyApi){
        this.mMyApi = mMyApi;
    }

    public void getNewsLists(){
        mMyApi.getNewsLists()
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<MessageBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<MessageBean>> data) {
                        if (checkJsonCode(data, false) && data.getResult() != null) {
                            getView().onMessageSuccess(data.getResult());
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

    public void updateNews(String newsId){
        mMyApi.updateNews(newsId)
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false) && data.getResult() != null) {
                            getNewsLists();
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

}
