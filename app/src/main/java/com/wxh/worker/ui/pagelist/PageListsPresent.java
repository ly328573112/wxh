package com.wxh.worker.ui.pagelist;

import android.arch.paging.ItemKeyedDataSource;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.json.AndroidGitLib;
import com.wxh.worker.json.AndroidLib;
import com.wxh.worker.json.GitUser;
import com.common.config.MyApi;
import com.common.retrofit.RxObserver;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.functions.Action;
import io.reactivex.functions.Function;
import timber.log.Timber;

public class PageListsPresent extends RxMvpPresenter<PageListsView> {

    protected MyApi mMyApi;

    protected long mStartBeingId = 0;
    protected long mStartBeingPage = 1;

    @Inject
    public PageListsPresent(MyApi myApi) {
        mMyApi = myApi;
    }

    class PageByIdDataSource extends ItemKeyedDataSource<Long, GitUser> {

        @Override
        public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull LoadInitialCallback<GitUser> callback) {
            getView().onLoadDoing();
            mMyApi.getUsersPagesById(mStartBeingId, params.requestedLoadSize)
                    .compose(RxUtils.<List<GitUser>>applySchedulersLifeCycle(getView()))
                    .subscribe(new RxObserver<List<GitUser>>() {

                        @Override
                        public void onNext(@NonNull List<GitUser> gitUsers) {
                            getView().onLoadDone();
                            callback.onResult(gitUsers);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            getView().onLoadError();
                        }
                    });
        }

        @Override
        public void loadAfter(@NonNull LoadParams<Long> params, @NonNull LoadCallback<GitUser> callback) {
            getView().onLoadDoing();
            Timber.e("loadAfter getKey = " + params.key);
            mMyApi.getUsersPagesById(params.key, params.requestedLoadSize)
                    .compose(RxUtils.<List<GitUser>>applySchedulersLifeCycle(getView()))
                    .subscribe(new RxObserver<List<GitUser>>() {

                        @Override
                        public void onNext(@NonNull List<GitUser> gitUsers) {
                            getView().onLoadDone();
                            callback.onResult(gitUsers);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            getView().onLoadError();
                        }
                    });
        }

        @Override
        public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<GitUser> callback) {
            // ignored, since we only ever append to our initial load
        }

        @NonNull
        @Override
        public Long getKey(@NonNull GitUser item) {
            Timber.e("getKey = " + item.getId());
            return item.getId();
        }
    }

    class PageByPageDataSource extends PageKeyedDataSource<Long, AndroidLib> {

        @Override
        public void loadInitial(@NonNull LoadInitialParams<Long> params, @NonNull LoadInitialCallback<Long, AndroidLib> callback) {
            mMyApi.getUsersPagesByPage("android", (int) mStartBeingPage, params.requestedLoadSize)
                    .map(new Function<AndroidGitLib, List<AndroidLib>>() {
                        @Override
                        public List<AndroidLib> apply(AndroidGitLib androidGitLib) throws
                                Exception {
                            return androidGitLib.getItems();
                        }
                    })
                    .compose(RxUtils.<List<AndroidLib>>applySchedulersLifeCycle(getView()))
                    .subscribe(new RxObserver<List<AndroidLib>>() {

                        @Override
                        public void onNext(@NonNull List<AndroidLib> androidLibs) {
                            mStartBeingPage = androidLibs.size() / PageByPageListsActivity.pageSize;
                            callback.onResult(androidLibs, mStartBeingPage - 1, mStartBeingPage + 1);
                            Timber.e("loadInitial 0 " + (mStartBeingPage - 1));
                            Timber.e("loadInitial 1 " + (mStartBeingPage + 1));
                            mStartBeingPage++;
                            getView().onLoadDone();
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            setErrorRetry(new Action() {
                                @Override
                                public void run() throws Exception {
                                    loadInitial(params, callback);
                                }
                            });
                            getView().onLoadError();
                        }
                    });
        }

        @Override
        public void loadBefore(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, AndroidLib> callback) {

        }

        @Override
        public void loadAfter(@NonNull LoadParams<Long> params, @NonNull LoadCallback<Long, AndroidLib> callback) {
            Timber.e("loadAfter = " + params.key + " mStartBeingPage = " + mStartBeingPage);
            getView().onLoadDoing();
            mMyApi.getUsersPagesByPage("android", params.key.intValue(), params.requestedLoadSize)
                    .map(new Function<AndroidGitLib, List<AndroidLib>>() {
                        @Override
                        public List<AndroidLib> apply(AndroidGitLib androidGitLib) throws
                                Exception {
                            return androidGitLib.getItems();
                        }
                    })
                    .compose(RxUtils.<List<AndroidLib>>applySchedulersLifeCycle(getView()))
                    .subscribe(new RxObserver<List<AndroidLib>>() {

                        @Override
                        public void onNext(@NonNull List<AndroidLib> androidLibs) {
                            callback.onResult(androidLibs, mStartBeingPage + 1);
                            mStartBeingPage++;
                            getView().onLoadDone();
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            setErrorRetry(new Action() {
                                @Override
                                public void run() throws Exception {
                                    loadAfter(params, callback);
                                }
                            });
                            getView().onLoadError();
                        }
                    });
        }
    }

}
