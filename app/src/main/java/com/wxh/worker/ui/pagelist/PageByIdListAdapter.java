package com.wxh.worker.ui.pagelist;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.utilCode.dagger.help.GlideApp;
import com.makeramen.roundedimageview.RoundedImageView;
import com.wxh.worker.R;
import com.wxh.worker.json.GitUser;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PageByIdListAdapter extends PagedListAdapter<GitUser, PageByIdListAdapter.PageByIdListHolder> {

    Context mContext;

    public PageByIdListAdapter(Context context) {
        super(DIFF_CALLBACK);
        mContext = context;
    }

    @NonNull
    @Override
    public PageByIdListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PageByIdListHolder(View.inflate(parent.getContext(), R.layout.page_list_item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull PageByIdListHolder holder, int position) {
        GlideApp.with(mContext)
                .asBitmap()
                .load(getItem(position).getAvatarUrl())
                .into(((PageByIdListHolder) holder).iv_gituser_image);
        ((PageByIdListHolder) holder).tv_gituser_name.setText(getItem(position).getLogin());
    }

    public class PageByIdListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_gituser_image)
        RoundedImageView iv_gituser_image;
        @BindView(R.id.tv_gituser_name)
        TextView tv_gituser_name;

        PageByIdListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setupView();
        }

        private void setupView() {

        }
    }

    public static final DiffUtil.ItemCallback<GitUser> DIFF_CALLBACK = new DiffUtil.ItemCallback<GitUser>() {

        @Override
        public boolean areItemsTheSame(GitUser oldItem, GitUser newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(GitUser oldItem, GitUser newItem) {
            return Objects.equals(oldItem, newItem);
        }
    };

}
