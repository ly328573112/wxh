package com.wxh.worker.ui.fragment.order;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;
import com.wxh.worker.ui.fragment.order.bean.OrderLabelBean;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderLabelAdapter extends BaseRecycleViewAdapter<OrderLabelBean> {

    private LabelClickListener clickListener;

    public OrderLabelAdapter(Context context, LabelClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, OrderLabelBean item) {
        if(holder instanceof OrderLabelHolder){
            OrderLabelHolder labelHolder = (OrderLabelHolder)holder;
            labelHolder.tvLabelName.setText(item.labelName);
            if(item.labelSelect){
                labelHolder.tvLabelName.setBackgroundResource(R.drawable.ic_label_select_yes);
                labelHolder.tvLabelName.setTextColor(mContext.getResources().getColor(R.color.app_white));
            } else {
                labelHolder.tvLabelName.setBackgroundResource(R.drawable.ic_label_select_no);
                labelHolder.tvLabelName.setTextColor(mContext.getResources().getColor(R.color.c_5D5D5D));
            }
            labelHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(clickListener != null){
                        clickListener.onLabelItemClick(item);
                    }
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new OrderLabelHolder(View.inflate(mContext, R.layout.item_order_label, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class OrderLabelHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_label_name)
        TextView tvLabelName;

        OrderLabelHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public interface LabelClickListener{
        void onLabelItemClick(OrderLabelBean item);
    }

}
