package com.wxh.worker.ui.login;

import com.utilCode.base.mvp.MvpView;

public interface LRView extends MvpView {

    void onSMS_Success();

    void onRegister();

    void onLoginSuccess(String status);

    void onFail(int type, String message);

    interface ValidateCodeListener {
        void disableValidateCodeButton();

        void countValidateCodeButton(long number);

        void resendValidateCodeButton();

        void enableValidateCodeButton();
    }

}
