package com.wxh.worker.ui.login;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.request.RequestOptions;
import com.common.config.Api;
import com.common.mvp.BaseMvpActivity;
import com.common.utils.FormatUtil;
import com.common.widght.EditTextWithDel;
import com.utilCode.dagger.help.GlideApp;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;

import java.util.UUID;

import butterknife.BindView;

public class RegisterActivity extends BaseMvpActivity<LRView, LRPresent> implements LRView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.et_register_phonenumber)
    EditTextWithDel etRegisterPhonenumber;
    @BindView(R.id.et_register_v_code)
    EditTextWithDel etRegisterCode;
    @BindView(R.id.et_register_pwd)
    EditTextWithDel etRegisterPwd;
    @BindView(R.id.et_register_pwd_confirm)
    EditTextWithDel etRegisterPwdConfirm;
    @BindView(R.id.tv_send_v_code)
    TextView tvSendCode;
    @BindView(R.id.tv_register_agreement)
    TextView tvRegisterAgreement;
    @BindView(R.id.tv_register)
    TextView tvRegister;
    @BindView(R.id.iv_send_figure_code)
    ImageView ivSendFigureCode;
    @BindView(R.id.et_register_figure_code)
    EditTextWithDel etRegisterFigureCode;

    private String phone = "";
    private boolean inputPwd = false;
    LoadingDialog mLoadingDialog;
    private String figureCode;


    public static void startActivity(Activity activity, String phone) {
        Intent intent = new Intent(activity, RegisterActivity.class);
        intent.putExtra("phone", phone);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarTitle.setText("注册");
        toolbarBack.setOnClickListener(this);
        tvSendCode.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        ivSendFigureCode.setOnClickListener(this);

        String hint = "点击“注册”， 即表示您同意并愿意遵守《威星汇用户协议》";
        setCallBack(hint);

        etRegisterPhonenumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(13)});
        etRegisterPhonenumber.addTextChangedListener(new TextWatcherListener(1));

        etRegisterPwd.addTextChangedListener(new TextWatcherListener(2));

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.signature((Key) new Signature(UUID.randomUUID().toString()));
        Glide.with(this).load(Api.CODE_PIC).apply(requestOptions).into(ivSendFigureCode);
    }

    private void setCallBack(String str) {
        SpannableStringBuilder style = new SpannableStringBuilder();
        //设置文字
        style.append(str);
        //设置部分文字点击事件
        ClickableSpan clickableSpan = new ClickableSpan() {

            @Override

            public void onClick(View widget) {
                ToastUtils.showLongToastSafe("《威星汇用户协议》");
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                /**set textColor**/
                ds.setColor(ds.linkColor);
                /**Remove the underline**/
                ds.setUnderlineText(false);
            }
        };
        style.setSpan(clickableSpan, style.length() - 9, style.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvRegisterAgreement.setText(style);
        //设置部分文字颜色
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.parseColor("#0089FE"));
        style.setSpan(foregroundColorSpan, style.length() - 9, style.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //配置给TextView
        tvRegisterAgreement.setMovementMethod(LinkMovementMethod.getInstance());
        tvRegisterAgreement.setText(style);
        avoidHintColor(tvRegisterAgreement);
    }

    private void avoidHintColor(View view) {
        if (view instanceof TextView)
            ((TextView) view).setHighlightColor(getResources().getColor(android.R.color.transparent));
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        phone = getIntent().getStringExtra("phone");
        etRegisterPhonenumber.setText(TextUtils.isEmpty(phone) ? "" : phone);

        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    //禁止输入框输入空格和换行符号2
    private InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source.equals(" ") || source.toString().contentEquals("\n")) {
                return "";
            } else {
                return null;
            }
        }
    };

    class TextWatcherListener implements TextWatcher {

        int type;

        public TextWatcherListener(int type) {
            this.type = type;
        }


        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            if (type == 1) {
                FormatUtil.addEditSpace(charSequence, start, before, etRegisterPhonenumber);
            } else if (type == 2) {
                checkPasswordEditTextLogin(charSequence.toString());
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    @Override
    public void onSMS_Success() {

    }

    @Override
    public void onRegister() {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showShortToastSafe("注册成功");
        finish();
    }

    @Override
    public void onLoginSuccess(String status) {

    }

    @Override
    public void onFail(int type, String message) {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showShortToastSafe("注册失败");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.iv_send_figure_code:
                break;
            case R.id.tv_send_v_code:
                phone = etRegisterPhonenumber.getText().toString().replace(" ", "");
                if (!checkPhoneEditTextLogin(phone)) {
                    ToastUtils.showShortToastSafe("请输入正确的手机号");
                    return;
                }
//                getPresenter().sendSMS(phone, validateCodeListener);
                break;
            case R.id.tv_register:
                phone = etRegisterPhonenumber.getText().toString().replace(" ", "");
                String vCode = etRegisterCode.getText().toString();
                figureCode = etRegisterFigureCode.getText().toString();
//                String password = etRegisterPwd.getText().toString();
                String PasswordConfirm = etRegisterPwdConfirm.getText().toString();
                if (!checkPhoneEditTextLogin(phone)) {
                    ToastUtils.showShortToastSafe("请输入正确的手机号");
                    return;
                } else if (TextUtils.isEmpty(figureCode)) {
                    ToastUtils.showShortToastSafe("请输入图形验证码");
                    return;
                } else if (TextUtils.isEmpty(vCode) || vCode.length() != 4) {
                    ToastUtils.showShortToastSafe("请输入正确的验证码");
                    return;
                }
//                else if (!inputPwd && TextUtils.isEmpty(password)) {
//                    ToastUtils.showShortToastSafe("请输入6~18位数密码");
//                    return;
//                } else if (TextUtils.isEmpty(PasswordConfirm) || !TextUtils.equals(password, PasswordConfirm)) {
//                    ToastUtils.showShortToastSafe("两次密码输入不一致，请重新输入");
//                    return;
//                }
                if (!mLoadingDialog.isVisible()) {
                    mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                }
                getPresenter().register(phone, PasswordConfirm, vCode);
                break;
            default:
                break;
        }
    }

    private LRView.ValidateCodeListener validateCodeListener = new LRView.ValidateCodeListener() {

        @Override
        public void disableValidateCodeButton() {
            tvSendCode.setEnabled(false);
        }

        @Override
        public void countValidateCodeButton(long number) {
            tvSendCode.setText("已发送(" + number + ")");
        }

        @Override
        public void resendValidateCodeButton() {
            tvSendCode.setEnabled(true);
            tvSendCode.setText("重发验证码");
        }

        @Override
        public void enableValidateCodeButton() {
            tvSendCode.setEnabled(true);
        }
    };

    public void checkPasswordEditTextLogin(String content) {
        inputPwd = content.length() > 5;
    }

    public boolean checkPhoneEditTextLogin(String phone) {
        return !TextUtils.isEmpty(phone) && phone.replace(" ", "").length() == 11;
    }
}
