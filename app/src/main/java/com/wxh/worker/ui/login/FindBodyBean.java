package com.wxh.worker.ui.login;

import android.support.annotation.Keep;

@Keep
public class FindBodyBean {

    public String phone;
    public String smsCode;
    public String password;
    public String confirmPassword;

}
