package com.wxh.worker.ui.pagelist;

import android.arch.paging.DataSource;
import android.arch.paging.PagedList;
import android.arch.paging.RxPagedListBuilder;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.utilCode.base.mvp.MvpView;
import com.utilCode.utils.RxUtils;
import com.utilCode.widget.recycleadpter.BasePageRcvAdapter;
import com.utilCode.widget.recycleadpter.NetworkState;
import com.wxh.worker.R;
import com.wxh.worker.json.AndroidLib;
import com.common.mvp.BaseMvpActivity;
import com.common.retrofit.RxObserver;

import butterknife.BindView;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import timber.log.Timber;

public class PageByPageListsActivity extends BaseMvpActivity<PageListsView, PageListsPresent> implements PageListsView {

    @BindView(R.id.toolbar_back)
    FrameLayout toolbar_back;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.rcv_list_id)
    RecyclerView rcv_list_id;
    @BindView(R.id.refresh_text)
    TextView refresh_text;
    @BindView(R.id.change_text)
    TextView change_text;

    public static final int pageSize = 10;
    PageByPageListAdapter mPageByPageListAdapter;
    Flowable<PagedList<AndroidLib>> mFlowablePagedList;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pagebyid_layout;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar_title.setText("分页方式--PAGE");

        PagedList.Config config = new PagedList.Config.Builder().setPageSize(pageSize)
                .setInitialLoadSizeHint(pageSize * 2)
                .setEnablePlaceholders(false)   //Item为null时进行调用
                .build();

        mFlowablePagedList = new RxPagedListBuilder<>(new DataSource.Factory<Long, AndroidLib>() {
            @Override
            public DataSource<Long, AndroidLib> create() {
                return getPresenter().new PageByPageDataSource();
            }
        }, config).buildFlowable(BackpressureStrategy.LATEST);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PageByPageListsActivity.this);
        rcv_list_id.setLayoutManager(linearLayoutManager);
        mPageByPageListAdapter = new PageByPageListAdapter(PageByPageListsActivity
                .this, new BasePageRcvAdapter.RetryCallback() {
            @Override
            public void onErrorRetry() {
                getPresenter().errorRetry();
            }
        });
        rcv_list_id.setAdapter(mPageByPageListAdapter);

        refresh_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //刷新数据
                mPageByPageListAdapter.getCurrentList().getDataSource().invalidate();
            }
        });

        change_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPageByPageListAdapter.getCurrentList().get(0).setFull_name("jbwplay");
                mPageByPageListAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        mFlowablePagedList.toObservable()
                .compose(RxUtils.<PagedList<AndroidLib>>applySchedulersLifeCycle((MvpView) this))
                .subscribe(new RxObserver<PagedList<AndroidLib>>() {
                    @Override
                    public void onNext(PagedList<AndroidLib> androidLibs) {
                        mPageByPageListAdapter.submitList(androidLibs);
                    }
                });
    }

    @Override
    public void onLoadDoing() {
        Timber.e("Thread = " + Thread.currentThread().getName());
        mPageByPageListAdapter.setNetworkState(NetworkState.LOADING);
    }

    @Override
    public void onLoadDone() {
        Timber.e("Thread = " + Thread.currentThread().getName());
        mPageByPageListAdapter.setNetworkState(NetworkState.LOADED);
    }

    @Override
    public void onLoadComplete() {
        Timber.e("Thread = " + Thread.currentThread().getName());
        mPageByPageListAdapter.setNetworkState(NetworkState.complete(""));
    }

    @Override
    public void onLoadError() {
        Timber.e("Thread = " + Thread.currentThread().getName());
        mPageByPageListAdapter.setNetworkState(NetworkState.error(""));
    }

}
