package com.wxh.worker.ui.fragment.order;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.common.utils.MoneyUtil;
import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;
import com.wxh.worker.json.OrderListBean;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderAdapter extends BaseRecycleViewAdapter<OrderListBean> {

    private OrderClickListener clickListener;

    public OrderAdapter(Context context, OrderClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, OrderListBean item) {
        if (holder instanceof OrderHolder) {
            OrderHolder orderHolder = (OrderHolder) holder;
            setTextDrawable(orderHolder.tvItemOrderNumber, item.type);
            orderHolder.tvItemOrderNumber.setText("订单编号：" + item.orderNo);
            setOrderAmount(orderHolder.tvItemOrderAmount, MoneyUtil.parseMoneyWithComma(item.otherAmt));
            orderHolder.tvItemAddress.setText(item.addressDetail);
            orderHolder.tvItemTime.setText(item.appointmentTime);
            orderHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null) {
                        clickListener.onOrderItemClick(item);
                    }
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new OrderHolder(View.inflate(mContext, R.layout.item_order_list, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    public void setTextDrawable(TextView textView, String type){
        if (TextUtils.equals(type, "1")) {
            Drawable drawableLeft = mContext.getResources().getDrawable(R.drawable.ic_order_measure);
            textView.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, null, null, null);
        } else if (TextUtils.equals(type, "2")) {
            Drawable drawableLeft = mContext.getResources().getDrawable(R.drawable.ic_order_install);
            textView.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, null, null, null);
        }
        textView.setCompoundDrawablePadding(10);
    }

    public void setOrderAmount(TextView textView, String amount) {
        if(TextUtils.isEmpty(amount)){
            textView.setText("");
            return;
        }
        SpannableStringBuilder style = new SpannableStringBuilder();
        //设置文字
        style.append(amount + " 元");
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#E60000"));
        style.setSpan(colorSpan, 0, style.length() - 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //配置给TextView
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(style);
    }

    class OrderHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_item_order_number)
        TextView tvItemOrderNumber;
        @BindView(R.id.tv_item_order_amount)
        TextView tvItemOrderAmount;
        @BindView(R.id.tv_item_address)
        TextView tvItemAddress;
        @BindView(R.id.tv_item_time)
        TextView tvItemTime;

        OrderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public interface OrderClickListener {
        void onOrderItemClick(OrderListBean item);
    }
}
