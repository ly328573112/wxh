package com.wxh.worker.ui.message;

import com.common.config.MyApi;
import com.utilCode.base.mvp.RxMvpPresenter;

import javax.inject.Inject;

public class MessagePresent extends RxMvpPresenter<MessageView> {

    MyApi mMyApi;

    @Inject
    public MessagePresent(MyApi mMyApi) {
        this.mMyApi = mMyApi;
    }

}
