package com.wxh.worker.ui.window_type;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;
import com.wxh.worker.ui.window_type.bean.WindowTypeBean;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WindowAdapter extends BaseRecycleViewAdapter<WindowTypeBean> {

    WindowClickListener windowClickListener;

    public WindowAdapter(Context context, WindowClickListener windowClickListener) {
        super(context);
        this.windowClickListener = windowClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, WindowTypeBean item) {
        if (holder instanceof WindowHolder) {
            WindowHolder windowHolder = (WindowHolder) holder;
            setWindowImg(windowHolder.ivTypeImg, item);
            windowHolder.tvTypeName.setText(item.windowName);
            windowHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    item.windowSeelct = true;
                    windowClickListener.onItemWindowType(item);
                }
            });
        }
    }

    private void setWindowImg(ImageView ivTypeImg, WindowTypeBean item) {
        switch (item.windowIndex) {
            case 1:
                if (item.windowSeelct) {
                    ivTypeImg.setImageResource(R.drawable.ic_a_s);
                } else {
                    ivTypeImg.setImageResource(R.drawable.ic_a_d);
                }
                break;
            case 2:
                if (item.windowSeelct) {
                    ivTypeImg.setImageResource(R.drawable.ic_b_s);
                } else {
                    ivTypeImg.setImageResource(R.drawable.ic_b_d);
                }
                break;
            case 3:
                if (item.windowSeelct) {
                    ivTypeImg.setImageResource(R.drawable.ic_c_s);
                } else {
                    ivTypeImg.setImageResource(R.drawable.ic_c_d);
                }
                break;
            case 4:
                if (item.windowSeelct) {
                    ivTypeImg.setImageResource(R.drawable.ic_d_s);
                } else {
                    ivTypeImg.setImageResource(R.drawable.ic_d_d);
                }
                break;
            case 5:
                if (item.windowSeelct) {
                    ivTypeImg.setImageResource(R.drawable.ic_e_s);
                } else {
                    ivTypeImg.setImageResource(R.drawable.ic_e_d);
                }
                break;
            case 6:
                if (item.windowSeelct) {
                    ivTypeImg.setImageResource(R.drawable.ic_f_s);
                } else {
                    ivTypeImg.setImageResource(R.drawable.ic_f_d);
                }
                break;
            case 7:
                if (item.windowSeelct) {
                    ivTypeImg.setImageResource(R.drawable.ic_g_s);
                } else {
                    ivTypeImg.setImageResource(R.drawable.ic_g_d);
                }
                break;
            case 8:
                if (item.windowSeelct) {
                    ivTypeImg.setImageResource(R.drawable.ic_h_s);
                } else {
                    ivTypeImg.setImageResource(R.drawable.ic_h_d);
                }
                break;
            case 9:
                if (item.windowSeelct) {
                    ivTypeImg.setImageResource(R.drawable.ic_i_s);
                } else {
                    ivTypeImg.setImageResource(R.drawable.ic_i_d);
                }
                break;
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new WindowHolder(View.inflate(mContext, R.layout.item_window_type, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class WindowHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_type_img)
        ImageView ivTypeImg;
        @BindView(R.id.tv_type_name)
        TextView tvTypeName;

        WindowHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface WindowClickListener {

        void onItemWindowType(WindowTypeBean item);

    }

}
