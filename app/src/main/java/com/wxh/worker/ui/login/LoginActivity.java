package com.wxh.worker.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.MediaStoreSignature;
import com.common.config.Api;
import com.common.config.Global;
import com.common.mvp.BaseMvpActivity;
import com.common.retrofit.json.Data;
import com.common.utils.FormatUtil;
import com.common.utils.LogUtil;
import com.common.utils.PhoneNumTools;
import com.common.widght.EditTextWithDel;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.ui.HomeActivity;

import butterknife.BindView;

public class LoginActivity extends BaseMvpActivity<LRView, LRPresent> implements LRView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.login_phonenumber)
    EditTextWithDel loginPhonenumber;
    @BindView(R.id.login_password)
    EditTextWithDel loginPassword;
    @BindView(R.id.iv_login_eye)
    CheckBox ivLoginEye;
    @BindView(R.id.tv_login_forget_password)
    TextView tvLoginForgetPassword;
    @BindView(R.id.tv_login_register)
    TextView tvLoginRegister;
    @BindView(R.id.login)
    TextView login;
    @BindView(R.id.et_login_v_code)
    EditTextWithDel etLoginVCode;
    @BindView(R.id.tv_send_v_code)
    Button tvSendVCode;
    @BindView(R.id.et_register_figure_code)
    EditTextWithDel etFigureCode;
    @BindView(R.id.iv_send_figure_code)
    ImageView ivFigureCode;
    @BindView(R.id.ll_code_pic)
    LinearLayout llCodePic;

    LoadingDialog mLoadingDialog;
    private boolean inputPwd = false;
    private String phone;
    private RequestOptions requestOptions = new RequestOptions();
    private String figureCode;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return R.layout.activity_login;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        barColor = false;
        super.setTranslateStatusBar(barColor);
    }

    @Override
    protected void setupView() {
        toolbarBack.setVisibility(View.GONE);
        toolbarTitle.setText("登录/注册");
        tvLoginForgetPassword.setOnClickListener(this);
        tvLoginRegister.setOnClickListener(this);
        login.setOnClickListener(this);
        tvSendVCode.setOnClickListener(this);
        ivFigureCode.setOnClickListener(this);
        //禁止输入框输入空格和换行符号2
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.equals(" ") || source.toString().contentEquals("\n")) {
                    return "";
                } else {
                    return null;
                }
            }
        };
        loginPhonenumber.setText(Global.getUserPhotoNumber());
        loginPhonenumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(11)});
        loginPhonenumber.addTextChangedListener(new TextWatcherListener(1));
        loginPassword.addTextChangedListener(new TextWatcherListener(2));
        ivLoginEye.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                loginPassword.setInputType(ivLoginEye.isChecked() ?
                        InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD :
                        InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                loginPassword.setSelection(loginPassword.length());
            }
        });

        etFigureCode.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_login_forget_password:
                FindPasswordActivity.startActivity(this,
                        loginPhonenumber.getText().toString().replaceAll(" ", ""));
                break;
            case R.id.tv_login_register:
                RegisterActivity.startActivity(this,
                        loginPhonenumber.getText().toString().replaceAll(" ", ""));
                break;
            case R.id.iv_send_figure_code:
                phone = loginPhonenumber.getText().toString().replace(" ", "");
                if (!checkPhoneEditTextLogin(phone)) {
                    ToastUtils.showShortToastSafe("请输入正确的手机号");
                    return;
                }
                LogUtil.i(Api.CODE_PIC + "?phone=" + phone);
                Glide.with(LoginActivity.this).load(Api.CODE_PIC + "?phone=" + phone).apply(requestOptions).into(ivFigureCode);
                break;
            case R.id.tv_send_v_code:
                phone = loginPhonenumber.getText().toString().replace(" ", "");
                figureCode = etFigureCode.getText().toString();
                if (!checkPhoneEditTextLogin(phone)) {
                    ToastUtils.showShortToastSafe("请输入正确的手机号");
                    return;
                } else if (TextUtils.isEmpty(figureCode)) {
                    ToastUtils.showShortToastSafe("请输入图形验证码");
                    return;
                }
                getPresenter().sendSMS(figureCode, phone, validateCodeListener);
                break;
            case R.id.login:
                phone = loginPhonenumber.getText().toString().replaceAll(" ", "");
                String loginVCode = etLoginVCode.getText().toString().trim();
                figureCode = etFigureCode.getText().toString();
                if (!checkPhoneEditTextLogin(phone)) {
                    ToastUtils.showShortToastSafe("请输入正确的手机号");
                    return;
                } else if (TextUtils.isEmpty(figureCode)) {
                    ToastUtils.showShortToastSafe("请输入图形验证码");
                    return;
                } else if (TextUtils.isEmpty(loginVCode) || loginVCode.length() < 4 || loginVCode.length() > 6) {
                    ToastUtils.showShortToastSafe("请输入正确的验证码");
                    return;
                }
                if (mLoadingDialog == null) {
                    mLoadingDialog = new LoadingDialog();
                }
                if (!mLoadingDialog.isVisible()) {
                    mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                }
                getPresenter().loginOn(phone, figureCode, loginVCode);
//                getPresenter().loginOn(phone, loginVCode);
                break;
        }
    }

    private LRView.ValidateCodeListener validateCodeListener = new LRView.ValidateCodeListener() {

        @Override
        public void disableValidateCodeButton() {
            tvSendVCode.setEnabled(false);
        }

        @Override
        public void countValidateCodeButton(long number) {
            tvSendVCode.setText("已发送(" + number + ")");
        }

        @Override
        public void resendValidateCodeButton() {
            tvSendVCode.setEnabled(true);
            tvSendVCode.setText("重发验证码");
        }

        @Override
        public void enableValidateCodeButton() {
            tvSendVCode.setEnabled(true);
        }
    };

    class TextWatcherListener implements TextWatcher {

        int type;

        public TextWatcherListener(int type) {
            this.type = type;
        }


        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            if (type == 1) {
//                FormatUtil.addEditSpace(charSequence, start, before, loginPhonenumber);
                if(charSequence.length() == 11){
                    setPicCode(charSequence.toString());
                }
            } else if (type == 2) {
                checkPasswordEditTextLogin(charSequence.toString());
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    public void checkPasswordEditTextLogin(String content) {
        inputPwd = content.length() > 5;
    }

    public void setPicCode(String phone){
        boolean checkMobile = PhoneNumTools.checkMobile(phone);
        if(checkMobile){
            phone = loginPhonenumber.getText().toString();
            Glide.with(LoginActivity.this).load(Api.CODE_PIC + "?phone=" + phone).apply(requestOptions).into(ivFigureCode);
            llCodePic.setVisibility(View.VISIBLE);
        } else {
            llCodePic.setVisibility(View.GONE);
            ToastUtils.showLongToastSafe("请输入正确的手机号");
        }
    }

    public boolean checkPhoneEditTextLogin(String phone) {
        return !TextUtils.isEmpty(phone) && phone.replace(" ", "").length() == 11;
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);
        String photoNumber = Global.getUserPhotoNumber();
        if(!TextUtils.isEmpty(photoNumber)){
            setPicCode(photoNumber);
        }
//        LogUtil.i(Api.CODE_PIC + "?phone=" + phone);
//        Glide.with(this).load(Api.CODE_PIC + "?phone=" + phone).apply(requestOptions).into(ivFigureCode);
    }

    @Override
    public void onSMS_Success() {

    }

    @Override
    public void onRegister() {

    }

    @Override
    public void onLoginSuccess(String status) {
        mLoadingDialog.dismissAllowingStateLoss();
        if (TextUtils.equals(status, "2")) {
            PerfectMessageActivity.startActivity(this);
        } else {
            HomeActivity.startActivity(this);
        }
        finish();
    }

    @Override
    public void onFail(int type, String message) {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showShortToast(message);
        LogUtil.i(Api.CODE_PIC + "?phone=" + phone);
        Glide.with(LoginActivity.this).load(Api.CODE_PIC + "?phone=" + phone).apply(requestOptions).into(ivFigureCode);
    }
}
