package com.wxh.worker.ui.fragment.message;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.common.mvp.BaseMvpFragment;
import com.wxh.worker.R;
import com.wxh.worker.ui.HomeActivity;
import com.wxh.worker.ui.fragment.message.bean.MessageBean;
import com.wxh.worker.ui.message.MessageDetailActivity;
import com.wxh.worker.ui.order.OrderDetailActivity;

import java.util.List;

import butterknife.BindView;

public class MessageFragment extends BaseMvpFragment<MessageFmtView, MessageFmtPresent> implements MessageFmtView {

    @BindView(R.id.ptr_fresh_id)
    SwipeRefreshLayout ptr_fresh_id;
    @BindView(R.id.rv_message_list)
    RecyclerView rvMessageList;
    private MessageAdapter messageAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_message;
    }

    @Override
    protected void initInject() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected void setupView(View rootView) {
        ptr_fresh_id.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.c_E60000));
        ptr_fresh_id.setOnRefreshListener(onRefreshListener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvMessageList.setLayoutManager(linearLayoutManager);
        messageAdapter = new MessageAdapter(getContext(), messageClickListener);
        rvMessageList.setAdapter(messageAdapter);

    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        ptr_fresh_id.postDelayed(new Runnable() {
            @Override
            public void run() {
                ptr_fresh_id.setRefreshing(true);
                getPresenter().getNewsLists();
            }
        }, 50);

    }

    private MessageAdapter.MessageClickListener messageClickListener =
            new MessageAdapter.MessageClickListener() {
                @Override
                public void onMessageItemClick(MessageBean item) {
                    getPresenter().updateNews(item.id);
                    if (TextUtils.equals(item.newsType, "1")) {
                        OrderDetailActivity.startActivity(getActivity(), item.orderId, null);
                    } else {
                        MessageDetailActivity.startActivity(getActivity(), item.title, item.messages);
                    }
                }
            };

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            getPresenter().getNewsLists();
        }
    };

    @Override
    public void onMessageSuccess(List<MessageBean> messageBeanList) {
        if (ptr_fresh_id.isRefreshing()) {
            ptr_fresh_id.setRefreshing(false);
        }
        if (messageBeanList == null || messageBeanList.size() == 0) {
            messageAdapter.resetData(messageBeanList);
            messageAdapter.setEmptyView(R.layout.empty_view);
//            orderAdapter.loadMoreEnd(false);
//            orderAdapter.loadMoreComplete();
        } else {
            messageAdapter.resetData(messageBeanList);
            int statusNumber = 0;
            for (MessageBean messageBean : messageBeanList){
                if (TextUtils.equals("0", messageBean.newsStatus)) {
                    statusNumber ++;
                }
            }
            ((HomeActivity) getActivity()).onMsgSuccess(statusNumber);
        }
    }

    @Override
    public void onFail() {
        if (ptr_fresh_id.isRefreshing()) {
            ptr_fresh_id.setRefreshing(false);
        }
    }
}
