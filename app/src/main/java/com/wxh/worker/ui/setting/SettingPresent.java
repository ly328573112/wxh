package com.wxh.worker.ui.setting;

import android.support.annotation.NonNull;

import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.facebook.stetho.common.LogUtil;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;

import javax.inject.Inject;

public class SettingPresent extends RxMvpPresenter<SettingView> {

    MyApi mMyApi;

    @Inject
    public SettingPresent(MyApi mMyApi) {
        this.mMyApi = mMyApi;
    }

    public void getVersion() {
        mMyApi.getVersion()
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, true)) {
                            String result = data.getResult().toString().replace(".", "");
                            getView().onVersionSuccess(Integer.parseInt(result));
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }
}
