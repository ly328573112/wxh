package com.wxh.worker.ui.measure;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wxh.worker.event.MeasureAddEvent;
import com.wxh.worker.event.MeasureDeleteEvent;
import com.common.mvp.BaseMvpActivity;
import com.utilCode.dialog.LoadingDialog;
import com.wxh.worker.R;
import com.wxh.worker.json.MeasureListBean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;

public class MeasureListActivity extends BaseMvpActivity<MeasureView, MeasurePresent> implements MeasureView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.srl)
    SwipeRefreshLayout mSrl;
    @BindView(R.id.rv_order_list)
    RecyclerView rvOrderList;
    @BindView(R.id.tv_button_red_e)
    TextView tvButtonRedE;

    private String orderId;

    LoadingDialog mLoadingDialog;
    private MeasureListAdapter measureListAdapter;

    public static void startActivity(Activity activity, String orderId) {
        Intent intent = new Intent(activity, MeasureListActivity.class);
        intent.putExtra("orderId", orderId);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_measure_list;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarTitle.setText("测量数据");
        toolbarBack.setOnClickListener(this);
        tvButtonRedE.setOnClickListener(this);
        tvButtonRedE.setVisibility(View.VISIBLE);
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }

        mSrl.setColorSchemeColors(ContextCompat.getColor(this, R.color.c_E60000));
        mSrl.setProgressViewOffset(true, -20, 100);
        mSrl.setOnRefreshListener(refreshListener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvOrderList.setLayoutManager(linearLayoutManager);
        measureListAdapter = new MeasureListAdapter(this, measureClickListener);
        rvOrderList.setAdapter(measureListAdapter);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        Intent intent = getIntent();
        orderId = intent.getStringExtra("orderId");
        if (!mLoadingDialog.isVisible()) {
            mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
        }
        getPresenter().getQueryMeasureRecord(orderId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            getPresenter().getQueryMeasureRecord(orderId);
        }
    };

    private MeasureListAdapter.MeasureClickListener measureClickListener = new MeasureListAdapter.MeasureClickListener() {
        @Override
        public void onMeasureItemClick(MeasureListBean item) {
            MeasureDetailActivity.startActivity(MeasureListActivity.this, item);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.tv_button_red_e:
                MeasureAddDataActivity.startActivity(this, orderId);
                break;
        }
    }

    @Override
    public void onMeasureList(List<MeasureListBean> measureListBeanList) {
        if (mSrl.isRefreshing()) {
            mSrl.setRefreshing(false);
        }
        mLoadingDialog.dismissAllowingStateLoss();
        if (measureListBeanList == null || measureListBeanList.size() == 0) {
            measureListAdapter.setEmptyView(R.layout.empty_view);
            measureListAdapter.loadMoreEnd(false);
            measureListAdapter.loadMoreComplete();
        } else {
            measureListAdapter.resetData(measureListBeanList);
        }
    }

    @Override
    public void onFail() {
        mLoadingDialog.dismissAllowingStateLoss();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MeasureDeleteEvent event) {
        getPresenter().getQueryMeasureRecord(orderId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MeasureAddEvent event) {
        getPresenter().getQueryMeasureRecord(orderId);
    }
}
