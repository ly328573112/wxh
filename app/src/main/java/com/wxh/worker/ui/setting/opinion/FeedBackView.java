package com.wxh.worker.ui.setting.opinion;

import com.utilCode.base.mvp.MvpView;

public interface FeedBackView extends MvpView {

    void onSuccess();

    void onFail();

}
