package com.wxh.worker.ui.order;

import android.support.annotation.NonNull;

import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.common.utils.LogUtil;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.json.OrderDetailBean;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class OrderDetailPresent extends RxMvpPresenter<OrderDetailView> {

    MyApi mMyApi;

    @Inject
    public OrderDetailPresent(MyApi mMyApi) {
        this.mMyApi = mMyApi;
    }

    public void getOrdersDetails(String orderId) {
        mMyApi.getOrdersDetails(orderId)
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<OrderDetailBean>>() {

                    @Override
                    public void onNext(@NonNull Data<OrderDetailBean> data) {
                        LogUtil.d(data.toString());
                        if (checkJsonCode(data, false)) {
                            getView().onOrderDetailSuccess(data.getResult());
                        } else {
                            getView().onFail(1);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(1);
                    }
                });
    }

    public void acceptOrder(String orderId) {
        mMyApi.acceptOrder(orderId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        LogUtil.d(data.toString());
                        if (checkJsonCode(data, false)) {
                            getView().onAcceptSuccess();
                        } else {
                            getView().onFail(2);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(2);
                    }
                });
    }

    public void refuseOrder(String orderId) {
        mMyApi.refuseOrder(orderId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        LogUtil.d(data.toString());
                        if (checkJsonCode(data, false)) {
                            getView().onRefuseSuccess();
                        } else {
                            getView().onFail(3);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(3);
                    }
                });
    }


    public void postAppointment(String orderId, String serverTime, String mark) {
        JSONObject requestAttributes = new JSONObject();
        try {
            requestAttributes.put("orderId", orderId);
            requestAttributes.put("serverTime", serverTime);
            requestAttributes.put("mark", mark);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), requestAttributes.toString());
        mMyApi.postAppointment(requestBody)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        LogUtil.d(data.toString());
                        if (checkJsonCode(data, false)) {
                            getView().onAppointmentSuccess();
                        } else {
                            getView().onFail(4);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(4);
                    }
                });
    }
}
