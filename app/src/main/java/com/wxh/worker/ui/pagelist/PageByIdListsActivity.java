package com.wxh.worker.ui.pagelist;

import android.arch.paging.DataSource;
import android.arch.paging.PagedList;
import android.arch.paging.RxPagedListBuilder;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.utilCode.base.mvp.MvpView;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.R;
import com.wxh.worker.json.GitUser;
import com.common.mvp.BaseMvpActivity;
import com.common.retrofit.RxObserver;

import butterknife.BindView;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;

public class PageByIdListsActivity extends BaseMvpActivity<PageListsView, PageListsPresent> implements PageListsView {

    @BindView(R.id.toolbar_back)
    FrameLayout toolbar_back;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.rcv_list_id)
    RecyclerView rcv_list_id;

    public static final int pageSize = 10;
    PageByIdListAdapter mPageByIdListAdapter;
    Flowable<PagedList<GitUser>> mFlowablePagedList;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pagebyid_layout;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar_title.setText("分页方式--ID");

        PagedList.Config config = new PagedList.Config.Builder().setPageSize(pageSize)
                .setInitialLoadSizeHint(pageSize * 2)
                .setEnablePlaceholders(false)
                .build();

        mFlowablePagedList = new RxPagedListBuilder<>(new DataSource.Factory<Long, GitUser>() {
            @Override
            public DataSource<Long, GitUser> create() {
                return getPresenter().new PageByIdDataSource();
            }
        }, config).buildFlowable(BackpressureStrategy.LATEST);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PageByIdListsActivity.this);
        rcv_list_id.setLayoutManager(linearLayoutManager);
        mPageByIdListAdapter = new PageByIdListAdapter(PageByIdListsActivity.this);
        rcv_list_id.setAdapter(mPageByIdListAdapter);

    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        mFlowablePagedList.toObservable()
                .compose(RxUtils.<PagedList<GitUser>>applySchedulersLifeCycle((MvpView) this))
                .subscribe(new RxObserver<PagedList<GitUser>>() {
                    @Override
                    public void onNext(PagedList<GitUser> debugInfos) {
                        mPageByIdListAdapter.submitList(debugInfos);
                    }
                });
    }

    @Override
    public void onLoadDoing() {

    }

    @Override
    public void onLoadDone() {

    }

    @Override
    public void onLoadComplete() {

    }

    @Override
    public void onLoadError() {

    }
}
