package com.wxh.worker.ui.measure.bean;

import android.support.annotation.Keep;

import com.wxh.worker.json.MeasureListBean;

import java.util.List;

@Keep
public class MeasureBean {

    public String measureId;//测量id
    public String orderId;//订单ID
    public String homeName;//房间名称
    public String windowType;//窗户类型
    public String width;//宽度
    public String leftLength;//左侧边长
    public String rightLength;//右侧边长
    public String trackType;//轨道类型(1电动直轨2电动弯轨3电动L轨4电动升降直轨5电动升降弯轨)
    public String levelType;//单双轨类型1单轨2双轨
    public String fixType;//单双轨类型1顶装2侧装
    public String ocType;//开合方式类型1单开2双开
    public String heightFlag;
    public String height;//轨道安装高度，默认0小于3.5米，大于输入数值
    public String openFlag;//是否断开 1是 2否
    public String fullFlag;//是否满墙 1是 2否
    public String boxFlag;//是否有窗帘 1有 0无
    public String boxWidth;//窗帘盒宽度：厘米
    public String materialQuality;//安装面材质：1木板、2混凝土、3大理石、4石膏板
    public String linePosition;//电源线位置：1左侧、2右侧、3两侧
    public String remarks;//备注信息
    public String measureTime;//测量时间
    public String status;//1有效0删除
    public List<MeasureListBean.ImagesBean> images;//现场图片url

}
