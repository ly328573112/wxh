package com.wxh.worker.ui.fragment.home;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;
import com.wxh.worker.ui.fragment.home.bean.HomeListBean;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeListAdapter extends BaseRecycleViewAdapter<HomeListBean> {

    private HomeListClickListener clickListener;

    public HomeListAdapter(Context context, HomeListClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, HomeListBean item) {
        if (holder instanceof HomeListHolder) {
            HomeListHolder listHolder = (HomeListHolder) holder;
            setTextDrawable(listHolder.tvHomeListName, item.index);
            listHolder.tvHomeListName.setText(item.name);
            listHolder.tvHomeListValue.setText(item.numberOfstatus);
            listHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(clickListener != null){
                        clickListener.onHomeListItemClick(item);
                    }
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new HomeListHolder(View.inflate(mContext, R.layout.item_home_list, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    public void setTextDrawable(TextView textView, int index){
        Drawable drawableLeft = null;
        if(index == 1){
            drawableLeft = mContext.getResources().getDrawable(R.drawable.ic_home_order_servcie);
        } else if(index == 2){
            drawableLeft = mContext.getResources().getDrawable(R.drawable.ic_home_order_received);
        } else if(index == 3){
            drawableLeft = mContext.getResources().getDrawable(R.drawable.ic_home_order_appointment);
        } else if(index == 4){
            drawableLeft = mContext.getResources().getDrawable(R.drawable.ic_home_order_withdrawal);
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, null, null, null);
        textView.setCompoundDrawablePadding(20);
    }

    class HomeListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_home_list_name)
        TextView tvHomeListName;
        @BindView(R.id.tv_home_list_value)
        TextView tvHomeListValue;

        HomeListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface HomeListClickListener {
        void onHomeListItemClick(HomeListBean item);
    }

}
