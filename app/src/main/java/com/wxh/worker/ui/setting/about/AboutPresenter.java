package com.wxh.worker.ui.setting.about;

import com.common.config.MyApi;
import com.utilCode.base.mvp.RxMvpPresenter;

import javax.inject.Inject;

public class AboutPresenter extends RxMvpPresenter<AboutView> {
    MyApi mApi;

    @Inject
    public AboutPresenter( MyApi api) {
        mApi = api;
    }
}
