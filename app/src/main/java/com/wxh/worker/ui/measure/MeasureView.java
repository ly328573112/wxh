package com.wxh.worker.ui.measure;

import com.utilCode.base.mvp.MvpView;
import com.wxh.worker.json.MeasureListBean;

import java.util.List;

public interface MeasureView extends MvpView {

    void onMeasureList(List<MeasureListBean> measureListBeanList);

    void onFail();

}
