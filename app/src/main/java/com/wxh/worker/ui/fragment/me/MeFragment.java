package com.wxh.worker.ui.fragment.me;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.config.Global;
import com.wxh.worker.event.MasterEvent;
import com.common.mvp.BaseMvpFragment;
import com.wxh.worker.R;
import com.wxh.worker.json.MasterInfoBean;
import com.wxh.worker.ui.me.PersonalDataActivity;
import com.wxh.worker.ui.money.CashWithdrawalActivity;
import com.wxh.worker.ui.setting.SettingActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

public class MeFragment extends BaseMvpFragment<MeFmtView, MeFmtPresent> implements MeFmtView, View.OnClickListener {

    @BindView(R.id.rl_personal)
    RelativeLayout rlPersonal;
    @BindView(R.id.iv_master_head)
    ImageView ivMasterHead;
    @BindView(R.id.tv_master_name)
    TextView tvMasterName;
    @BindView(R.id.tv_master_activation)
    TextView tvMasterActivation;
    @BindView(R.id.tv_master_phone_number)
    TextView tvMasterPhoneNumber;
    @BindView(R.id.rl_cash_withdrawal)
    RelativeLayout rlCashWithdrawal;
    @BindView(R.id.tv_cash_withdrawal)
    TextView tvCashWithdrawal;
    @BindView(R.id.rl_me_Bond)
    RelativeLayout rlMeBond;
    @BindView(R.id.rl_me_information)
    RelativeLayout rlMeInformation;
    @BindView(R.id.rl_me_evaluate)
    RelativeLayout rlMeEvaluate;
    @BindView(R.id.rl_setting)
    RelativeLayout rlSetting;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_me;
    }

    @Override
    protected void initInject() {
        getFragmentComponent().inject(this);
    }

    @Override
    protected void setupView(View rootView) {
        initClickListener();
    }

    private void initClickListener() {
        rlPersonal.setOnClickListener(this);
        rlCashWithdrawal.setOnClickListener(this);
        rlMeBond.setOnClickListener(this);
        rlMeInformation.setOnClickListener(this);
        rlMeEvaluate.setOnClickListener(this);
        rlSetting.setOnClickListener(this);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        MasterInfoBean masterInfoBean = Global.getMasterInfoBean();
        tvMasterName.setText(masterInfoBean.emergencyContactsName);
        tvMasterPhoneNumber.setText(masterInfoBean.emergencyContactsPhone);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_personal:
                break;
            case R.id.rl_cash_withdrawal:
                CashWithdrawalActivity.startActivity(getActivity());
                break;
            case R.id.rl_me_Bond:
                break;
            case R.id.rl_me_information:
                PersonalDataActivity.startActivity(getActivity());
                break;
            case R.id.rl_me_evaluate:
                break;
            case R.id.rl_setting:
                SettingActivity.startActivity(getActivity());
                break;
            default:
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MasterEvent event) {
        MasterInfoBean masterInfoBean = Global.getMasterInfoBean();
        tvMasterName.setText(masterInfoBean.emergencyContactsName);
        tvMasterPhoneNumber.setText(masterInfoBean.emergencyContactsPhone);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
