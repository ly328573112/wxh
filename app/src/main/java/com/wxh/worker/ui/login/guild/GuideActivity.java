package com.wxh.worker.ui.login.guild;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.utilCode.base.BaseActivity;
import com.utilCode.utils.DisplayUtils;
import com.wxh.worker.R;
import com.wxh.worker.ui.login.LoginActivity;

import java.util.ArrayList;

public class GuideActivity extends BaseActivity {

    ViewPager vpPager;
//    LinearLayout llAllDots;
//    ImageView ivLightDots;

    private int mDistance;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_guild_layout;
    }

    @Override
    protected void setupView() {
        DisplayUtils.fullScreen(GuideActivity.this, true);

        vpPager = findViewById(R.id.vp_pager);
//        llAllDots = findViewById(R.id.ll_all_dots);
//        ivLightDots = findViewById(R.id.iv_light_dots);

        ArrayList<View> guildViewList = new ArrayList<>();
        guildViewList.add(LayoutInflater.from(this).inflate(R.layout.guildlayoutone, null));
        guildViewList.add(LayoutInflater.from(this).inflate(R.layout.guildlayouttwo, null));
        ViewGroup viewgroup = (ViewGroup) LayoutInflater.from(this).inflate(R.layout.guildlayoutthree, null);
        viewgroup.findViewById(R.id.finish).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(GuideActivity.this, LoginActivity.class));
                        finish();
                    }
                });
        guildViewList.add(viewgroup);
        ViewPageAdapter viewPageAdapter = new ViewPageAdapter(guildViewList);
        vpPager.setAdapter(viewPageAdapter);

//        addDots();

        vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                float leftMargin = mDistance * (position + positionOffset);
//                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ivLightDots.getLayoutParams();
//                params.leftMargin = (int) leftMargin;
//                ivLightDots.setLayoutParams(params);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

//    private void addDots() {
//
//        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        layoutParams.setMargins(0, 0, DisplayUtils.dp2px(15), 0);
//        layoutParams.width = DisplayUtils.dp2px(10);
//        layoutParams.height = DisplayUtils.dp2px(10);
//        for (int i = 0; i < 3; i++) {
//            ImageView imageview = new ImageView(this);
//            imageview.setImageResource(R.drawable.ic_guideview_normal);
//            llAllDots.addView(imageview, layoutParams);
//        }
//
//        ivLightDots.getViewTreeObserver()
//                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                    @Override
//                    public void onGlobalLayout() {
//                        //获得两个圆点之间的距离
//                        mDistance = llAllDots.getChildAt(1).getLeft() - llAllDots.getChildAt(0).getLeft();
//                        ivLightDots.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                    }
//                });
//    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }

}
