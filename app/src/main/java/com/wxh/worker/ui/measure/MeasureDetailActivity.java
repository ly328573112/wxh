package com.wxh.worker.ui.measure;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wxh.worker.event.MeasureDeleteEvent;
import com.common.mvp.BaseMvpActivity;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.common_view.ImageClickListener;
import com.wxh.worker.common_view.PhotoImageAdapter;
import com.wxh.worker.common_view.bean.PhotoInfo;
import com.wxh.worker.common_view.imgoriginal.OriginalPagerActivity;
import com.wxh.worker.json.MeasureListBean;
import com.wxh.worker.ui.RoomTypeUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MeasureDetailActivity extends BaseMvpActivity<MeasureView, MeasurePresent> implements MeasureView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.tv_measure_room)
    TextView tvMeasureRoom;
    @BindView(R.id.tv_measure_material)
    TextView tvMeasureMaterial;
    @BindView(R.id.tv_measure_height)
    TextView tvMeasureHeight;
    @BindView(R.id.tv_measure_height_s)
    TextView tvMeasureHeightS;

    @BindView(R.id.tv_track_type)
    TextView tvTrackType;//轨道类型
    @BindView(R.id.tv_track_width)
    TextView tvTrackWidth;//轨道尺寸
    @BindView(R.id.tv_track_number)
    TextView tvTrackNumber;//单轨/双轨
    @BindView(R.id.tv_track_height)
    TextView tvTrackHeight;//开合方式
    @BindView(R.id.tv_track_install)
    TextView tvTrackInstall;//安装方式
    @BindView(R.id.tv_track_curtain)
    TextView tvTrackCurtain;//是否满墙
    @BindView(R.id.tv_track_used)
    TextView tvTrackUsed;//是否断开

    @BindView(R.id.tv_measure_remarks)
    TextView tvMeasureRemarks;
    @BindView(R.id.tv_button_red_c)
    TextView tvButtonRedC;

    @BindView(R.id.rv_measure_img)
    RecyclerView rvMeasureImg;

    LoadingDialog mLoadingDialog;

    private List<PhotoInfo> photoList = new ArrayList<>();// 照片uri列表
    private MeasureListBean measureBean;
    private PhotoImageAdapter imageAdapter;

    public static void startActivity(Activity activity, MeasureListBean measureListBean) {
        Intent intent = new Intent(activity, MeasureDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("measureList", measureListBean);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_measure_detail;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarBack.setOnClickListener(this);
        tvButtonRedC.setOnClickListener(this);
        tvButtonRedC.setVisibility(View.VISIBLE);
        tvButtonRedC.setText("删除测量记录");
        toolbarTitle.setText("测量数据");

        GridLayoutManager layoutManager = new GridLayoutManager(this, 5, LinearLayoutManager.VERTICAL, false);
        rvMeasureImg.setLayoutManager(layoutManager);
        rvMeasureImg.setHasFixedSize(true);
        imageAdapter = new PhotoImageAdapter(this);
        rvMeasureImg.setAdapter(imageAdapter);
        rvMeasureImg.setItemAnimator(new DefaultItemAnimator());
        imageAdapter.setImageClickListener(imageClickListener);

        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        measureBean = (MeasureListBean) intent.getSerializableExtra("measureList");
        if (measureBean == null) {
            return;
        }
//        getPresenter().getQuerySingleMeasureRecord(measureBean.measureId);
        setImgData();
        setTextData();
    }

    private void setImgData() {
        if (measureBean == null || measureBean.images.size() == 0) {
            return;
        }
        for (int i = 0; i < measureBean.images.size(); i++) {
            /**
             * 目前使用Glide（4.7.1）加载图片，需要通过ssl来信任所有网络图片访问的证书，后期再处理，暂时改成http
             */
            PhotoInfo photoInfo = null;
            String url = measureBean.images.get(i).url;
            if (url.startsWith("https")) {
                photoInfo = new PhotoInfo(i + 1, url.replace("https", "http"), false);
            } else {
                photoInfo = new PhotoInfo(i + 1, url, false);
            }
            photoList.add(photoInfo);
        }
        imageAdapter.resetData(photoList);
    }

    private ImageClickListener imageClickListener = new ImageClickListener() {
        @Override
        public void openCamera(int position) {
            ArrayList<String> arrayList = new ArrayList<>();
            for (int i = 0; i < photoList.size(); i++) {
                arrayList.add(photoList.get(i).photoUri);
            }
            OriginalPagerActivity.start(MeasureDetailActivity.this, arrayList, position, false);
        }

        @Override
        public void onDeleteImage(int position) {

        }
    };

    private void setTextData() {
        tvMeasureRoom.setText(measureBean.homeName);
        tvMeasureMaterial.setText(RoomTypeUtil.getMterialQuality(measureBean.materialQuality));
        String sn = TextUtils.isEmpty(measureBean.height) ? "" : measureBean.height + " 米";
        tvMeasureHeight.setText(sn);
        setOrderAmount(tvMeasureHeight, sn, 0, sn.length() - 1);
        String sh = TextUtils.isEmpty(measureBean.boxWidth) ? "" : measureBean.boxWidth + " 厘米";
        tvMeasureHeightS.setText(sh);
        setOrderAmount(tvMeasureHeightS, sh, 0, sh.length() - 2);

        tvTrackType.setText(RoomTypeUtil.getTrackType(measureBean.trackType));
        String windowSize = RoomTypeUtil.getWindowSize(measureBean.width, measureBean.leftLength, measureBean.rightLength);
        tvTrackWidth.setText(windowSize);
        setOrderAmount(tvTrackWidth, windowSize, 0, windowSize.length() - 2);
        tvTrackNumber.setText(RoomTypeUtil.getLevelType(measureBean.levelType));
        tvTrackHeight.setText(RoomTypeUtil.getOcType(measureBean.ocType));
        tvTrackInstall.setText(RoomTypeUtil.getLayerType(measureBean.fixType));
        tvTrackCurtain.setText(RoomTypeUtil.getFullFlag(measureBean.fullFlag));
        tvTrackUsed.setText(RoomTypeUtil.getFullFlag(measureBean.openFlag));

        tvMeasureRemarks.setText(measureBean.remarks);
    }

    public void setOrderAmount(TextView textView, String keyName, int start, int end) {
        SpannableStringBuilder style = new SpannableStringBuilder();
        //设置文字
        style.append(keyName);
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#E60000"));
        style.setSpan(colorSpan, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //配置给TextView
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(style);
    }

    @Override
    public void onMeasureList(List<MeasureListBean> measureListBeanList) {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showLongToastSafe("删除测量记录成功");
        EventBus.getDefault().post(new MeasureDeleteEvent());
        finish();
    }

    @Override
    public void onFail() {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showLongToastSafe("删除测量记录失败");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.tv_button_red_c:
                if (!mLoadingDialog.isVisible()) {
                    mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                }
                getPresenter().delMeasureRecord(measureBean.measureId);
                break;
        }
    }
}
