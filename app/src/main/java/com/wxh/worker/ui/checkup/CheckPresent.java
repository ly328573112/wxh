package com.wxh.worker.ui.checkup;

import android.support.annotation.NonNull;

import com.alibaba.fastjson.JSONObject;
import com.common.config.Constant;
import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.facebook.stetho.common.LogUtil;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.ui.login.LRView;


import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class CheckPresent extends RxMvpPresenter<CheckView> {

    MyApi mMyApi;

    @Inject
    public CheckPresent(MyApi mMyApi) {
        this.mMyApi = mMyApi;
    }

    public void postApplyAcceptance(String orderId, String smsCode, String mark, List<Object> url) {
        JSONObject requestAttributes = new JSONObject();
        requestAttributes.put("orderId", orderId);
        requestAttributes.put("smsCode", smsCode);
        requestAttributes.put("mark", mark);
        requestAttributes.put("images", url);
        String toString = requestAttributes.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), toString);
        mMyApi.postApplyAcceptance(requestBody)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false)) {
                            getView().onAcceptanceSuccess();
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

    public void deleteImg(String fileName) {
        mMyApi.deleteImg(fileName)
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        LogUtil.i(data.toString());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }
                });
    }

    public void sendAcceptanceSms(String orderId, LRView.ValidateCodeListener validateCodeListener) {
        validateCodeListener.disableValidateCodeButton();
        mMyApi.sendAcceptanceSms(orderId)
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false)) {
                            ToastUtils.showLongToast("短信发送成功");
                            countValidateCode(validateCodeListener);
                        } else {
                            validateCodeListener.enableValidateCodeButton();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        validateCodeListener.enableValidateCodeButton();
                    }
                });
    }

    public void countValidateCode(LRView.ValidateCodeListener validateCodeListener) {
        Observable.interval(0, 1, TimeUnit.SECONDS)
                /*.takeUntil(new Predicate<Long>() {
                    @Override
                    public boolean test(Long aLong) throws Exception {
                        // 包含临界条件,条件满足则结束
                        return aLong >= Constant.VALIDETE_CODE_WATING_TIME_MAX;
                    }
                })*/
                .takeWhile(new Predicate<Long>() {
                    @Override
                    public boolean test(Long aLong) throws Exception {
                        // 不包含临界条件,条件不满足则结束
                        return aLong <= Constant.VALIDETE_CODE_WATING_TIME_MAX;
                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Long>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Long>() {

                    @Override
                    public void onNext(Long aLong) {
                        LogUtil.i("onNext = " + aLong);
                        long diff = Constant.VALIDETE_CODE_WATING_TIME_MAX - aLong;
                        validateCodeListener.countValidateCodeButton(diff);
                        //倒计时完毕后，按钮可以再次被点击，重新设置按钮文案
                        if (diff == 0) {
                            validateCodeListener.resendValidateCodeButton();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        validateCodeListener.resendValidateCodeButton();
                    }
                });
    }


}
