package com.wxh.worker.ui;

import android.support.annotation.NonNull;

import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.common.UIApplication;
import com.common.config.Constant;
import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.wxh.worker.json.CheckUpdate;
import com.common.retrofit.json.Data;

import java.util.HashMap;

import javax.inject.Inject;

import timber.log.Timber;

public class MainPresent extends RxMvpPresenter<MainView> {

    MyApi mMyApi;

    @Inject
    public MainPresent(MyApi myApi) {
        mMyApi = myApi;
        Timber.e("===========================================================");
        Timber.e("okhttpHelper = " + UIApplication.getAppComponent().okhttpHelper());
        Timber.e("cacheDir = " + UIApplication.getAppComponent().getCacheDir());
        Timber.e("glideCacheDir = " + UIApplication.getAppComponent().getGlideCacheDir());
        Timber.e("okhttpCacheDir = " + UIApplication.getAppComponent()
                .getOkhttpCacheDir());
    }

    public void requestVersion() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("classcode", Constant.ANDROIDVERSION);

        mMyApi.checkUpdate(hashMap)
                .compose(RxUtils.<Data<CheckUpdate>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<CheckUpdate>>() {

                    @Override
                    public void onNext(@NonNull Data<CheckUpdate> checkUpdateData) {
                        if (checkJsonCode(checkUpdateData, false)) {
                            getView().OnVersionUpdate(checkUpdateData.getResult());
                        } else {
                            getView().OnVersionUpdateError();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().OnVersionUpdateError();
                    }

                });
    }

}
