package com.wxh.worker.ui.money;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.common.mvp.BaseMvpActivity;
import com.wxh.worker.R;
import com.wxh.worker.json.CashWithDrawalBean;

import java.util.List;

public class PersonalBondActivity extends BaseMvpActivity<MoneyView, MoneyPresent> implements MoneyView {

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, PersonalBondActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_personal_bond;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {

    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }

    @Override
    public void onSuccess(List<CashWithDrawalBean> cashWithDrawalBeanList) {

    }

    @Override
    public void onWithdrawalSuccess() {

    }

    @Override
    public void onFail() {

    }
}
