package com.wxh.worker.ui.measure;

import android.support.annotation.NonNull;

import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.facebook.stetho.common.LogUtil;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.json.MeasureListBean;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class MeasurePresent extends RxMvpPresenter<MeasureView> {
    MyApi mMyApi;

    @Inject
    public MeasurePresent(MyApi mMyApi) {
        this.mMyApi = mMyApi;
    }

    public void getQueryMeasureRecord(String orderId) {
        mMyApi.getQueryMeasureRecord(orderId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<MeasureListBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<MeasureListBean>> data) {
                        if (checkJsonCode(data, false)) {
                            getView().onMeasureList(data.getResult());
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

    public void getQuerySingleMeasureRecord(String id) {
        mMyApi.getQuerySingleMeasureRecord(id)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false)) {
//                            getView().onMeasureList(data.getResult());
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

    public void delMeasureRecord(String id) {
        mMyApi.delMeasureRecord(id)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false)) {
                            getView().onMeasureList(null);
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

    public void deleteImg(String fileName) {
        mMyApi.deleteImg(fileName)
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        LogUtil.i(data.toString());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }
                });
    }


    public void uploadMeasureRecord(String dataString) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), dataString);
        mMyApi.uploadMeasureRecord(requestBody)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false)) {
                            getView().onMeasureList(null);
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

}
