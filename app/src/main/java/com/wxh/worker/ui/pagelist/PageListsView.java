package com.wxh.worker.ui.pagelist;

import com.utilCode.base.mvp.MvpView;

public interface PageListsView extends MvpView {

    void onLoadDoing();

    void onLoadDone();

    void onLoadComplete();

    void onLoadError();
}
