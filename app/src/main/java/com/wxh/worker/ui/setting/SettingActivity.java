package com.wxh.worker.ui.setting;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.apputil.AppStoreDownload;
import com.common.retrofit.RxObserver;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.utilCode.utils.RxUtils;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.BuildConfig;
import com.wxh.worker.event.LoginInvalideEvent;
import com.common.mvp.BaseMvpActivity;
import com.wxh.worker.R;
import com.wxh.worker.dialog.ConfirmDialog;
import com.wxh.worker.ui.setting.about.AboutActivity;
import com.wxh.worker.ui.setting.opinion.FeedBackActivity;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

public class SettingActivity extends BaseMvpActivity<SettingView, SettingPresent> implements SettingView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.rl_accoutn_security)
    RelativeLayout rlAccoutnSecurity;
    @BindView(R.id.rl_feed_back)
    RelativeLayout rlFeedBack;
    @BindView(R.id.rl_about)
    RelativeLayout rlAbout;
    @BindView(R.id.rl_update)
    RelativeLayout rlUpdate;
    @BindView(R.id.tv_button_loginout)
    TextView tvButtonLoginout;
    @BindView(R.id.tv_version)
    TextView tvVersion;

    private ConfirmDialog mConfirmDialog;
    private AppStoreDownload storeDownload;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, SettingActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarBack.setOnClickListener(this);
        rlAccoutnSecurity.setOnClickListener(this);
        rlFeedBack.setOnClickListener(this);
        rlAbout.setOnClickListener(this);
        rlUpdate.setOnClickListener(this);
        tvButtonLoginout.setOnClickListener(this);
        toolbarTitle.setText("设置");
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        storeDownload = new AppStoreDownload(SettingActivity.this);
        tvVersion.setText("v" + BuildConfig.VERSION_NAME);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.rl_accoutn_security:
                break;
            case R.id.rl_feed_back:
                FeedBackActivity.startActivity(this);
                break;
            case R.id.rl_about:
                AboutActivity.startActivity(this);
                break;
            case R.id.rl_update:
                getPresenter().getVersion();
                break;
            case R.id.tv_button_loginout:
                ConfirmDialog.Builder builder = new ConfirmDialog.Builder(this);
                mConfirmDialog = builder.setContentText("确认退出吗？").setConfirmListener(new ConfirmDialog.Builder.ConfirmClickListener() {
                            @Override
                            public void click(boolean b) {
                                mConfirmDialog.dismiss();
                                if (b) {
                                    EventBus.getDefault().post(new LoginInvalideEvent());
                                }
                            }
                        }).create();
                mConfirmDialog.show();
                break;
            default:
                break;
        }
    }

    @Override
    public void onVersionSuccess(int version) {
        String versionName = BuildConfig.VERSION_NAME.replace(".", "");
        if (version <= Integer.parseInt(versionName)) {
            ToastUtils.showLongToastSafe("当前版本已是最新版本");
        } else {
            if (!storeDownload.marketLogic()) {
                new RxPermissions(SettingActivity.this).request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .compose(RxUtils.<Boolean>applySchedulersLifeCycle(getMvpView()))
                        .subscribe(new RxObserver<Boolean>() {
                            @Override
                            public void onNext(Boolean aBoolean) {
                                if (aBoolean) {
                                } else {
                                    ToastUtils.showShortToast("检查到新版本，但您的权限被拒绝，无法使用存储空间，无法下载最新版本。");
                                }
                            }
                        });
            }
        }
    }

    @Override
    public void onFail() {

    }
}
