package com.wxh.worker.ui.me;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.config.Global;
import com.wxh.worker.event.MasterEvent;
import com.common.mvp.BaseMvpActivity;
import com.common.widght.EditTextWithDel;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.json.MasterInfoBean;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

public class PersonalDataActivity extends BaseMvpActivity<PersonalView, PersonalPresent> implements PersonalView, View.OnClickListener {

    @BindView(R.id.ll_personal)
    LinearLayout llPersonal;
    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.tv_service_area)
    TextView tvServiceArea;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.et_urgent)
    EditTextWithDel etUrgent;
    @BindView(R.id.et_phone)
    EditTextWithDel etPhone;
    @BindView(R.id.et_relationship)
    EditTextWithDel etRelationship;
    @BindView(R.id.tv_edit_contacts)
    TextView tvEditContacts;

    LoadingDialog mLoadingDialog;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, PersonalDataActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return R.layout.activity_personal_data;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarBack.setOnClickListener(this);
        tvEditContacts.setOnClickListener(this);
        toolbarTitle.setText("我的资料");
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        setContactsData();

        etUrgent.setEnabled(false);
        etUrgent.setDrawable(false);
        etPhone.setEnabled(false);
        etPhone.setDrawable(false);
        etRelationship.setEnabled(false);
        etRelationship.setDrawable(false);
    }

    public void setContactsData() {
        MasterInfoBean masterInfoBean = Global.getMasterInfoBean();
        if (masterInfoBean != null) {
            if (masterInfoBean.serviceAddressDTOList != null) {
                StringBuffer stringBuffer = new StringBuffer();
                for (MasterInfoBean.ServiceAddressDTOListBean serviceAddressDTOList : masterInfoBean.serviceAddressDTOList) {
                    stringBuffer.append(serviceAddressDTOList.serviceRegion + " ");
                    tvServiceArea.setText(stringBuffer);
                }
            }
            tvAddress.setText(masterInfoBean.masterAddress);
            etUrgent.setText(masterInfoBean.emergencyContactsName);
            etPhone.setText(masterInfoBean.emergencyContactsPhone);
            etRelationship.setText(masterInfoBean.emergencyContactsRelation);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.tv_edit_contacts:
                if (etUrgent.isEnabled()) {
                    String name = etUrgent.getText().toString();
                    String phone = etPhone.getText().toString();
                    String relation = etRelationship.getText().toString();
                    if (!mLoadingDialog.isVisible()) {
                        mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                    }
                    getPresenter().postEditUrgent(name, phone, relation);
                } else {
                    etUrgent.setEnabled(true);//可编辑
                    etUrgent.setDrawable(true);
                    etUrgent.setSelection(etUrgent.getText().length());//光标停留在最后位置
                    etPhone.setEnabled(true);//可编辑
                    etPhone.setDrawable(true);
                    etRelationship.setEnabled(true);//可编辑
                    etRelationship.setDrawable(true);
                    tvEditContacts.setText("保存");
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onSuccess() {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showLongToastSafe("修改成功");
        setContactsData();
        EventBus.getDefault().post(new MasterEvent());
        llPersonal.postDelayed(new Runnable() {
            @Override
            public void run() {
                etUrgent.setEnabled(false);
                etUrgent.setDrawable(false);
                etPhone.setEnabled(false);
                etPhone.setDrawable(false);
                etRelationship.setEnabled(false);
                etRelationship.setDrawable(false);
                tvEditContacts.setText("编辑联系人");
            }
        }, 100);
    }

    @Override
    public void onFail() {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showLongToastSafe("修改失败");
        llPersonal.postDelayed(new Runnable() {
            @Override
            public void run() {
                etUrgent.setEnabled(false);
                etUrgent.setDrawable(false);
                etPhone.setEnabled(false);
                etPhone.setDrawable(false);
                etRelationship.setEnabled(false);
                etRelationship.setDrawable(false);
                tvEditContacts.setText("编辑联系人");
            }
        }, 100);
    }
}
