package com.wxh.worker.ui.me;

import com.utilCode.base.mvp.MvpView;

public interface PersonalView extends MvpView {

    void onSuccess();

    void onFail();

}
