package com.wxh.worker.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.mvp.BaseMvpActivity;
import com.common.utils.FormatUtil;
import com.common.widght.EditTextWithDel;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;

import butterknife.BindView;

public class FindPasswordActivity extends BaseMvpActivity<LRView, LRPresent> implements LRView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.et_find_phonenumber)
    EditTextWithDel etFindPhonenumber;
    @BindView(R.id.et_find_v_code)
    EditTextWithDel etFindCode;
    @BindView(R.id.et_find_pwd)
    EditTextWithDel etFindPwd;
    @BindView(R.id.et_find_pwd_confirm)
    EditTextWithDel etFindPwdConfirm;
    @BindView(R.id.tv_send_v_code)
    TextView tvSendCode;
    @BindView(R.id.et_find_commit)
    TextView etFindCommit;

    private String phone = "";
    private boolean inputPwd = false;
    LoadingDialog mLoadingDialog;

    public static void startActivity(Activity activity, String phone) {
        Intent intent = new Intent(activity, FindPasswordActivity.class);
        intent.putExtra("phone", phone);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_find_password;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarTitle.setText("找回密码");
        toolbarBack.setOnClickListener(this);
        tvSendCode.setOnClickListener(this);
        etFindCommit.setOnClickListener(this);

        etFindPhonenumber.setFilters(new InputFilter[]{filter, new InputFilter.LengthFilter(13)});
        etFindPhonenumber.addTextChangedListener(new TextWatcherListener(1));

        etFindPwd.addTextChangedListener(new TextWatcherListener(2));
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        phone = getIntent().getStringExtra("phone");
        etFindPhonenumber.setText(TextUtils.isEmpty(phone) ? "" : phone);

        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }
    }

    //禁止输入框输入空格和换行符号2
    private InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (source.equals(" ") || source.toString().contentEquals("\n")) {
                return "";
            } else {
                return null;
            }
        }
    };

    class TextWatcherListener implements TextWatcher {

        int type;

        public TextWatcherListener(int type) {
            this.type = type;
        }


        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            if (type == 1) {
                FormatUtil.addEditSpace(charSequence, start, before, etFindPhonenumber);
            } else if (type == 2) {
                checkPasswordEditTextLogin(charSequence.toString());
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }

    @Override
    public void onSMS_Success() {

    }

    @Override
    public void onRegister() {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showShortToastSafe("密码修改成功");
        finish();
    }

    @Override
    public void onLoginSuccess(String status) {

    }

    @Override
    public void onFail(int type, String message) {
        mLoadingDialog.dismissAllowingStateLoss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.tv_send_v_code:
                phone = etFindPhonenumber.getText().toString().replace(" ", "");
                if (!checkPhoneEditTextLogin(phone)) {
                    ToastUtils.showShortToastSafe("请输入正确的手机号");
                    return;
                }
//                getPresenter().sendSMS(phone, validateCodeListener);
                break;
            case R.id.et_find_commit:
                phone = etFindPhonenumber.getText().toString().replace(" ", "");
                String vCode = etFindCode.getText().toString();
                String password = etFindPwd.getText().toString();
                String passwordConfirm = etFindPwdConfirm.getText().toString();
                if (!checkPhoneEditTextLogin(phone)) {
                    ToastUtils.showShortToastSafe("请输入正确的手机号");
                    return;
                } else if (TextUtils.isEmpty(vCode) || vCode.length() != 4) {
                    ToastUtils.showShortToastSafe("请输入正确的验证码");
                    return;
                } else if (!inputPwd && TextUtils.isEmpty(password)) {
                    ToastUtils.showShortToastSafe("请输入6~18位数密码");
                    return;
                } else if (TextUtils.isEmpty(passwordConfirm) || !TextUtils.equals(password, passwordConfirm)) {
                    ToastUtils.showShortToastSafe("两次密码输入不一致，请重新输入");
                    return;
                }
                if (!mLoadingDialog.isVisible()) {
                    mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                }
                getPresenter().getRetrievePwd(phone, vCode, password, passwordConfirm);
                break;
            default:
                break;
        }
    }

    private LRView.ValidateCodeListener validateCodeListener = new LRView.ValidateCodeListener() {

        @Override
        public void disableValidateCodeButton() {
            tvSendCode.setEnabled(false);
        }

        @Override
        public void countValidateCodeButton(long number) {
            tvSendCode.setText("已发送(" + number + ")");
        }

        @Override
        public void resendValidateCodeButton() {
            tvSendCode.setEnabled(true);
            tvSendCode.setText("重发验证码");
        }

        @Override
        public void enableValidateCodeButton() {
            tvSendCode.setEnabled(true);
        }
    };

    public void checkPasswordEditTextLogin(String content) {
        inputPwd = content.length() > 5;
    }

    public boolean checkPhoneEditTextLogin(String phone) {
        return !TextUtils.isEmpty(phone) && phone.replace(" ", "").length() == 11;
    }
}
