package com.wxh.worker.ui.setting.contact;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.mvp.BaseMvpActivity;
import com.common.retrofit.RxObserver;
import com.common.utils.Utils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.R;
import com.wxh.worker.dialog.ConfirmDialog;
import com.wxh.worker.ui.HomeActivity;
import com.wxh.worker.ui.setting.about.AboutActivity;

import butterknife.BindView;

public class ContactActivity extends BaseMvpActivity<ContactView, ContactPresenter> implements ContactView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarNack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.rl_contact_phone)
    LinearLayout rlContactPhone;

    private ConfirmDialog mConfirmDialog;
    private String servicePhone = "0571-68767990";

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, ContactActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_contact;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        rlContactPhone.setOnClickListener(this);
        toolbarNack.setOnClickListener(this);
        toolbarTitle.setText("联系我们");
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.rl_contact_phone:
                ConfirmDialog.Builder builder = new ConfirmDialog.Builder(ContactActivity.this);
                mConfirmDialog = builder.setContentText("联系电话：" + servicePhone).setConfirmListener(new ConfirmDialog.Builder.ConfirmClickListener() {
                    @Override
                    public void click(boolean b) {
                        mConfirmDialog.dismiss();
                        if (b) {
                            try {
                                new RxPermissions(ContactActivity.this).request(Manifest.permission.CALL_PHONE)
                                        .compose(RxUtils.applySchedulersLifeCycle(getMvpView()))
                                        .subscribe(new RxObserver<Boolean>() {
                                            @Override
                                            public void onComplete() {
                                                Utils.callPhone(ContactActivity.this, servicePhone.replace("-", ""));
                                            }
                                        });
                            } catch (Exception excp) {
                                excp.printStackTrace();
                            }
                        }
                    }
                }).create();
                mConfirmDialog.show();
                break;
        }
    }
}
