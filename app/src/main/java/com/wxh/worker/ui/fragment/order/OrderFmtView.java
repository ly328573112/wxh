package com.wxh.worker.ui.fragment.order;

import com.utilCode.base.mvp.MvpView;
import com.wxh.worker.json.OrderListBean;

import java.util.List;

public interface OrderFmtView extends MvpView {

    void onOrderListData(List<OrderListBean> orderListBeans, int pageNumber);

    void onFail();

}
