package com.wxh.worker.ui.order;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.mvp.BaseMvpActivity;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;
import com.wxh.worker.event.CheckAcceptanceEvent;
import com.wxh.worker.json.OrderListBean;
import com.wxh.worker.ui.fragment.order.OrderAdapter;
import com.wxh.worker.ui.fragment.order.OrderFmtPresent;
import com.wxh.worker.ui.fragment.order.OrderFmtView;
import com.wxh.worker.ui.setting.about.AboutActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;

public class OrderListActivity extends BaseMvpActivity<OrderFmtView, OrderFmtPresent> implements OrderFmtView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.srl)
    SwipeRefreshLayout mSrl;
    @BindView(R.id.rv_order_list)
    RecyclerView rvOrderList;

    private String orderStatus = "3";
    private int pageNum = 1;
    private int pageSize = 20;
    private OrderAdapter orderAdapter;

    private LoadingDialog mLoadingDialog;

    public static void startActivity(Activity activity, String orderStatus) {
        Intent intent = new Intent(activity, OrderListActivity.class);
        intent.putExtra("orderStatus", orderStatus);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_order_list;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        EventBus.getDefault().register(this);
        toolbarBack.setOnClickListener(this);

        mSrl.setColorSchemeColors(ContextCompat.getColor(this, R.color.c_E60000));
        mSrl.setProgressViewOffset(true, -20, 100);
        mSrl.setOnRefreshListener(refreshListener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvOrderList.setLayoutManager(linearLayoutManager);
        orderAdapter = new OrderAdapter(this, orderClickListener);
        rvOrderList.setAdapter(orderAdapter);
        orderAdapter.setOnLoadMoreListener(onLoadMoreListener, rvOrderList);

        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        orderStatus = intent.getStringExtra("orderStatus");
        if (TextUtils.equals(orderStatus, "5")) {
            toolbarTitle.setText("待服务订单");
        } else if (TextUtils.equals(orderStatus, "3")) {
            toolbarTitle.setText("待接受订单");
        } else if (TextUtils.equals(orderStatus, "4")) {
            toolbarTitle.setText("待预约订单");
        }

        if (!mLoadingDialog.isVisible()) {
            mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
        }
        pageNum = 1;
        getPresenter().getListOrders(orderStatus, pageNum, pageSize);
    }

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            pageNum = 1;
            getPresenter().getListOrders(orderStatus, pageNum, pageSize);
        }
    };

    private OrderAdapter.OrderClickListener orderClickListener = new OrderAdapter.OrderClickListener() {
        @Override
        public void onOrderItemClick(OrderListBean item) {
            OrderDetailActivity.startActivity(OrderListActivity.this, item.id, orderStatus);
        }
    };

    private BaseRecycleViewAdapter.OnLoadMoreListener onLoadMoreListener = new BaseRecycleViewAdapter.OnLoadMoreListener() {

        @Override
        public void onLoadMore() {
            getPresenter().getListOrders(orderStatus, pageNum, pageSize);
        }
    };

    @Override
    public void onOrderListData(List<OrderListBean> orderListBeans, int pageNumber) {
        if (mSrl.isRefreshing()) {
            mSrl.setRefreshing(false);
        }
        mLoadingDialog.dismissAllowingStateLoss();
        if (pageNum == 1 && orderListBeans == null || orderListBeans.size() == 0) {
            orderAdapter.resetData(orderListBeans);
            orderAdapter.setEmptyView(R.layout.empty_view);
            orderAdapter.loadMoreEnd(false);
            orderAdapter.loadMoreComplete();
        } else if (orderListBeans != null && orderListBeans.size() < pageSize) {
            if (pageNum == 1) {
                orderAdapter.resetData(orderListBeans);
            } else {
                orderAdapter.addData(orderListBeans);
            }
            orderAdapter.loadMoreEnd(false);
            pageNum = pageNumber;
            orderAdapter.loadMoreComplete();
        } else {
            if (pageNum == 1) {
                orderAdapter.resetData(orderListBeans);
            } else {
                orderAdapter.addData(orderListBeans);
            }
            pageNum = pageNumber + 1;
        }
    }

    @Override
    public void onFail() {
        if (mSrl.isRefreshing()) {
            mSrl.setRefreshing(false);
        }
        mLoadingDialog.dismissAllowingStateLoss();
    }

    @Override
    public void onClick(View view) {
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CheckAcceptanceEvent event) {
        pageNum = 1;
        getPresenter().getListOrders(orderStatus, pageNum, pageSize);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
