package com.wxh.worker.ui.checkup;

import com.utilCode.base.mvp.MvpView;

public interface CheckView extends MvpView {

    void onAcceptanceSuccess();

    void onFail();

}
