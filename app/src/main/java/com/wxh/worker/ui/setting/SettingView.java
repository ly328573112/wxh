package com.wxh.worker.ui.setting;

import com.utilCode.base.mvp.MvpView;

public interface SettingView extends MvpView {

    void onVersionSuccess(int version);

    void onFail();

}
