package com.wxh.worker.ui.checkup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.common.config.Global;
import com.wxh.worker.event.CheckAcceptanceEvent;
import com.wxh.worker.event.OriginalDeleteEvent;
import com.common.logupload.BitmapThreedTask;
import com.common.logupload.UploadLogUtils;
import com.common.logupload.UploadRequestBean;
import com.common.mvp.BaseMvpActivity;
import com.common.utils.LogUtil;
import com.common.widght.CustomTextWatcher;
import com.common.widght.EditTextWithDel;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.common_view.CameraSelectView;
import com.wxh.worker.common_view.CameraUtil;
import com.wxh.worker.common_view.ImageClickListener;
import com.wxh.worker.common_view.PhotoImageAdapter;
import com.wxh.worker.common_view.bean.PhotoInfo;
import com.wxh.worker.common_view.imgoriginal.OriginalPagerActivity;
import com.wxh.worker.ui.login.LRView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class CheckAcceptanceActivity extends BaseMvpActivity<CheckView, CheckPresent> implements CheckView, View.OnClickListener, ImageClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.et_measure_remarks)
    EditText etMeasureRemarks;
    @BindView(R.id.rv_img_grid)
    RecyclerView rvImgGrid;
    @BindView(R.id.et_acceptance_v_code)
    EditTextWithDel etAcceptanceVCode;
    @BindView(R.id.tv_send_v_code)
    Button tvSendVCode;
    @BindView(R.id.tv_button_red_c)
    TextView tvButtonRedC;

    private CameraUtil cameraUtil;
    private CameraSelectView selectView;
    private PhotoImageAdapter imageAdapter;
    private List<PhotoInfo> photoList = new ArrayList<>();// 照片uri列表
    private int currentItemId = 1;// 加号
    private int COLUMN = 20;//图片可选数量
    private int imageColumn = 1;//图片集合是否带有加号

    private LoadingDialog mLoadingDialog;
    private UploadLogUtils uploadLogUtils;

    private String orderId;

    public static void startActivity(Activity activity, String orderId) {
        Intent intent = new Intent(activity, CheckAcceptanceActivity.class);
        intent.putExtra("orderId", orderId);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return R.layout.activity_check_acceptance;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarBack.setOnClickListener(this);
        tvSendVCode.setOnClickListener(this);
        tvButtonRedC.setOnClickListener(this);
        tvButtonRedC.setVisibility(View.VISIBLE);
        toolbarTitle.setText("申请验收");

        etMeasureRemarks.addTextChangedListener(new CustomTextWatcher(this, etMeasureRemarks, 200));

        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }

        uploadLogUtils = new UploadLogUtils();
        initImgGridView();
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        Intent intent = getIntent();
        orderId = intent.getStringExtra("orderId");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.tv_send_v_code:
                if (TextUtils.isEmpty(orderId)) {
                    return;
                }
                getPresenter().sendAcceptanceSms(orderId, validateCodeListener);
                break;
            case R.id.tv_button_red_c:
                setData();
                break;
        }
    }

    private LRView.ValidateCodeListener validateCodeListener = new LRView.ValidateCodeListener() {

        @Override
        public void disableValidateCodeButton() {
            tvSendVCode.setEnabled(false);
        }

        @Override
        public void countValidateCodeButton(long number) {
            tvSendVCode.setText("已发送(" + number + ")");
        }

        @Override
        public void resendValidateCodeButton() {
            tvSendVCode.setEnabled(true);
            tvSendVCode.setText("重发验证码");
        }

        @Override
        public void enableValidateCodeButton() {
            tvSendVCode.setEnabled(true);
        }
    };

    private void setData() {
        List<Object> arrayList = new ArrayList<>();
        String smsCode = etAcceptanceVCode.getText().toString().trim();
        String mark = etMeasureRemarks.getText().toString().trim();
        int photoSize = photoList.size();
        for (int i = 0; i < photoList.size(); i++) {
            if (photoList.get(i).photoNumber == currentItemId) {
                photoSize = photoList.size() - 1;
                photoList.remove(i);
            }
        }
        if (photoSize == 0) {
            ToastUtils.showLongToastSafe("请上传安装效果图");
            return;
        }
//        else if (TextUtils.isEmpty(smsCode) || smsCode.length() != 4) {
//            ToastUtils.showLongToastSafe("请输入正确的验证码");
//            return;
//        }
        JSONObject urlObject = new JSONObject();
        for (int i = 0; i < photoList.size(); i++) {
            urlObject.put("url", photoList.get(i).networkUrL);
            arrayList.add(urlObject);
        }
        if (!mLoadingDialog.isVisible()) {
            mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
        }
        getPresenter().postApplyAcceptance(orderId, smsCode, mark, arrayList);
    }

    /********************************************图片选择上传部分**********************************************/
    private void initImgGridView() {
        selectView = new CameraSelectView(this);
        selectView.setPictureCut(false);
        cameraUtil = new CameraUtil(this);

        photoList.clear();
        PhotoInfo info = new PhotoInfo(currentItemId, null, false);
        photoList.add(info);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false);
        rvImgGrid.setLayoutManager(layoutManager);
        rvImgGrid.setHasFixedSize(true);
        imageAdapter = new PhotoImageAdapter(this);
        rvImgGrid.setAdapter(imageAdapter);
        rvImgGrid.setItemAnimator(new DefaultItemAnimator());
        imageAdapter.setImageClickListener(this);
        imageAdapter.addData(photoList);
    }

    private CameraSelectView.CameraPotoListener potoListener = new CameraSelectView.CameraPotoListener() {

        @Override
        public void onResultPotoList(List<PhotoInfo> imgList) {
            if (!mLoadingDialog.isVisible()) {
                mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
            }
            new BitmapThreedTask().new LoadlocalBitmapThread(imgList, handler).start();
        }
    };

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            // 可能有反复调用相册的情况，所以需要多重迭代增加
            @SuppressWarnings("unchecked")
            ArrayList<File> callBacklist = (ArrayList<File>) msg.obj;
            for (int i = 0; i < callBacklist.size(); i++) {
                LogUtil.i("压缩后图片大小：" + BitmapThreedTask.formatFileSize(callBacklist.get(i)));
                PhotoInfo photoInfo = new PhotoInfo(0, callBacklist.get(i).toString(), false);

                File file = new File(photoInfo.photoUri);
                if (!file.exists()) {
                    return;
                }
                RequestBody type = RequestBody.create(MediaType.parse("form-data"), "1");
                uploadLogUtils.uploadImg(type, file);
                uploadLogUtils.setResponseBodyListener(new UploadImage(photoInfo.photoUri));
            }
        }
    };

    @Override
    public void openCamera(int position) {
        int photoNumber = photoList.get(position).photoNumber;
        if (photoNumber == currentItemId) {
            int optionalNumber = COLUMN - (photoList.size() - imageColumn);
            selectView.showCameraDialog(cameraUtil, optionalNumber, View.VISIBLE);
            selectView.setCameraPotoListener(potoListener);
        } else if (photoNumber == 0) {
            ArrayList<String> arrayList = new ArrayList<>();
            for (int i = 0; i < photoList.size() - imageColumn; i++) {
                arrayList.add(photoList.get(i).photoUri);
            }
            OriginalPagerActivity.start(CheckAcceptanceActivity.this, arrayList, position);
        }
    }

    @Override
    public void onDeleteImage(int position) {
        String networkUrL = photoList.get(position).networkUrL.replaceAll("https://oss.aguogo.cn/", "");
        if (!TextUtils.isEmpty(networkUrL)) {
            getPresenter().deleteImg(networkUrL);
        }
        photoList.remove(position);
        confineListSize();
        imageAdapter.resetData(photoList);
    }

    @Override
    public void onAcceptanceSuccess() {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showLongToast("申请验收已提交!");
        EventBus.getDefault().post(new CheckAcceptanceEvent());
        finish();
    }

    @Override
    public void onFail() {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showLongToast("申请验收提交失败!");
    }

    class UploadImage implements UploadLogUtils.ResponseBodyListener {

        String photoUri;

        UploadImage(String photoUri) {
            this.photoUri = photoUri;
        }


        @Override
        public void requestBody(UploadRequestBean.ResultBean result) {
            mLoadingDialog.dismissAllowingStateLoss();
            PhotoInfo photoInfo = null;
            String url = result.name;
            if (url.startsWith("https")) {
                photoInfo = new PhotoInfo(0, url.replace("https", "http"), url, true);
            } else {
                photoInfo = new PhotoInfo(0, url, url, true);
            }
            photoList.add(photoInfo);
            confineListSize();
            imageAdapter.resetData(photoList);
        }

        @Override
        public void requestError() {
            mLoadingDialog.dismissAllowingStateLoss();
            ToastUtils.showLongToast("图片上传失败!");
        }
    }

    /**
     * 加号始终放在List最后
     */
    private void confineListSize() {
        for (PhotoInfo photoInfo : photoList) {
            if (photoInfo.photoNumber == currentItemId) {
                photoList.remove(photoInfo);
                break;
            }
        }
        if (photoList.size() < COLUMN) {
            PhotoInfo info = new PhotoInfo(currentItemId, null, false);
            photoList.add(info);
            imageColumn = 1;
        } else {
            imageColumn = 0;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        cameraUtil.onHandleActivityResult(requestCode, resultCode, data);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(OriginalDeleteEvent event) {
        onDeleteImage(event.curPosition);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}
