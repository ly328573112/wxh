package com.wxh.worker.ui.measure.module_view;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.alibaba.fastjson.JSONObject;
import com.common.logupload.BitmapThreedTask;
import com.common.logupload.UploadLogUtils;
import com.common.logupload.UploadRequestBean;
import com.common.utils.LogUtil;
import com.common.widght.CustomTextWatcher;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.common_view.CameraSelectView;
import com.wxh.worker.common_view.CameraUtil;
import com.wxh.worker.common_view.ImageClickListener;
import com.wxh.worker.common_view.PhotoImageAdapter;
import com.wxh.worker.common_view.bean.PhotoInfo;
import com.wxh.worker.common_view.imgoriginal.OriginalPagerActivity;
import com.wxh.worker.json.MeasureListBean;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class RemarksImageView<T extends RxAppCompatActivity> implements ImageClickListener {

    private RxAppCompatActivity activity;
    private EditText etMeasureRemarks;
    private RecyclerView rvImgGrid;

    private CameraUtil cameraUtil;
    private CameraSelectView selectView;
    private PhotoImageAdapter imageAdapter;
    private List<PhotoInfo> photoList = new ArrayList<>();// 照片uri列表
    private int currentItemId = 1;// 加号
    private int COLUMN = 20;//图片可选数量
    private int imageColumn = 1;//图片集合是否带有加号

    private LoadingDialog mLoadingDialog;
    private UploadLogUtils uploadLogUtils;
    private RemarksListener remarksListener;

    public RemarksImageView(T activity, RemarksListener remarksListener) {
        this.activity = activity;
        this.remarksListener = remarksListener;
        initObject();
    }

    private void initObject() {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }
        uploadLogUtils = new UploadLogUtils();
    }

    public void setMeasureListBean(MeasureListBean measureListBean) {
        etMeasureRemarks.setText(measureListBean.remarks);
        for (int i = 0; i < measureListBean.arrayList.size(); i++) {
            JSONObject jsonObject = JSONObject.parseObject(measureListBean.arrayList.get(i).toString());
            String url = jsonObject.getString("url");
            PhotoInfo photoInfo = null;
            if (url.startsWith("https")) {
                photoInfo = new PhotoInfo(0, url.replace("https", "http"), url, true);
            } else {
                photoInfo = new PhotoInfo(0, url, url, true);
            }
            photoList.add(photoInfo);
        }
        confineListSize();
        imageAdapter.resetData(photoList);
    }


    public View getView() {
        View view = View.inflate(activity, R.layout.layout_remarks_img_view, null);
        etMeasureRemarks = view.findViewById(R.id.et_measure_remarks);
        rvImgGrid = view.findViewById(R.id.rv_img_grid);

        etMeasureRemarks.addTextChangedListener(new CustomTextWatcher(activity, etMeasureRemarks,200));
        etMeasureRemarks.addTextChangedListener(textWatcher);
        initImgGridView();
        return view;
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            remarksListener.onRemarksData(charSequence.toString());
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void initImgGridView() {
        selectView = new CameraSelectView(activity);
        selectView.setPictureCut(false);
        cameraUtil = new CameraUtil(activity);

        photoList.clear();
        PhotoInfo info = new PhotoInfo(currentItemId, null, false);
        photoList.add(info);

        GridLayoutManager layoutManager = new GridLayoutManager(activity, 4, LinearLayoutManager.VERTICAL, false);
        rvImgGrid.setLayoutManager(layoutManager);
        rvImgGrid.setHasFixedSize(true);
        imageAdapter = new PhotoImageAdapter(activity);
        rvImgGrid.setAdapter(imageAdapter);
        rvImgGrid.setItemAnimator(new DefaultItemAnimator());
        imageAdapter.setImageClickListener(this);
        imageAdapter.addData(photoList);
    }

    public String getRemarks() {
        return etMeasureRemarks.getText().toString();
    }

    private CameraSelectView.CameraPotoListener potoListener =
            new CameraSelectView.CameraPotoListener() {

                @Override
                public void onResultPotoList(List<PhotoInfo> imgList) {
                    if (!mLoadingDialog.isVisible()) {
                        mLoadingDialog.show(activity.getSupportFragmentManager(), "loadingdialog");
                    }
                    new BitmapThreedTask().new LoadlocalBitmapThread(imgList, handler).start();
                }
            };

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            // 可能有反复调用相册的情况，所以需要多重迭代增加
            @SuppressWarnings("unchecked")
            ArrayList<File> callBacklist = (ArrayList<File>) msg.obj;
            for (int i = 0; i < callBacklist.size(); i++) {
                LogUtil.i("压缩后图片大小：" + BitmapThreedTask.formatFileSize(callBacklist.get(i)));
                PhotoInfo photoInfo = new PhotoInfo(0, callBacklist.get(i).toString(), false);

                File file = new File(photoInfo.photoUri);
                if (!file.exists()) {
                    return;
                }
                RequestBody type = RequestBody.create(MediaType.parse("form-data"), "1");
                uploadLogUtils.uploadImg(type, file);
                uploadLogUtils.setResponseBodyListener(bodyListener);
            }
        }
    };

    private UploadLogUtils.ResponseBodyListener bodyListener =
            new UploadLogUtils.ResponseBodyListener() {
                @Override
                public void requestBody(UploadRequestBean.ResultBean result) {
                    mLoadingDialog.dismissAllowingStateLoss();
                    PhotoInfo photoInfo = null;
                    String url = result.name;
                    remarksListener.onImageData(url);
                    if (url.startsWith("https")) {
                        photoInfo = new PhotoInfo(0, url.replace("https", "http"), url, true);
                    } else {
                        photoInfo = new PhotoInfo(0, url, url, true);
                    }
                    photoList.add(photoInfo);
                    confineListSize();
                    imageAdapter.resetData(photoList);
                }

                @Override
                public void requestError() {
                    mLoadingDialog.dismissAllowingStateLoss();
                    ToastUtils.showLongToast("图片上传失败!");
                }
            };

    @Override
    public void openCamera(int position) {
        int photoNumber = photoList.get(position).photoNumber;
        if (photoNumber == currentItemId) {
            int optionalNumber = COLUMN - (photoList.size() - imageColumn);
            selectView.showCameraDialog(cameraUtil, optionalNumber, View.VISIBLE);
            selectView.setCameraPotoListener(potoListener);
        } else if (photoNumber == 0) {
            ArrayList<String> arrayList = new ArrayList<>();
            for (int i = 0; i < photoList.size() - imageColumn; i++) {
                arrayList.add(photoList.get(i).photoUri);
            }
            OriginalPagerActivity.start(activity, arrayList, position);
        }
    }

    @Override
    public void onDeleteImage(int position) {
        if (photoList.size() > 0) {
            remarksListener.onDelete(photoList.get(position).networkUrL);
        }
        photoList.remove(position);
        confineListSize();
        imageAdapter.resetData(photoList);
    }

    /**
     * 加号始终放在List最后
     */
    private void confineListSize() {
        for (PhotoInfo photoInfo : photoList) {
            if (photoInfo.photoNumber == currentItemId) {
                photoList.remove(photoInfo);
                break;
            }
        }
        if (photoList.size() < COLUMN) {
            PhotoInfo info = new PhotoInfo(currentItemId, null, false);
            photoList.add(info);
            imageColumn = 1;
        } else {
            imageColumn = 0;
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        cameraUtil.onHandleActivityResult(requestCode, resultCode, data);
    }

    public interface RemarksListener {

        void onRemarksData(String remarks);

        void onImageData(String url);

        void onDelete(String networkUrL);
    }

}
