package com.wxh.worker.ui.measure.module_view;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.wxh.worker.R;
import com.wxh.worker.json.MeasureListBean;

public class OtherView<T extends Activity> {

    private Activity activity;
    private EditText etBoxWidth;
    private OtherListener otherListener;
    private TextView tvBoxWidthM;
    private RadioButton rbOpenShi;
    private RadioButton rbOpenFou;
    private RadioButton rbFullShi;
    private RadioButton rbFullFou;
    private RadioButton rbBoxYou;
    private RadioButton rbBoxWu;
    private RadioButton rbMuBan;
    private RadioButton rbHunNingTu;
    private RadioButton rbDaLiShi;
    private RadioButton rbShiGaoBan;
    private RadioButton rbZuoCe;
    private RadioButton rbYouCe;
    private RadioButton rbLiangCe;

    public OtherView(T activity, OtherListener otherListener) {
        this.activity = activity;
        this.otherListener = otherListener;
    }

    public View getView() {
        View view = View.inflate(activity, R.layout.layout_other_view, null);

        RadioGroup rgOpenFlag = view.findViewById(R.id.rg_open_flag);
        rgOpenFlag.setOnCheckedChangeListener(rgOpenFlagListener);
        rbOpenShi = view.findViewById(R.id.rb_open_shi);
        rbOpenFou = view.findViewById(R.id.rb_open_fou);

        RadioGroup rgFullFlag = view.findViewById(R.id.rg_full_flag);
        rgFullFlag.setOnCheckedChangeListener(rgFullFlagListener);
        rbFullShi = view.findViewById(R.id.rb_full_shi);
        rbFullFou = view.findViewById(R.id.rb_full_fou);

        RadioGroup rgBoxFlag = view.findViewById(R.id.rg_box_flag);
        rgBoxFlag.setOnCheckedChangeListener(rgBoxFlagListener);
        rbBoxYou = view.findViewById(R.id.rb_box_you);
        rbBoxWu = view.findViewById(R.id.rb_box_wu);
        etBoxWidth = view.findViewById(R.id.et_box_width);
        tvBoxWidthM = view.findViewById(R.id.tv_box_width_m);

        RadioGroup rgMaterialQuality = view.findViewById(R.id.rg_material_quality);
        rgMaterialQuality.setOnCheckedChangeListener(rgMaterialQualityListener);
        rbMuBan = view.findViewById(R.id.rb_mu_ban);
        rbHunNingTu = view.findViewById(R.id.rb_hun_ning_tu);
        rbDaLiShi = view.findViewById(R.id.rb_da_li_shi);
        rbShiGaoBan = view.findViewById(R.id.rb_shi_gao_ban);

        RadioGroup rgLinePosition = view.findViewById(R.id.rg_line_position);
        rgLinePosition.setOnCheckedChangeListener(rgLinePositionListener);
        rbZuoCe = view.findViewById(R.id.rb_zuo_ce);
        rbYouCe = view.findViewById(R.id.rb_you_ce);
        rbLiangCe = view.findViewById(R.id.rb_liang_ce);

        return view;
    }

    public void setMeasureListBean(MeasureListBean measure) {
        if (!TextUtils.isEmpty(measure.openFlag)) {
            if (TextUtils.equals(measure.openFlag, "1")) {
                rbOpenShi.setChecked(true);
                rbOpenFou.setChecked(false);
            } else if (TextUtils.equals(measure.openFlag, "2")) {
                rbOpenShi.setChecked(false);
                rbOpenFou.setChecked(true);
            }
        }
        if (!TextUtils.isEmpty(measure.fullFlag)) {
            if (TextUtils.equals(measure.fullFlag, "1")) {
                rbFullShi.setChecked(true);
                rbFullFou.setChecked(false);
            } else if (TextUtils.equals(measure.fullFlag, "2")) {
                rbFullShi.setChecked(false);
                rbFullFou.setChecked(true);
            }
        }
        if (!TextUtils.isEmpty(measure.boxFlag)) {
            if (TextUtils.equals(measure.boxFlag, "0")) {
                etBoxWidth.setVisibility(View.INVISIBLE);
                tvBoxWidthM.setVisibility(View.INVISIBLE);
                rbBoxWu.setChecked(true);
                rbBoxYou.setChecked(false);
            } else if (TextUtils.equals(measure.boxFlag, "1")) {
                etBoxWidth.setVisibility(View.VISIBLE);
                tvBoxWidthM.setVisibility(View.VISIBLE);
                etBoxWidth.setText(measure.boxWidth);
                rbBoxWu.setChecked(false);
                rbBoxYou.setChecked(true);
            }
        }
        if (!TextUtils.isEmpty(measure.materialQuality)) {
            if (TextUtils.equals(measure.materialQuality, "1")) {
                rbMuBan.setChecked(true);
                rbHunNingTu.setChecked(false);
                rbDaLiShi.setChecked(false);
                rbShiGaoBan.setChecked(false);
            } else if (TextUtils.equals(measure.materialQuality, "2")) {
                rbMuBan.setChecked(false);
                rbHunNingTu.setChecked(true);
                rbDaLiShi.setChecked(false);
                rbShiGaoBan.setChecked(false);
            } else if (TextUtils.equals(measure.materialQuality, "3")) {
                rbMuBan.setChecked(false);
                rbHunNingTu.setChecked(false);
                rbDaLiShi.setChecked(true);
                rbShiGaoBan.setChecked(false);
            } else if (TextUtils.equals(measure.materialQuality, "4")) {
                rbMuBan.setChecked(false);
                rbHunNingTu.setChecked(false);
                rbDaLiShi.setChecked(false);
                rbShiGaoBan.setChecked(true);
            }
        }
        if (!TextUtils.isEmpty(measure.linePosition)) {
            if (TextUtils.equals(measure.linePosition, "1")) {
                rbZuoCe.setChecked(true);
                rbYouCe.setChecked(false);
                rbLiangCe.setChecked(false);
            } else if (TextUtils.equals(measure.linePosition, "2")) {
                rbZuoCe.setChecked(false);
                rbYouCe.setChecked(true);
                rbLiangCe.setChecked(false);
            } else if (TextUtils.equals(measure.linePosition, "3")) {
                rbZuoCe.setChecked(false);
                rbYouCe.setChecked(false);
                rbLiangCe.setChecked(true);
            }
        }
    }

    public String getBoxWidth() {
        return etBoxWidth.getText().toString();
    }

    ;

    private RadioGroup.OnCheckedChangeListener rgOpenFlagListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.rb_open_shi:
                    otherListener.onOpenFlag("1");
                    break;
                case R.id.rb_open_fou:
                    otherListener.onOpenFlag("2");
                    break;
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener rgFullFlagListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.rb_full_shi:
                    otherListener.onFullFlag("1");
                    break;
                case R.id.rb_full_fou:
                    otherListener.onFullFlag("2");
                    break;
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener rgBoxFlagListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.rb_box_you:
                    etBoxWidth.setVisibility(View.VISIBLE);
                    tvBoxWidthM.setVisibility(View.VISIBLE);
                    otherListener.onBoxFlag("1", null);
                    break;
                case R.id.rb_box_wu:
                    etBoxWidth.setVisibility(View.INVISIBLE);
                    tvBoxWidthM.setVisibility(View.INVISIBLE);
                    otherListener.onBoxFlag("0", null);
                    break;
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener rgMaterialQualityListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.rb_mu_ban:
                    otherListener.onMaterialQuality("1");
                    break;
                case R.id.rb_hun_ning_tu:
                    otherListener.onMaterialQuality("2");
                    break;
                case R.id.rb_da_li_shi:
                    otherListener.onMaterialQuality("3");
                    break;
                case R.id.rb_shi_gao_ban:
                    otherListener.onMaterialQuality("4");
                    break;
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener rgLinePositionListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.rb_zuo_ce:
                    otherListener.onLinePosition("1");
                    break;
                case R.id.rb_you_ce:
                    otherListener.onLinePosition("2");
                    break;
                case R.id.rb_liang_ce:
                    otherListener.onLinePosition("3");
                    break;
            }
        }
    };


    public interface OtherListener {

        void onOpenFlag(String openFlag);

        void onFullFlag(String fullFlag);

        void onBoxFlag(String boxFlag, String boxWidth);

        void onMaterialQuality(String materialQuality);

        void onLinePosition(String linePosition);
    }

}
