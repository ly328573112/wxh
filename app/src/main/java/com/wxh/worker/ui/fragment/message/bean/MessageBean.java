package com.wxh.worker.ui.fragment.message.bean;

import android.support.annotation.Keep;

@Keep
public class MessageBean {


    /**
     * id : 1160393369955614720
     * newsType : 1
     * title : 订单消息
     * messages : 派给您一个新订单，请尽快查看并处理
     * orderId : 1160218923949113344
     * newsStatus : 0
     * publishTime : 2019-08-11 11:31:53
     */

    public String id;
    public String newsType;
    public String title;
    public String messages;
    public String orderId;
    public String newsStatus;//消息状态0未1已
    public String publishTime;

    @Override
    public String toString() {
        return "MessageBean{" +
                "id='" + id + '\'' +
                ", newsType='" + newsType + '\'' +
                ", title='" + title + '\'' +
                ", messages='" + messages + '\'' +
                ", orderId='" + orderId + '\'' +
                ", newsStatus='" + newsStatus + '\'' +
                ", publishTime='" + publishTime + '\'' +
                '}';
    }
}
