package com.wxh.worker.ui;

import android.text.TextUtils;

public class RoomTypeUtil {

    /**
     * 轨道类型(1电动直轨2电动弯轨3电动L轨4电动升降直轨5电动升降弯轨)
     *
     * @param trackType
     * @return
     */
    public static String getTrackType(String trackType) {
        if (TextUtils.isEmpty(trackType)) {
            return "";
        }
        switch (trackType) {
            case "1":
                return "电动直轨";
            case "2":
                return "电动弯轨";
            case "3":
                return "电动L轨";
            case "4":
                return "电动升降直轨";
            case "5":
                return "电动升降弯轨";
            default:
                return "电动直轨";
        }
    }

    /**
     * 轨道数量1单层2双层
     *
     * @param trackNum
     * @return
     */
    public static String getTrackNumber(String trackNum) {
        if (TextUtils.isEmpty(trackNum)) {
            return "";
        }
        switch (trackNum) {
            case "1":
                return "单轨";
            case "2":
                return "双轨";
            default:
                return "单轨";
        }
    }

    /**
     * 窗户高度(1、小于等于3.5米2、大于3.5米)
     *
     * @param windowHight
     * @return
     */
    public static String getWindowHight(String windowHight) {
        if (TextUtils.isEmpty(windowHight)) {
            return "";
        }
        switch (windowHight) {
            case "1":
                return "小于等于3.5米";
            case "2":
                return "大于3.5米";
            default:
                return "小于等于3.5米";
        }
    }

    /**
     * 安装方式(1顶装2侧装)
     *
     * @param layerType
     * @return
     */
    public static String getLayerType(String layerType) {
        if (TextUtils.isEmpty(layerType)) {
            return "";
        }
        switch (layerType) {
            case "1":
                return "顶装";
            case "2":
                return "侧装";
            default:
                return "顶装";
        }
    }

    /**
     * 窗幔类型(0无1独立幔2一体幔)
     *
     * @param curtainType
     * @return
     */
    public static String getCurtainType(String curtainType) {
        if (TextUtils.isEmpty(curtainType)) {
            return "";
        }
        switch (curtainType) {
            case "0":
                return "无";
            case "1":
                return "独立幔";
            case "2":
                return "一体幔";
            default:
                return "无";
        }
    }

    /**
     * 是否拆旧0否1是
     *
     * @param oldFlag
     * @return
     */
    public static String getOldFlag(String oldFlag) {
        if (TextUtils.isEmpty(oldFlag)) {
            return "";
        }
        switch (oldFlag) {
            case "0":
                return "否";
            case "1":
                return "是";
            default:
                return "否";
        }
    }

    /**
     * 墙体材质(0普通1大理石2木质3混泥土)
     *
     * @param wallType
     * @return
     */
    public static String getWallType(String wallType) {
        if (TextUtils.isEmpty(wallType)) {
            return "";
        }
        switch (wallType) {
            case "0":
                return "普通";
            case "1":
                return "大理石";
            case "2":
                return "木质";
            case "3":
                return "混泥土";
            default:
                return "普通";
        }
    }

    /**
     * 安装面材质：1木板、2混凝土、3大理石、4石膏板
     *
     * @param materialQuality
     * @return
     */
    public static String getMterialQuality(String materialQuality) {
        if (TextUtils.isEmpty(materialQuality)) {
            return "";
        }
        switch (materialQuality) {
            case "1":
                return "木板";
            case "2":
                return "混凝土";
            case "3":
                return "大理石";
            case "4":
                return "石膏板";
            default:
                return "木板";
        }
    }


    /**
     * 单双轨类型1单轨2双轨
     *
     * @return
     */
    public static String getLevelType(String levelType) {
        if (TextUtils.isEmpty(levelType)) {
            return "";
        }
        switch (levelType) {
            case "1":
                return "单轨";
            case "2":
                return "双轨";
            default:
                return "单轨";
        }
    }

    /**
     * 开合方式类型1单开2双开
     *
     * @return
     */
    public static String getOcType(String ocType) {
        if (TextUtils.isEmpty(ocType)) {
            return "";
        }
        switch (ocType) {
            case "1":
                return "单开";
            case "2":
                return "双开";
            default:
                return "单开";
        }
    }

    /**
     * 是否满墙 1是 2否
     *
     * @return
     */
    public static String getFullFlag(String fullFlag) {
        if (TextUtils.isEmpty(fullFlag)) {
            return "";
        }
        switch (fullFlag) {
            case "1":
                return "是";
            case "2":
                return "否";
            default:
                return "是";
        }
    }

    /**
     * 轨道尺寸
     *
     * @return
     */
    public static String getWindowSize(String width, String leftLength, String rightLength) {
        int mWidth = 0;
        int mLeftLength = 0;
        int mRightLength = 0;
        if (!TextUtils.isEmpty(width)) {
            mWidth = Integer.parseInt(width);
        }
        if (!TextUtils.isEmpty(leftLength)) {
            mLeftLength = Integer.parseInt(leftLength);
        }
        if (!TextUtils.isEmpty(rightLength)) {
            mRightLength = Integer.parseInt(rightLength);
        }
        double m = (mWidth + mLeftLength + mRightLength) * 0.01;
        return m + " 米";
    }
}
