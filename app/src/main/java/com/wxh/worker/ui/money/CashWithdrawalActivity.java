package com.wxh.worker.ui.money;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.mvp.BaseMvpActivity;
import com.common.retrofit.RxObserver;
import com.common.utils.MoneyUtil;
import com.common.utils.Utils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.utils.RxUtils;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.dialog.ConfirmDialog;
import com.wxh.worker.json.CashWithDrawalBean;
import com.wxh.worker.ui.fragment.order.OrderLabelAdapter;
import com.wxh.worker.ui.fragment.order.bean.OrderLabelBean;
import com.wxh.worker.ui.setting.contact.ContactActivity;

import java.util.List;

import butterknife.BindView;

import static com.utilCode.utils.AndroidUtils.getContext;

public class CashWithdrawalActivity extends BaseMvpActivity<MoneyView, MoneyPresent> implements MoneyView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.rv_order_top_gridview)
    RecyclerView rvOrderTopGridview;
    @BindView(R.id.srl)
    SwipeRefreshLayout mSrl;
    @BindView(R.id.rv_order_list)
    RecyclerView rvOrderList;

    private OrderLabelAdapter labelAdapter;
    private List<OrderLabelBean> orderLabelBeans;
    private String status = "0";
    private LoadingDialog mLoadingDialog;
    private CashWithDrawalAdapter withDrawalAdapter;
    private ConfirmDialog mConfirmDialog;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, CashWithdrawalActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_cash_withdrawal;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarTitle.setText("订单提现");
        toolbarBack.setOnClickListener(this);

        GridLayoutManager layoutManager = new GridLayoutManager(CashWithdrawalActivity.this, 3, LinearLayoutManager.VERTICAL, false);
        rvOrderTopGridview.setLayoutManager(layoutManager);
        labelAdapter = new OrderLabelAdapter(getContext(), labelClickListener);
        rvOrderTopGridview.setAdapter(labelAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvOrderList.setLayoutManager(linearLayoutManager);
        withDrawalAdapter = new CashWithDrawalAdapter(CashWithdrawalActivity.this, withDrawalListener);
        rvOrderList.setAdapter(withDrawalAdapter);

        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

        orderLabelBeans = OrderLabelBean.getLabelData(status);
        labelAdapter.resetData(orderLabelBeans);
        if (!mLoadingDialog.isVisible()) {
            mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
        }
        getPresenter().getWithdrawOrders(status);
    }

    private MoneyView.WithDrawalListener withDrawalListener = new MoneyView.WithDrawalListener() {
        @Override
        public void onItemDrawal(CashWithDrawalBean item) {
            ConfirmDialog.Builder builder = new ConfirmDialog.Builder(CashWithdrawalActivity.this);
            mConfirmDialog = builder.setContentText("提现金额：" + MoneyUtil.parseMoneyWithComma(item.otherAmt + "") + "元").setConfirmListener(new ConfirmDialog.Builder.ConfirmClickListener() {
                @Override
                public void click(boolean b) {
                    mConfirmDialog.dismiss();
                    if (b) {
                        if (!mLoadingDialog.isVisible()) {
                            mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                        }
                        getPresenter().postApplyWithdraw(item.id);
                    } else {

                    }
                }
            }).create();
            mConfirmDialog.show();
        }
    };

    private OrderLabelAdapter.LabelClickListener labelClickListener = new OrderLabelAdapter.LabelClickListener() {

        @Override
        public void onLabelItemClick(OrderLabelBean item) {
            labelAdapter.getData().clear();
            for (OrderLabelBean labelBean : orderLabelBeans) {
                labelBean.labelSelect = false;
                if (labelBean.labelIndex == item.labelIndex) {
                    labelBean.labelSelect = true;
                }
            }
            labelAdapter.resetData(orderLabelBeans);
            if (!mLoadingDialog.isVisible()) {
                mLoadingDialog.show(getSupportFragmentManager(), "loadingdialog");
            }
            status = item.orderStatus;
            getPresenter().getWithdrawOrders(status);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
        }
    }

    @Override
    public void onSuccess(List<CashWithDrawalBean> cashWithDrawalBeanList) {
        mLoadingDialog.dismissAllowingStateLoss();
        if (cashWithDrawalBeanList == null || cashWithDrawalBeanList.size() == 0) {
            withDrawalAdapter.resetData(cashWithDrawalBeanList);
            withDrawalAdapter.setEmptyView(R.layout.empty_view);
            withDrawalAdapter.loadMoreEnd(false);
            withDrawalAdapter.loadMoreComplete();
        } else {
            for (CashWithDrawalBean drawalBean : cashWithDrawalBeanList) {
                drawalBean.status = status;
            }
            withDrawalAdapter.resetData(cashWithDrawalBeanList);
        }
    }

    @Override
    public void onWithdrawalSuccess() {
        mLoadingDialog.dismissAllowingStateLoss();
        ToastUtils.showLongToastSafe("申请成功");
        getPresenter().getWithdrawOrders(status);
    }

    @Override
    public void onFail() {
        mLoadingDialog.dismissAllowingStateLoss();
    }
}
