package com.wxh.worker.ui;

import com.utilCode.base.mvp.MvpView;

public interface HomeView extends MvpView {

    void onMsgSuccess(int statusNumber);

    void onFail();

}
