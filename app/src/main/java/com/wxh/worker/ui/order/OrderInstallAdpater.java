package com.wxh.worker.ui.order;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;
import com.wxh.worker.json.OrderDetailBean;
import com.wxh.worker.ui.RoomTypeUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderInstallAdpater extends BaseRecycleViewAdapter<OrderDetailBean.OrderExtendDTOsBean> {

    InstallClickListener installClickListener;

    public OrderInstallAdpater(Context context, InstallClickListener installClickListener) {
        super(context);
        this.installClickListener = installClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, OrderDetailBean.OrderExtendDTOsBean item) {
        if (holder instanceof InstallHolder) {
            InstallHolder installHolder = (InstallHolder) holder;
            installHolder.tvItemOrderNo.setText("" + item.listIndex);
            installHolder.tvOrderShopName.setText(item.shopName);
            installHolder.tvTrackType.setText(RoomTypeUtil.getTrackType(item.trackType));
            installHolder.tvTrackWidth.setText(item.trackWidth + " 厘米");
            installHolder.tvTrackNumber.setText(RoomTypeUtil.getTrackNumber(item.trackNum));
            installHolder.tvTrackHeight.setText(RoomTypeUtil.getWindowHight(item.windowHight));
            installHolder.tvTrackInstall.setText(RoomTypeUtil.getLayerType(item.layerType));
            installHolder.tvTrackCurtain.setText(RoomTypeUtil.getCurtainType(item.curtainType));
            installHolder.tvTrackUsed.setText(RoomTypeUtil.getOldFlag(item.oldFlag));
            installHolder.tvTrackMaterial.setText(RoomTypeUtil.getWallType(item.wallType));
            installHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                     if(installClickListener != null){
                         installClickListener.onInstallItemClick(item);
                     }
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new InstallHolder(View.inflate(mContext, R.layout.item_order_window, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }



    class InstallHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_item_order_no)
        TextView tvItemOrderNo;
        @BindView(R.id.tv_order_shop_name)
        TextView tvOrderShopName;
        @BindView(R.id.tv_track_type)
        TextView tvTrackType;//轨道类型
        //        @BindView(R.id.tv_track_Lifting)
//        TextView tvTrackLifting;//升降系统
        @BindView(R.id.tv_track_width)
        TextView tvTrackWidth;//轨道宽度
        @BindView(R.id.tv_track_number)
        TextView tvTrackNumber;//轨道数量
        @BindView(R.id.tv_track_height)
        TextView tvTrackHeight;//轨道高度
        @BindView(R.id.tv_track_install)
        TextView tvTrackInstall;//安装方式
        //        @BindView(R.id.tv_track_window)
//        TextView tvTrackWindow;//窗户类型
        @BindView(R.id.tv_track_curtain)
        TextView tvTrackCurtain;//窗幔
        @BindView(R.id.tv_track_used)
        TextView tvTrackUsed;//是否拆旧
        @BindView(R.id.tv_track_material)
        TextView tvTrackMaterial;//墙体材质

        InstallHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface InstallClickListener {
        void onInstallItemClick(OrderDetailBean.OrderExtendDTOsBean item);
    }
}
