package com.wxh.worker.ui.fragment.order.bean;

import android.support.annotation.Keep;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

@Keep
public class OrderLabelBean {

    public int labelIndex;
    public String labelName;
    /**
     * 0 草稿
     * 1 商户已撤回
     * 2 已发布-待派单
     * 3 已指定师傅-待接收
     * 4 师傅接单-待预约
     * 5 已预约-服务中
     * 6 师傅申请验收-待验收
     * 7 商户验收-未通过
     * 8 验收通过已完成-未提现
     * 9 商户申请-二次上门
     * 10 平台删除订单
     * 11 后台撤销
     * 12 验收通过已完成-已提现
     * 13 平台验收冻结-未通过
     * 14 商户删除订单
     */
    public String orderStatus;
    public boolean labelSelect;

    public OrderLabelBean() {
        super();
    }

    public OrderLabelBean(int labelIndex, String labelName, String orderStatus, boolean labelSelect) {
        this.labelIndex = labelIndex;
        this.labelName = labelName;
        this.orderStatus = orderStatus;
        this.labelSelect = labelSelect;
    }

    @Override
    public String toString() {
        return "OrderLabelBean{" +
                "labelIndex=" + labelIndex +
                ", labelName='" + labelName + '\'' +
                ", orderStatus='" + orderStatus + '\'' +
                ", labelSelect=" + labelSelect +
                '}';
    }

    public static List<OrderLabelBean> setLabelData(String orderStatus){
        List<OrderLabelBean> orderLabelList = new ArrayList<>();
        orderLabelList.add(new OrderLabelBean(1, "待接受", "3", false));
        orderLabelList.add(new OrderLabelBean(2, "待预约", "4", false));
        orderLabelList.add(new OrderLabelBean(3, "服务中", "5", false));
        orderLabelList.add(new OrderLabelBean(4, "待验收", "6", false));
        orderLabelList.add(new OrderLabelBean(5, "已完成", "8", false));
        for (OrderLabelBean labelBean : orderLabelList){
            labelBean.labelSelect = false;
            if(TextUtils.equals(labelBean.orderStatus, orderStatus)){
                labelBean.labelSelect = true;
            }
        }
        return orderLabelList;
    }

    public static List<OrderLabelBean> getLabelData(String status){
        List<OrderLabelBean> orderLabelList = new ArrayList<>();
        orderLabelList.add(new OrderLabelBean(1, "未提现", "0", false));
        orderLabelList.add(new OrderLabelBean(2, "申请中", "1", false));
        orderLabelList.add(new OrderLabelBean(3, "已提现", "2", false));
        for (OrderLabelBean labelBean : orderLabelList){
            labelBean.labelSelect = false;
            if(TextUtils.equals(labelBean.orderStatus, status)){
                labelBean.labelSelect = true;
            }
        }
        return orderLabelList;
    }
}
