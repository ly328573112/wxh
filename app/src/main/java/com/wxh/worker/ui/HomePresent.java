package com.wxh.worker.ui;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;

import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.ui.fragment.message.bean.MessageBean;

import java.util.List;

import javax.inject.Inject;

public class HomePresent extends RxMvpPresenter<HomeView> {

    MyApi mMyApi;

    @Inject
    public HomePresent(MyApi mMyApi){
        this.mMyApi = mMyApi;
    }

    public void getNewsLists(){
        mMyApi.getNewsLists()
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<MessageBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<MessageBean>> data) {
                        if (checkJsonCode(data, false) && data.getResult() != null) {
                            int statusNumber = 0;
                            for (MessageBean messageBean : data.getResult()){
                                if (TextUtils.equals("0", messageBean.newsStatus)) {
                                    statusNumber ++;
                                }
                            }
                            getView().onMsgSuccess(statusNumber);
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

}
