package com.wxh.worker.ui.fragment.home;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.utilCode.webview.WebViewTbsActivity;
import com.wxh.worker.event.RefreshOrderEvent;
import com.common.imageloader.GlideImageLoader;
import com.common.mvp.BaseMvpFragment;
import com.utilCode.dialog.LoadingDialog;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.json.ADBean;
import com.wxh.worker.json.HomeOrderBean;
import com.wxh.worker.ui.fragment.home.bean.HomeListBean;
import com.wxh.worker.ui.money.CashWithdrawalActivity;
import com.wxh.worker.ui.order.OrderListActivity;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerClickListener;
import com.youth.banner.listener.OnBannerListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class HomeFragment extends BaseMvpFragment<HomeFmtView, HomeFmtPresent> implements HomeFmtView, View.OnClickListener {

    @BindView(R.id.home_banner)
    Banner homeBanner;
    @BindView(R.id.rv_home_list)
    RecyclerView rvHomeList;
    @BindView(R.id.ll_home_rawal)
    LinearLayout llHomeRawal;
    @BindView(R.id.ll_home_manual)
    LinearLayout llHomeManual;
    @BindView(R.id.ll_home_study)
    LinearLayout llHomeStudy;

    private HomeListAdapter listAdapter;
    private List<HomeListBean> homeListBean;
    private LoadingDialog mLoadingDialog;
    private PerformClickListener performClickListener;
    private List<ADBean> beanList = new ArrayList<>();

    public void setPerformClickListener(PerformClickListener performClickListener) {
        this.performClickListener = performClickListener;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initInject() {
        getFragmentComponent().inject(this);
    }

    @SuppressLint("NewApi")
    @Override
    protected void setupView(View rootView) {
        llHomeRawal.setOnClickListener(this);
        llHomeManual.setOnClickListener(this);
        llHomeStudy.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvHomeList.setLayoutManager(linearLayoutManager);
        listAdapter = new HomeListAdapter(getContext(), listClickListener);
        rvHomeList.setAdapter(listAdapter);

        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog();
        }
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        homeListBean = HomeListBean.getHomeListBean();
        listAdapter.resetData(homeListBean);
        if (!mLoadingDialog.isVisible()) {
            mLoadingDialog.show(getFragmentManager(), "loadingdialog");
        }
        getPresenter().getOrderCount();
        getPresenter().adPositionLists();
    }

    private HomeListAdapter.HomeListClickListener listClickListener = new HomeListAdapter.HomeListClickListener() {

        @Override
        public void onHomeListItemClick(HomeListBean item) {
            OrderListActivity.startActivity(getActivity(), item.orderStatus);
//            if(performClickListener != null){
//                EventBus.getDefault().post(new RefreshOrderEvent(item.orderStatus));
//                performClickListener.onPerformClick(item.orderStatus);
//            }
        }
    };

    @Override
    public void onOrderListSuccess(List<HomeOrderBean> homeOrderBeans) {
        mLoadingDialog.dismissAllowingStateLoss();
        for (int i = 0; i < homeListBean.size(); i++) {
            if (homeListBean.get(i).index == 1) {
                homeListBean.get(i).numberOfstatus = homeOrderBeans.get(0).dfOrderNum;
            } else if (homeListBean.get(i).index == 2) {
                homeListBean.get(i).numberOfstatus = homeOrderBeans.get(0).djOrderNum;
            } else if (homeListBean.get(i).index == 3) {
                homeListBean.get(i).numberOfstatus = homeOrderBeans.get(0).dtOrderNum;
            } else if (homeListBean.get(i).index == 4) {
                homeListBean.get(i).numberOfstatus = homeOrderBeans.get(0).dyOrderNum;
            }
        }
        listAdapter.resetData(homeListBean);
    }

    @Override
    public void onADSuccess(List<ADBean> adBeanList) {
        if (adBeanList.size() == 0) {
            List<Integer> images = new ArrayList<>();
            images.add(R.drawable.ic_banner);
            setHomeBanner(images);
        } else {
            beanList = adBeanList;
            List<String> images = new ArrayList<>();
            for (ADBean adBean : adBeanList) {
                images.add(adBean.adCode);
            }
            setHomeBanner(images);
        }
    }

    private void setHomeBanner(List<?> imageUrls) {
        homeBanner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        //设置图片加载器
        homeBanner.setImageLoader(new GlideImageLoader());
        //设置图片集合
        homeBanner.setImages(imageUrls);
        //设置banner动画效果
//        homeBanner.setBannerAnimation(Transformer.CubeOut);
        homeBanner.setBannerAnimation(Transformer.Default);
        //设置轮播时间
        homeBanner.setDelayTime(3500);
        //设置指示器位置（当banner模式中有指示器时）
        homeBanner.setIndicatorGravity(BannerConfig.CENTER);
        homeBanner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                if (imageUrls.get(position) instanceof String) {
                    for (ADBean adBean : beanList) {
                        if(position == adBean.positionId){
                            WebViewTbsActivity.start(getActivity(), adBean.adLink, adBean.adName);
                        }
                    }
                } else if (imageUrls.get(position) instanceof Integer) {
                    ToastUtils.showLongToastSafe("广告");
                }
            }
        });
        //banner设置方法全部调用完毕时最后调用
        homeBanner.start();
    }

    @Override
    public void onFail() {
        mLoadingDialog.dismissAllowingStateLoss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_home_rawal:
                CashWithdrawalActivity.startActivity(getActivity());
                break;
            case R.id.ll_home_manual:
                ToastUtils.showLongToastSafe("师傅手册");
                break;
            case R.id.ll_home_study:
                ToastUtils.showLongToastSafe("学习园地");
                break;
        }
    }
}
