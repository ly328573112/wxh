package com.wxh.worker.ui.order;

import com.utilCode.base.mvp.MvpView;
import com.wxh.worker.json.OrderDetailBean;

public interface OrderDetailView extends MvpView {

    void onOrderDetailSuccess(OrderDetailBean orderDetailBean);

    void onAcceptSuccess();

    void onRefuseSuccess();

    void onAppointmentSuccess();

    void onFail(int error);

}
