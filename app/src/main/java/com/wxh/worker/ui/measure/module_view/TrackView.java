package com.wxh.worker.ui.measure.module_view;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;

import com.wxh.worker.R;
import com.wxh.worker.json.MeasureListBean;

public class TrackView<T extends Activity> {

    private Activity activity;
    private EditText etWidth;
    private EditText etLeftLength;
    private EditText etRightLength;
    private EditText etZiDingYi;
    private TrackLisener trackLisener;
    private TextView tvZiDingYiM;
    private LinearLayout tvWidthCm;
    private LinearLayout tvLeftCm;
    private LinearLayout tvRightCm;
    private RadioButton rbZhiGui;
    private RadioButton rbLGui;
    private RadioButton rbHuGui;
    private RadioButton rbDanGui;
    private RadioButton rbShuangGui;
    private RadioButton rbDingZhuang;
    private RadioButton rbCeZhuang;
    private RadioButton rbDanKai;
    private RadioButton rbShuangKai;
    private RadioButton rbXiaoYu;
    private RadioButton rbDaYu;

    public TrackView(T activity, TrackLisener trackLisener) {
        this.activity = activity;
        this.trackLisener = trackLisener;
    }

    public View getView() {
        View view = View.inflate(activity, R.layout.layout_track_view, null);
        etWidth = view.findViewById(R.id.et_width);
        tvWidthCm = view.findViewById(R.id.tv_width_cm);
        etLeftLength = view.findViewById(R.id.et_left_length);
        tvLeftCm = view.findViewById(R.id.tv_left_cm);
        etRightLength = view.findViewById(R.id.et_right_length);
        tvRightCm = view.findViewById(R.id.tv_right_cm);

        RadioGroup rgTrackType = view.findViewById(R.id.rg_track_type);
        rgTrackType.setOnCheckedChangeListener(rgTrackTypeListener);
        rbZhiGui = view.findViewById(R.id.rb_zhi_gui);
        rbLGui = view.findViewById(R.id.rb_l_gui);
        rbHuGui = view.findViewById(R.id.rb_hu_gui);

        RadioGroup rgLevelType = view.findViewById(R.id.rg_level_type);
        rgLevelType.setOnCheckedChangeListener(rgLevelTypeListener);
        rbDanGui = view.findViewById(R.id.rb_dan_gui);
        rbShuangGui = view.findViewById(R.id.rb_shuang_gui);

        RadioGroup rgFixType = view.findViewById(R.id.rg_fix_type);
        rgFixType.setOnCheckedChangeListener(rgFixTypeListener);
        rbDingZhuang = view.findViewById(R.id.rb_ding_zhuang);
        rbCeZhuang = view.findViewById(R.id.rb_ce_zhuang);

        RadioGroup rgOcType = view.findViewById(R.id.rg_oc_type);
        rgOcType.setOnCheckedChangeListener(rgOcTypeListener);
        rbDanKai = view.findViewById(R.id.rb_dan_kai);
        rbShuangKai = view.findViewById(R.id.rb_shuang_kai);

        RadioGroup rgHeightFlag = view.findViewById(R.id.rg_height_flag);
        rgHeightFlag.setOnCheckedChangeListener(rgHeightFlagListener);
        rbXiaoYu = view.findViewById(R.id.rb_xiao_yu);
        rbDaYu = view.findViewById(R.id.rb_da_yu);
        etZiDingYi = view.findViewById(R.id.et_zi_ding_yi);
        tvZiDingYiM = view.findViewById(R.id.tv_zi_ding_yi_m);
        return view;
    }

    public void setMeasureListBean(MeasureListBean measure) {
        if (!TextUtils.isEmpty(measure.trackType)) {
            if (TextUtils.equals(measure.trackType, "1")) {
                rbZhiGui.setChecked(true);
                rbLGui.setChecked(false);
                rbHuGui.setChecked(false);
            } else if (TextUtils.equals(measure.trackType, "3")) {
                rbZhiGui.setChecked(false);
                rbLGui.setChecked(true);
                rbHuGui.setChecked(false);
            } else if (TextUtils.equals(measure.trackType, "5")) {
                rbZhiGui.setChecked(false);
                rbLGui.setChecked(false);
                rbHuGui.setChecked(true);
            }
        }
        if (!TextUtils.isEmpty(measure.levelType)) {
            if (TextUtils.equals(measure.levelType, "1")) {
                rbDanGui.setChecked(true);
                rbShuangGui.setChecked(false);
            } else if (TextUtils.equals(measure.levelType, "2")) {
                rbDanGui.setChecked(false);
                rbShuangGui.setChecked(true);
            }
        }
        if (!TextUtils.isEmpty(measure.fixType)) {
            if (TextUtils.equals(measure.fixType, "1")) {
                rbDingZhuang.setChecked(true);
                rbCeZhuang.setChecked(false);
            } else if (TextUtils.equals(measure.fixType, "2")) {
                rbDingZhuang.setChecked(false);
                rbCeZhuang.setChecked(true);
            }
        }
        if (!TextUtils.isEmpty(measure.ocType)) {
            if (TextUtils.equals(measure.ocType, "1")) {
                rbDanKai.setChecked(true);
                rbShuangKai.setChecked(false);
            } else if (TextUtils.equals(measure.ocType, "2")) {
                rbDanKai.setChecked(false);
                rbShuangKai.setChecked(true);
            }
        }
        if (!TextUtils.isEmpty(measure.heightFlag)) {
            if (TextUtils.equals(measure.heightFlag, "0")) {
                etZiDingYi.setVisibility(View.INVISIBLE);
                tvZiDingYiM.setVisibility(View.INVISIBLE);
                rbXiaoYu.setChecked(true);
                rbDaYu.setChecked(false);
            } else if (TextUtils.equals(measure.heightFlag, "1")) {
                etZiDingYi.setVisibility(View.VISIBLE);
                tvZiDingYiM.setVisibility(View.VISIBLE);
                etZiDingYi.setText(measure.height);
                rbXiaoYu.setChecked(false);
                rbDaYu.setChecked(true);
            }
        }
    }

    public void setEdittextVisibility(int width, int left, int right) {
        etWidth.setVisibility(width);
        tvWidthCm.setVisibility(width);
        etLeftLength.setVisibility(left);
        tvLeftCm.setVisibility(left);
        etRightLength.setVisibility(right);
        tvRightCm.setVisibility(right);
    }

    public String getWidth() {
        return etWidth.getText().toString().trim();
    }

    public String getLeftLength() {
        return etLeftLength.getText().toString().trim();
    }

    public String getRightLength() {
        return etRightLength.getText().toString().trim();
    }

    public String getZiDingYi() {
        return etZiDingYi.getText().toString().trim();
    }

    private RadioGroup.OnCheckedChangeListener rgTrackTypeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.rb_zhi_gui:
                    trackLisener.onTrackType("1");
                    break;
                case R.id.rb_l_gui:
                    trackLisener.onTrackType("3");
                    break;
                case R.id.rb_hu_gui:
                    trackLisener.onTrackType("5");
                    break;
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener rgLevelTypeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.rb_dan_gui:
                    trackLisener.onLevelType("1");
                    break;
                case R.id.rb_shuang_gui:
                    trackLisener.onLevelType("2");
                    break;
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener rgFixTypeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.rb_ding_zhuang:
                    trackLisener.onFixType("1");
                    break;
                case R.id.rb_ce_zhuang:
                    trackLisener.onFixType("2");
                    break;
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener rgOcTypeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.rb_dan_kai:
                    trackLisener.onOcType("1");
                    break;
                case R.id.rb_shuang_kai:
                    trackLisener.onOcType("2");
                    break;
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener rgHeightFlagListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.rb_xiao_yu:
                    etZiDingYi.setVisibility(View.INVISIBLE);
                    tvZiDingYiM.setVisibility(View.INVISIBLE);
                    trackLisener.onHeightFlag("0", null);
                    break;
                case R.id.rb_da_yu:
                    etZiDingYi.setVisibility(View.VISIBLE);
                    tvZiDingYiM.setVisibility(View.VISIBLE);
                    trackLisener.onHeightFlag("1", null);
                    break;
            }
        }
    };

    public interface TrackLisener {

        void onWidth(String width);

        void onLeftLength(String leftLength);

        void onRightLength(String rightLength);

        void onTrackType(String trackType);

        void onLevelType(String levelType);

        void onFixType(String fixType);

        void onOcType(String ocType);

        void onHeightFlag(String heightFlag, String height);


    }

}
