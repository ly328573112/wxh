package com.wxh.worker.ui.pagelist;

import android.content.Context;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.utilCode.dagger.help.GlideApp;
import com.utilCode.widget.recycleadpter.BasePageRcvAdapter;
import com.wxh.worker.R;
import com.wxh.worker.json.AndroidLib;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PageByPageListAdapter extends BasePageRcvAdapter<AndroidLib, PageByPageListAdapter.PageByIdListHolder> {

    public PageByPageListAdapter(Context context, RetryCallback retryCallback) {
        super(context, DIFF_CALLBACK, retryCallback);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, AndroidLib item) {
        GlideApp.with(mContext)
                .asBitmap()
                .load(item.getOwner().getAvatar_url())
                .into(((PageByIdListHolder) holder).iv_gituser_image);
        ((PageByIdListHolder) holder).tv_gituser_name.setText(item.getFull_name());
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new PageByIdListHolder(View.inflate(parent.getContext(), R.layout.page_list_item, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    public class PageByIdListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_gituser_image)
        RoundedImageView iv_gituser_image;
        @BindView(R.id.tv_gituser_name)
        TextView tv_gituser_name;

        PageByIdListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setupView();
        }

        private void setupView() {

        }
    }

    public static final DiffUtil.ItemCallback<AndroidLib> DIFF_CALLBACK = new DiffUtil.ItemCallback<AndroidLib>() {

        @Override
        public boolean areItemsTheSame(AndroidLib oldItem, AndroidLib newItem) {
            return oldItem != null && newItem != null && oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(AndroidLib oldItem, AndroidLib newItem) {
            return oldItem != null && newItem != null && Objects.equals(oldItem, newItem);
        }
    };

}
