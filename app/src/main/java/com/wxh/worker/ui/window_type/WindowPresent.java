package com.wxh.worker.ui.window_type;

import com.common.config.MyApi;
import com.utilCode.base.mvp.RxMvpPresenter;

import javax.inject.Inject;

public class WindowPresent extends RxMvpPresenter<WindowView> {

    MyApi mMyApi;

    @Inject
    public WindowPresent(MyApi mMyApi) {
        this.mMyApi = mMyApi;
    }

}
