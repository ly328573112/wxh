package com.wxh.worker.ui.login;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;

import com.common.config.Global;
import com.common.mvp.BaseMvpActivity;
import com.common.retrofit.RxObserver;
import com.common.utils.LogUtil;
import com.common.utils.TimeConstants;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.R;
import com.wxh.worker.ui.HomeActivity;
import com.wxh.worker.ui.login.guild.GuideActivity;

public class StartActivity extends BaseMvpActivity<LRView, LRPresent> implements LRView, View.OnClickListener {




    @Override
    protected int getLayoutId() {
        return R.layout.activity_start;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {

        /*--记录App启动时间--*/
        TimeConstants.getInstance().setAppLaunchTS(System.currentTimeMillis());
        getDisplayMetrics();
    }

    public void getDisplayMetrics() {
        DisplayMetrics metrics = new DisplayMetrics();
        metrics = this.getApplicationContext().getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        LogUtil.i(width + "++" + height);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        authorizationPermissions();
    }

    private void authorizationPermissions() {
        try {
            new RxPermissions(StartActivity.this).request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE)
                    .compose(RxUtils.applySchedulersLifeCycle(getMvpView()))
                    .subscribe(new RxObserver<Boolean>() {
                        @Override
                        public void onComplete() {
                            if (!Global.getFirstUsing()) {
                                /*--首次启动进入引导页--*/
                                Global.setFirstUsing(true);
                                startActivity(new Intent(StartActivity.this, GuideActivity.class));
                                finish();
                            } else {
                                getPresenter().getMasterInfo();
                            }

                        }
                    });
        } catch (Exception excp) {
            excp.printStackTrace();
        }
    }

    @Override
    public void onSMS_Success() {

    }

    @Override
    public void onRegister() {

    }

    @Override
    public void onLoginSuccess(String status) {
        if (TextUtils.equals(status, "2")) {
            PerfectMessageActivity.startActivity(this);
        } else {
            HomeActivity.startActivity(this);
        }
        finish();
    }

    @Override
    public void onFail(int type, String message) {
        LoginActivity.startActivity(this);
        finish();
    }

    @Override
    public void onClick(View view) {

    }
}
