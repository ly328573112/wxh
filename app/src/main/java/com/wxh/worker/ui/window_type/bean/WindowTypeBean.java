package com.wxh.worker.ui.window_type.bean;

import android.support.annotation.Keep;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Keep
public class WindowTypeBean implements Serializable {

    public int windowIndex;
    public String windowName;
    public boolean windowSeelct;

    public WindowTypeBean(int windowIndex, String windowName, boolean windowSeelct) {
        this.windowIndex = windowIndex;
        this.windowName = windowName;
        this.windowSeelct = windowSeelct;
    }

    @Override
    public String toString() {
        return "WindowTypeBean{" +
                "windowIndex=" + windowIndex +
                ", windowName='" + windowName + '\'' +
                ", windowSeelct=" + windowSeelct +
                '}';
    }

    public static List<WindowTypeBean> getWindowList() {
        List<WindowTypeBean> arrayList = new ArrayList<>();
        arrayList.add(new WindowTypeBean(1, "U型窗", false));
        arrayList.add(new WindowTypeBean(2, "U型窗", false));
        arrayList.add(new WindowTypeBean(3, "U型窗", false));
        arrayList.add(new WindowTypeBean(4, "U型窗", false));
        arrayList.add(new WindowTypeBean(5, "右L型窗", false));
        arrayList.add(new WindowTypeBean(6, "左L型窗", false));
        arrayList.add(new WindowTypeBean(7, "右L型窗", false));
        arrayList.add(new WindowTypeBean(8, "左L型窗", false));
        arrayList.add(new WindowTypeBean(9, "普通窗", false));
        return arrayList;
    }
}
