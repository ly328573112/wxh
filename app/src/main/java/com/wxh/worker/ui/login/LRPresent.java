package com.wxh.worker.ui.login;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.common.config.Constant;
import com.common.config.Global;
import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.facebook.stetho.common.LogUtil;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.json.MasterInfoBean;
import com.wxh.worker.json.UserInfoBean;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class LRPresent extends RxMvpPresenter<LRView> {

    protected MyApi mMyApi;

    @Inject
    public LRPresent(MyApi myApi) {
        mMyApi = myApi;
    }

    public void sendSMS(String codePic, String phone,
                        LRView.ValidateCodeListener validateCodeListener) {
        validateCodeListener.disableValidateCodeButton();
        mMyApi.sendSms(codePic, phone)
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, true)) {
                            ToastUtils.showLongToast("短信发送成功");
                            countValidateCode(validateCodeListener);
                        } else {
                            validateCodeListener.enableValidateCodeButton();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        validateCodeListener.enableValidateCodeButton();
                    }
                });
    }

    public void countValidateCode(LRView.ValidateCodeListener validateCodeListener) {
        Observable.interval(0, 1, TimeUnit.SECONDS)
                /*.takeUntil(new Predicate<Long>() {
                    @Override
                    public boolean test(Long aLong) throws Exception {
                        // 包含临界条件,条件满足则结束
                        return aLong >= Constant.VALIDETE_CODE_WATING_TIME_MAX;
                    }
                })*/
                .takeWhile(new Predicate<Long>() {
                    @Override
                    public boolean test(Long aLong) throws Exception {
                        // 不包含临界条件,条件不满足则结束
                        return aLong <= Constant.VALIDETE_CODE_WATING_TIME_MAX;
                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Long>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Long>() {

                    @Override
                    public void onNext(Long aLong) {
                        LogUtil.i("onNext = " + aLong);
                        long diff = Constant.VALIDETE_CODE_WATING_TIME_MAX - aLong;
                        validateCodeListener.countValidateCodeButton(diff);
                        //倒计时完毕后，按钮可以再次被点击，重新设置按钮文案
                        if (diff == 0) {
                            validateCodeListener.resendValidateCodeButton();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        validateCodeListener.resendValidateCodeButton();
                    }
                });
    }

    public void register(String phone, String pwd, String smsCode) {
        HashMap<String, String> updateMap = new HashMap<>();
        updateMap.put("phone", phone);
        updateMap.put("pwd", pwd);
        updateMap.put("smsCode", smsCode);
        updateMap.put("userType", "1");// 0(其它)1(师傅)2(供货商)3(服务商)4(平台)5(商户)
        mMyApi.postRegister(updateMap)
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false)) {
                            getView().onRegister();
                        } else {
                            getView().onFail(2, data.getResult() + "");
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(2, "");
                    }
                });
    }

    public void getRetrievePwd(String phone, String smsCode, String password,
                               String confirmPassword) {
        JSONObject requestAttributes = new JSONObject();
        try {
            requestAttributes.put("phone", phone);
            requestAttributes.put("smsCode", smsCode);
            requestAttributes.put("password", password);
            requestAttributes.put("confirmPassword", confirmPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),
                requestAttributes.toString());
        mMyApi.getRetrievePwd(requestBody)
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, true)) {
                            getView().onRegister();
                        } else {
                            getView().onFail(6, data.getResult() + "");
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(6, "");
                    }
                });
    }

    public void loginOn(String phone, String smsCode) {
        mMyApi.getLandLogin(phone, smsCode)
                .compose(RxUtils.<Data<UserInfoBean>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<UserInfoBean>>() {

                    @Override
                    public void onNext(@NonNull Data<UserInfoBean> data) {
                        if (checkJsonCode(data, true)) {
                            Global.setUserInfoBean(data.getResult());
                            Global.setUserPhotoNumber(data.getResult().phone);
//                            getView().onLoginSuccess(data.getResult().status);
                            getMasterInfo();
                        } else {
                            getView().onFail(3, data.getResult() + "");
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(3, "");
                    }
                });
    }

    public void loginOn(String phone, String picturesCode, String smsCode) {
        mMyApi.getLandAndRegister(phone, picturesCode, smsCode)
                .compose(RxUtils.<Data<UserInfoBean>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<UserInfoBean>>() {

                    @Override
                    public void onNext(@NonNull Data<UserInfoBean> data) {
                        if (checkJsonCode(data, false)) {
                            Global.setUserInfoBean(data.getResult());
                            Global.setUserPhotoNumber(data.getResult().phone);
                            if (TextUtils.equals(data.getResult().status, "2") || TextUtils.equals(data.getResult().status, "5")) {
                                getView().onLoginSuccess(data.getResult().status);
                            } else if (TextUtils.equals(data.getResult().status, "0")) {
                                getMasterInfo();
                            } else if (TextUtils.equals(data.getResult().status, "4")) {
                                getView().onFail(3, "账号已冻结");
                            } else if( TextUtils.equals(data.getResult().status, "3")){
                                getView().onFail(3, "账号待审核");
                            } else if(TextUtils.equals(data.getResult().status, "1")){
                                getView().onFail(3, "账号已删除");
                            }
                        } else {
                            getView().onFail(3, data.getMessage());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(3, "登录异常");
                    }
                });
    }

    public void getMasterInfo() {
        mMyApi.getMasterInfo()
                .compose(RxUtils.<Data<MasterInfoBean>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<MasterInfoBean>>() {

                    @Override
                    public void onNext(@NonNull Data<MasterInfoBean> data) {
                        if (checkJsonCode(data, false) && data.getResult() != null) {
                            if (TextUtils.isEmpty(data.getResult().masterAddress)) {
                                getView().onLoginSuccess("2");
                            } else {
                                Global.setMasterInfoBean(data.getResult());
                                getView().onLoginSuccess("1");
                            }
                        } else if (data.getResult() == null) {
                            getView().onLoginSuccess("2");
                        } else {
                            getView().onFail(3, data.getResult() + "");
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(3, "");
                    }
                });
    }

    public void postPerfectMessages(RequestBody requestBody) {
        mMyApi.postPerfectMessages(requestBody)
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        LogUtil.i(data.toString());
                        if (checkJsonCode(data, true)) {
                            getView().onLoginSuccess(null);
                        } else {
                            getView().onFail(5, data.getResult() + "");
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(5, "");
                    }
                });
    }

    public void deleteImg(String fileName) {
        mMyApi.deleteImg(fileName)
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        LogUtil.i(data.toString());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }
                });
    }

    public void getVersion() {
        mMyApi.getVersion()
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        LogUtil.i(data.toString());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }
                });
    }

    public void getGenerate() {
        mMyApi.getGenerate()
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        LogUtil.i(data.toString());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                    }
                });
    }

}
