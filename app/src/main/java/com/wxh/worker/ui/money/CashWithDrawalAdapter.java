package com.wxh.worker.ui.money;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.common.utils.MoneyUtil;
import com.utilCode.widget.recycleadpter.BaseRecycleViewAdapter;
import com.wxh.worker.R;
import com.wxh.worker.json.CashWithDrawalBean;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CashWithDrawalAdapter extends BaseRecycleViewAdapter<CashWithDrawalBean> {

    private MoneyView.WithDrawalListener withDrawalListener;

    public CashWithDrawalAdapter(Context context, MoneyView.WithDrawalListener withDrawalListener) {
        super(context);
        this.withDrawalListener = withDrawalListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, CashWithDrawalBean item) {
        if (holder instanceof WithDrawalHolder) {
            WithDrawalHolder drawalHolder = (WithDrawalHolder) holder;
            setTextDrawable(drawalHolder.tvItemOrderNumber, item.type);
            drawalHolder.tvItemOrderNumber.setText("订单编号：" + item.orderNo);
            setOrderAmount(drawalHolder.tvItemOrderAmount, MoneyUtil.parseMoneyWithComma(item.otherAmt + ""));
            drawalHolder.tvItemAddress.setText(item.addressDetail);
            drawalHolder.tvItemTime.setText(item.successTime);
            if (TextUtils.equals("0", item.status)) {
                drawalHolder.tvButtonRedWithdrawal.setVisibility(View.VISIBLE);
                drawalHolder.tvButtonRedWithdrawal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        withDrawalListener.onItemDrawal(item);
                    }
                });
            } else {
                drawalHolder.tvButtonRedWithdrawal.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new WithDrawalHolder(View.inflate(mContext, R.layout.item_cash_with_drawal, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    public void setTextDrawable(TextView textView, String type) {
        if (TextUtils.equals(type, "1")) {
            Drawable drawableLeft = mContext.getResources().getDrawable(R.drawable.ic_order_measure);
            textView.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, null, null, null);
        } else if (TextUtils.equals(type, "2")) {
            Drawable drawableLeft = mContext.getResources().getDrawable(R.drawable.ic_order_install);
            textView.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, null, null, null);
        }
        textView.setCompoundDrawablePadding(10);
    }

    public void setOrderAmount(TextView textView, String amount) {
        if (TextUtils.isEmpty(amount)) {
            textView.setText("");
            return;
        }
        SpannableStringBuilder style = new SpannableStringBuilder();
        //设置文字
        style.append(amount + " 元");
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#E60000"));
        style.setSpan(colorSpan, 0, style.length() - 2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //配置给TextView
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(style);
    }

    class WithDrawalHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_item_order_number)
        TextView tvItemOrderNumber;
        @BindView(R.id.tv_item_order_amount)
        TextView tvItemOrderAmount;
        @BindView(R.id.tv_item_address)
        TextView tvItemAddress;
        @BindView(R.id.tv_item_time)
        TextView tvItemTime;
        @BindView(R.id.tv_button_red_withdrawal)
        TextView tvButtonRedWithdrawal;

        WithDrawalHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
