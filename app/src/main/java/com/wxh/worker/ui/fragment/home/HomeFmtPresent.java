package com.wxh.worker.ui.fragment.home;

import android.support.annotation.NonNull;

import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.common.utils.LogUtil;
import com.common.utils.TimeUtils;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.json.ADBean;
import com.wxh.worker.json.HomeOrderBean;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

public class HomeFmtPresent extends RxMvpPresenter<HomeFmtView> {

    MyApi mMyApi;

    @Inject
    public HomeFmtPresent(MyApi mMyApi){
        this.mMyApi = mMyApi;
    }

    public void getOrderCount(){
        String systemTimes = TimeUtils.getSystemTimes();
        HashMap<String, String> hashMap = new HashMap<>();
//        hashMap.put("beginDate", systemTimes + " 00:00:00");// 开始时间
//        hashMap.put("endDate", systemTimes + " 23:59:59");// 结束时间
//        hashMap.put("beginAppointmentTime", systemTimes + " 00:00:00");// 开始时间
//        hashMap.put("endAppointmentTime", systemTimes + " 23:59:59");// 结束时间
        /**
         * 0 草稿
         * 1 商户已撤回
         * 2 已发布-待派单
         * 3 已指定师傅-待接收
         * 4 师傅接单-待预约
         * 5 已预约-服务中
         * 6 师傅申请验收-待验收
         * 7 商户验收-未通过
         * 8 验收通过已完成-未提现
         * 9 商户申请-二次上门
         * 10 平台删除订单
         * 11 后台撤销
         * 12 验收通过已完成-已提现
         * 13 平台验收冻结-未通过
         * 14 商户删除订单
         */
//        hashMap.put("orderStatus", "");
//        hashMap.put("params", "");//业主姓名/手机号/地址/
//        hashMap.put("type", "0");//1表示测量 2表示安装 0全部
        mMyApi.getOrderCount(hashMap)
                .compose(RxUtils.applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<HomeOrderBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<HomeOrderBean>> data) {
                        LogUtil.d(data.toString());
                        if (checkJsonCode(data, false) && data.getResult() != null) {
                            getView().onOrderListSuccess(data.getResult());
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

    public void adPositionLists() {
        mMyApi.adPositionLists()
                .compose(RxUtils.<Data<List<ADBean>>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<ADBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<ADBean>> data) {
                        if (checkJsonCode(data, true)) {
                            getView().onADSuccess(data.getResult());
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

}
