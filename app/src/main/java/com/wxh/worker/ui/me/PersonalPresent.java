package com.wxh.worker.ui.me;

import android.support.annotation.NonNull;

import com.common.config.Global;
import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.facebook.stetho.common.LogUtil;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.json.MasterInfoBean;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class PersonalPresent extends RxMvpPresenter<PersonalView> {

    MyApi mMyApi;

    @Inject
    public PersonalPresent(MyApi mMyApi) {
        this.mMyApi = mMyApi;
    }

    public void postEditUrgent(String name, String phone, String relation) {
        JSONObject requestAttributes = new JSONObject();
        try {
            requestAttributes.put("emergencyContactsName", name);
            requestAttributes.put("emergencyContactsPhone", phone);
            requestAttributes.put("emergencyContactsRelation", relation);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), requestAttributes.toString());
        mMyApi.postEditUrgent(requestBody)
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        LogUtil.i(data.toString());
                        if (checkJsonCode(data, true)) {
//                            getView().onLoginSuccess(null);
                            getMasterInfo();
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

    public void getMasterInfo() {
        mMyApi.getMasterInfo()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<MasterInfoBean>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<MasterInfoBean>>() {

                    @Override
                    public void onNext(@NonNull Data<MasterInfoBean> data) {
                        if (checkJsonCode(data, false) && data.getResult() != null) {
                            Global.setMasterInfoBean(data.getResult());
                            getView().onSuccess();
                        } else if (data.getResult() == null) {
                            getView().onSuccess();
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

}
