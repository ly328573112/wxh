package com.wxh.worker.ui.money;

import android.support.annotation.NonNull;

import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.json.CashWithDrawalBean;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MoneyPresent extends RxMvpPresenter<MoneyView> {

    MyApi mMyApi;

    @Inject
    public MoneyPresent(MyApi mMyApi) {
        this.mMyApi = mMyApi;
    }


    public void getWithdrawOrders(String status) {
        mMyApi.getWithdrawOrders(status)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<List<CashWithDrawalBean>>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<CashWithDrawalBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<CashWithDrawalBean>> data) {
                        if (checkJsonCode(data, false)) {
                            getView().onSuccess(data.getResult());
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

    public void postApplyWithdraw(String orderId) {
        mMyApi.postApplyWithdraw(orderId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data>() {

                    @Override
                    public void onNext(@NonNull Data data) {
                        if (checkJsonCode(data, false)) {
                            getView().onWithdrawalSuccess();
                        } else {
                            ToastUtils.showLongToastSafe("申请失败");
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        ToastUtils.showLongToastSafe("申请失败");
                        getView().onFail();
                    }
                });
    }

}
