package com.wxh.worker.ui.setting.opinion;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.mvp.BaseMvpActivity;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.ui.setting.SettingActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;

public class FeedBackActivity extends BaseMvpActivity<FeedBackView, FeedBackPresent> implements FeedBackView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.et_feedback_content)
    EditText etFeedbackContent;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.tv_button_red_c)
    TextView tvButtonRedC;

    private int mSuggestionTextCount = 0;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, FeedBackActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_feed_back;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarBack.setOnClickListener(this);
        tvButtonRedC.setOnClickListener(this);
        tvButtonRedC.setVisibility(View.VISIBLE);
        tvButtonRedC.setText("提交");
        toolbarTitle.setText("意见反馈");

        etFeedbackContent.setFilters(new InputFilter[]{emojiFilter, new InputFilter.LengthFilter(300)});
        etFeedbackContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvCount.setText(s.length() + "/300");
//                mSuggestionTextCount = s.length();
//                if (mSuggestionTextCount == 0) {
//                    tvButtonRedC.setTextColor(ContextCompat.getColor(FeedBackActivity.this, R.color.c_999999));
//                    tvButtonRedC.setEnabled(false);
//                } else {
//                    tvButtonRedC.setTextColor(ContextCompat.getColor(FeedBackActivity.this, R.color.c_E60000));
//                    tvButtonRedC.setEnabled(true);
//                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }

    private InputFilter emojiFilter = new InputFilter() {
        Pattern emoji = Pattern.compile(
                "[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",
                Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            Matcher emojiMatcher = emoji.matcher(source);
            if (emojiMatcher.find()) {
                ToastUtils.showLongToast("不能输入表情");
                return "";
            }
            return null;
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.tv_button_red_c:
                String message = etFeedbackContent.getText().toString();
                if (TextUtils.isEmpty(message)) {
                    ToastUtils.showLongToast("请输入您要反馈的问题");
                    return;
                }
                getPresenter().feedbackMessages(message);
                break;
        }
    }

    @Override
    public void onSuccess() {
        ToastUtils.showLongToastSafe("提交成功");
        finish();
    }

    @Override
    public void onFail() {
        ToastUtils.showLongToastSafe("提交失败");
    }
}
