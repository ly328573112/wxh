package com.wxh.worker.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.config.Global;
import com.common.widght.BadgeView;
import com.tencent.bugly.crashreport.CrashReport;
import com.wxh.worker.event.LoginInvalideEvent;
import com.common.mvp.BaseMvpActivity;
import com.common.retrofit.RxObserver;
import com.common.utils.Utils;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.utilCode.base.BaseApplication;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.R;
import com.wxh.worker.dialog.ConfirmDialog;
import com.wxh.worker.ui.fragment.home.HomeFmtView;
import com.wxh.worker.ui.fragment.home.HomeFragment;
import com.wxh.worker.ui.fragment.me.MeFragment;
import com.wxh.worker.ui.fragment.message.MessageFragment;
import com.wxh.worker.ui.fragment.order.OrderFragment;
import com.wxh.worker.ui.login.LoginActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

public class HomeActivity extends BaseMvpActivity<HomeView, HomePresent> implements HomeView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.right_image)
    ImageView rightImage;
    @BindView(R.id.content)
    FrameLayout content;
    @BindView(R.id.rg)
    RadioGroup rg;
    @BindView(R.id.rb_home)
    RadioButton rbHome;
    @BindView(R.id.rb_order)
    RadioButton rbOrder;
    @BindView(R.id.rb_message)
    RadioButton rbMessage;
    @BindView(R.id.rb_mine)
    RadioButton rbMine;
    @BindView(R.id.btn_msg)
    Button btnMsg;

    FragmentTransaction ft;

    HomeFragment mHomeFragment;
    OrderFragment mOrderFragment;
    MessageFragment mMessageFragment;
    MeFragment mMeFragment;

    public static String oStatus = "3";//处理首次进入首页点击待服务订单状态
    private ConfirmDialog mConfirmDialog;
    private String servicePhone = "0571-88965007";
    private BadgeView badgeView;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, HomeActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void setupView() {
        toolbarBack.setVisibility(View.GONE);
        toolbarTitle.setText("威星汇师傅端");
        rbHome.setOnClickListener(this);
        rbOrder.setOnClickListener(this);
        rbMessage.setOnClickListener(this);
        rbMine.setOnClickListener(this);
        rightImage.setOnClickListener(onClickListener);
        rightImage.setVisibility(View.VISIBLE);
        rightImage.setImageResource(R.drawable.ic_home_service);

        showFragment(R.id.rb_home);
        remind(btnMsg);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ConfirmDialog.Builder builder = new ConfirmDialog.Builder(HomeActivity.this);
            mConfirmDialog = builder.setContentText("客服电话：" + servicePhone).setConfirmListener(new ConfirmDialog.Builder.ConfirmClickListener() {
                @Override
                public void click(boolean b) {
                    mConfirmDialog.dismiss();
                    if (b) {
                        try {
                            new RxPermissions(HomeActivity.this).request(Manifest.permission.CALL_PHONE)
                                    .compose(RxUtils.applySchedulersLifeCycle(getMvpView()))
                                    .subscribe(new RxObserver<Boolean>() {
                                        @Override
                                        public void onComplete() {
                                            Utils.callPhone(HomeActivity.this,
                                                    servicePhone.replace("-", ""));
                                        }
                                    });
                        } catch (Exception excp) {
                            excp.printStackTrace();
                        }
                    }
                }
            }).create();
            mConfirmDialog.show();
        }
    };

    @Override
    protected void setupData(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        getPresenter().getNewsLists();
    }

    private void remind(View view) { //BadgeView的具体使用
        // 创建一个BadgeView对象，view为你需要显示提醒的控件
        badgeView = new BadgeView(this, view);
//        badgeView.setContentText(12); // 需要显示的提醒类容
        badgeView.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);// 显示的位置.右上角,BadgeView.POSITION_BOTTOM_LEFT,下左，还有其他几个属性
        badgeView.setTextColor(Color.WHITE); // 文本颜色
        badgeView.setBadgeBackgroundColor(Color.RED); // 提醒信息的背景颜色，自己设置
        //badge1.setBackgroundResource(R.mipmap.icon_message_png); //设置背景图片
        badgeView.setTextSize(8); // 文本大小
        //badge1.setBadgeMargin(3, 3); // 水平和竖直方向的间距
        badgeView.setBadgeMargin(30, 3); //各边间隔
        // badge1.toggle(); //显示效果，如果已经显示，则影藏，如果影藏，则显示
//        badgeView.show();// 只有显示
        // badge1.hide();//影藏显示
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("rb_checked", rg.getCheckedRadioButtonId());
        super.onSaveInstanceState(outState);
    }

    private HomeFmtView.PerformClickListener performClickListener = new HomeFmtView.PerformClickListener() {
        @Override
        public void onPerformClick(String orderStatus) {
            oStatus = orderStatus;
            rbOrder.performClick();
        }
    };

    @Override
    public void onClick(View view) {
        showFragment(view.getId());
    }

    public void showFragment(int index) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ft = fragmentManager.beginTransaction();
        switch (index) {
            case R.id.rb_home:
                toolbarTitle.setText("威星汇师傅端");
                hideFragment(ft);
                if (mHomeFragment != null) {
                    ft.show(mHomeFragment);
                } else {
                    mHomeFragment = new HomeFragment();
                    ft.add(R.id.content, mHomeFragment);
                    mHomeFragment.setPerformClickListener(performClickListener);
                }
                ft.commitAllowingStateLoss();
                break;
            case R.id.rb_order:
                toolbarTitle.setText("订单中心");
                hideFragment(ft);
                if (mOrderFragment != null) {
                    ft.show(mOrderFragment);
                } else {
                    mOrderFragment = new OrderFragment();
                    ft.add(R.id.content, mOrderFragment);
                }
                ft.commitAllowingStateLoss();
                break;
            case R.id.rb_message:
                toolbarTitle.setText("消息中心");
                hideFragment(ft);
                if (mMessageFragment != null) {
                    ft.show(mMessageFragment);
                } else {
                    mMessageFragment = new MessageFragment();
                    ft.add(R.id.content, mMessageFragment);
                }
                ft.commitAllowingStateLoss();
                break;
            case R.id.rb_mine:
                toolbarTitle.setText("我的");
                hideFragment(ft);
                if (mMeFragment != null) {
                    ft.show(mMeFragment);
                } else {
                    mMeFragment = new MeFragment();
                    ft.add(R.id.content, mMeFragment);
                }
                ft.commitAllowingStateLoss();
                break;
            default:
                break;
        }
    }

    @Override
    public void onAttachFragment(android.support.v4.app.Fragment fragment) {
        super.onAttachFragment(fragment);
        if (mHomeFragment == null && fragment instanceof HomeFragment) {
            mHomeFragment = (HomeFragment) fragment;
            getSupportFragmentManager().beginTransaction().hide(mHomeFragment).commit();
        } else if (mOrderFragment == null && fragment instanceof OrderFragment) {
            mOrderFragment = (OrderFragment) fragment;
            getSupportFragmentManager().beginTransaction().hide(mOrderFragment).commit();
        } else if (mMessageFragment == null && fragment instanceof MessageFragment) {
            mMessageFragment = (MessageFragment) fragment;
            getSupportFragmentManager().beginTransaction().hide(mMessageFragment).commit();
        } else if (mMeFragment == null && fragment instanceof MeFragment) {
            mMeFragment = (MeFragment) fragment;
            getSupportFragmentManager().beginTransaction().hide(mMeFragment).commit();
        }
    }

    private void hideFragment(android.support.v4.app.FragmentTransaction ft) {
        if (mHomeFragment != null) {
            ft.hide(mHomeFragment);
        }
        if (mOrderFragment != null) {
            ft.hide(mOrderFragment);
        }
        if (mMessageFragment != null) {
            ft.hide(mMessageFragment);
        }
        if (mMeFragment != null) {
            ft.hide(mMeFragment);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return mDoubleClickExitHelper.onKeyDown(keyCode, event);

            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LoginInvalideEvent event) {
        /*--清空所有页面--*/
        BaseApplication.getAppManager().finishAllActivity();
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        Global.loginOutclear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onMsgSuccess(int statusNumber) {
        if (statusNumber > 0) {
            badgeView.setContentText(statusNumber);
            badgeView.show();// 显示
        } else {
            badgeView.hide();
        }
    }

    @Override
    public void onFail() {
        badgeView.hide();//隐藏
    }
}
