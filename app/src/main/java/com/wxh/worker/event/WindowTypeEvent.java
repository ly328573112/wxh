package com.wxh.worker.event;

import android.support.annotation.Keep;

@Keep
public class WindowTypeEvent {

    public int windowType;
    public String windowName;

    public WindowTypeEvent(int windowType, String windowName) {
        this.windowType = windowType;
        this.windowName = windowName;
    }
}
