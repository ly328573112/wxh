package com.wxh.worker.event;

import android.support.annotation.Keep;

@Keep
public class RefreshOrderEvent {

    public String orderStatus;

    public RefreshOrderEvent(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
