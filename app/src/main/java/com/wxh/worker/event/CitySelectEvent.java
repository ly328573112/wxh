package com.wxh.worker.event;

import android.support.annotation.Keep;

import com.wxh.worker.city_select.bean.CityBean;
import com.wxh.worker.city_select.bean.DistrictBean;
import com.wxh.worker.city_select.bean.ProvincesBean;

@Keep
public class CitySelectEvent {

    public ProvincesBean provincesBean;
    public CityBean cityBean;
    public DistrictBean districtBean;

    public CitySelectEvent(ProvincesBean provincesBean, CityBean cityBean, DistrictBean districtBean){
        this.provincesBean = provincesBean;
        this.cityBean = cityBean;
        this.districtBean = districtBean;
    }

}
