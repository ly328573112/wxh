package com.wxh.worker.city_select;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wxh.worker.event.CitySelectEvent;
import com.common.mvp.BaseMvpActivity;
import com.utilCode.utils.ToastUtils;
import com.wxh.worker.R;
import com.wxh.worker.city_select.bean.CityBean;
import com.wxh.worker.city_select.bean.DistrictBean;
import com.wxh.worker.city_select.bean.ProvincesBean;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;

public class CitySelectActivity extends BaseMvpActivity<CityView, CityPresent> implements CityView, View.OnClickListener {

    @BindView(R.id.toolbar_back)
    RelativeLayout toolbarBack;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.tv_select_city_ok)
    TextView tvSelectCityOk;

    @BindView(R.id.drop_down_list_view)
    XCDropDownListView dropDownListView;
    @BindView(R.id.drop_down_city_view)
    XCDropCityListView dropDownCityView;
    @BindView(R.id.drop_down_district_view)
    XCDropDistrictListView dropDownDistrictView;

    private ProvincesBean provinces;
    private CityBean city;
    private DistrictBean district;

    public static void startActivity(Activity activity, ProvincesBean provinces, CityBean city,
                                     DistrictBean district) {
        Intent intent = new Intent(activity, CitySelectActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("provinces", provinces);
        bundle.putSerializable("city", city);
        bundle.putSerializable("district", district);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_city_select;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        toolbarTitle.setText("选择省市区");
        toolbarBack.setOnClickListener(this);
        tvSelectCityOk.setOnClickListener(this);

        dropDownListView.setProvincesListener(provincesListener);
        dropDownCityView.setCityItemListener(cityListener);
        dropDownDistrictView.setDistrictListener(districtListener);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        Intent intent = getIntent();
        provinces = (ProvincesBean) intent.getSerializableExtra("provinces");
        city = (CityBean) intent.getSerializableExtra("city");
        district = (DistrictBean) intent.getSerializableExtra("district");

        getPresenter().getProvinces();
        if (null != provinces) {
            dropDownListView.setEditText(provinces.provinceName);
            getPresenter().getCity(provinces.provinceId);
        }
        if (null != city) {
            dropDownCityView.setEditText(city.cityName);
            getPresenter().getDistrict(city.cityId);
        }
        if (null != district) {
            dropDownDistrictView.setEditText(district.districtName);
        }
    }

    private XCDropDownListView.SelectProvincesListener provincesListener =
            new XCDropDownListView.SelectProvincesListener() {
                @Override
                public void onProvincesItem(ProvincesBean provincesBean) {
                    provinces = provincesBean;
                    getPresenter().getCity(provincesBean.provinceId);
                }
            };

    private XCDropCityListView.SelectCityListener cityListener =
            new XCDropCityListView.SelectCityListener() {
                @Override
                public void onCityItem(CityBean cityBean) {
                    city = cityBean;
                    getPresenter().getDistrict(cityBean.cityId);
                }
            };

    private XCDropDistrictListView.SelectDistrictListener districtListener =
            new XCDropDistrictListView.SelectDistrictListener() {
                @Override
                public void onDistrictItem(DistrictBean districtBean) {
                    district = districtBean;
                }
            };


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_back:
                finish();
                break;
            case R.id.tv_select_city_ok:
                if (null == provinces) {
                    ToastUtils.showShortToastSafe("请选择省份");
                    return;
                } else if (null == city) {
                    ToastUtils.showShortToastSafe("请选择城市");
                    return;
                } else if (null == district) {
                    ToastUtils.showShortToastSafe("请选择区/县/市");
                    return;
                }
                EventBus.getDefault().post(new CitySelectEvent(provinces, city, district));
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onProvincesData(List<ProvincesBean> provincesBeanList) {
        dropDownListView.setItemsData(provincesBeanList);
    }

    @Override
    public void onCityData(List<CityBean> provincesBeanList) {
        dropDownCityView.setItemsData(provincesBeanList);
    }

    @Override
    public void onDistrictData(List<DistrictBean> provincesBeanList) {
        dropDownDistrictView.setItemsData(provincesBeanList);
    }

    @Override
    public void onFail(int type) {

    }
}
