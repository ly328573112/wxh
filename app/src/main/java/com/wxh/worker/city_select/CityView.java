package com.wxh.worker.city_select;

import com.utilCode.base.mvp.MvpView;
import com.wxh.worker.city_select.bean.CityBean;
import com.wxh.worker.city_select.bean.DistrictBean;
import com.wxh.worker.city_select.bean.ProvincesBean;

import java.util.List;

public interface CityView extends MvpView {

    void onProvincesData(List<ProvincesBean> provincesBeanList);

    void onCityData(List<CityBean> provincesBeanList);

    void onDistrictData(List<DistrictBean> provincesBeanList);

    void onFail(int type);

}
