package com.wxh.worker.city_select.bean;

import android.support.annotation.Keep;

import java.io.Serializable;

@Keep
public class CityBean implements Serializable {

    public String cityId;
    public String provinceId;
    public String cityName;
    public String zipcode;
    public String sort;
}
