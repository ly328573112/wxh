package com.wxh.worker.city_select;

import android.support.annotation.NonNull;

import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.utilCode.base.mvp.RxMvpPresenter;
import com.utilCode.utils.RxUtils;
import com.wxh.worker.city_select.bean.CityBean;
import com.wxh.worker.city_select.bean.DistrictBean;
import com.wxh.worker.city_select.bean.ProvincesBean;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CityPresent extends RxMvpPresenter<CityView> {

    protected MyApi mMyApi;

    @Inject
    public CityPresent(MyApi myApi) {
        mMyApi = myApi;
    }

    public void getProvinces(){
        mMyApi.getProvinces()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<List<ProvincesBean>>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<ProvincesBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<ProvincesBean>> data) {
                        if (checkJsonCode(data, false)) {
                            getView().onProvincesData(data.getResult());
                        }  else {
                            getView().onFail(1);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(1);
                    }
                });
    }

    public void getCity(String provinceId){
        mMyApi.getCity(provinceId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<List<CityBean>>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<CityBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<CityBean>> data) {
                        if (checkJsonCode(data, false)) {
                            getView().onCityData(data.getResult());
                        }  else {
                            getView().onFail(2);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(2);
                    }
                });
    }

    public void getDistrict(String cityId){
        mMyApi.getDistrict(cityId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.<Data<List<DistrictBean>>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<DistrictBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<DistrictBean>> data) {
                        if (checkJsonCode(data, false)) {
                            getView().onDistrictData(data.getResult());
                        }  else {
                            getView().onFail(3);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(3);
                    }
                });
    }

}
