package com.wxh.worker.city_select.bean;

import android.support.annotation.Keep;

import java.io.Serializable;

@Keep
public class DistrictBean implements Serializable {

    public String districtId;
    public String cityId;
    public String districtName;
    public String sort;

}
