package com.wxh.worker.city_select.bean;

import android.support.annotation.Keep;

import java.io.Serializable;

@Keep
public class ProvincesBean implements Serializable {

    public String provinceId;
    public String areaId;
    public String provinceName;
    public String sort;
}
