package com.utilCode.utils;

import android.view.View;

import com.trello.rxlifecycle2.android.FragmentEvent;
import com.trello.rxlifecycle2.components.support.RxFragment;
import com.utilCode.base.mvp.MvpView;
import com.trello.rxlifecycle2.LifecycleProvider;
import com.trello.rxlifecycle2.LifecycleTransformer;
import com.trello.rxlifecycle2.android.ActivityEvent;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class RxUtils {
    public static <T> ObservableTransformer<T, T> applySchedulersLifeCycle(final LifecycleProvider lifecycleProvider) {
        return new ObservableTransformer<T, T>() {
            @Override
            public Observable<T> apply(Observable<T> observable) {
                return observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .compose(RxUtils.<T>bindToLifecycle(lifecycleProvider));
            }
        };
    }

    public static <T> ObservableTransformer<T, T> applySchedulersLifeCycle(final MvpView mvpView) {
        return new ObservableTransformer<T, T>() {
            @Override
            public Observable<T> apply(Observable<T> observable) {
                return observable.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .compose(RxUtils.<T>bindToLifecycle((LifecycleProvider) mvpView));
            }
        };
    }

    static <T> LifecycleTransformer<T> bindToLifecycle(LifecycleProvider lifecycleProvider) {
        if (lifecycleProvider instanceof RxFragment) {
            return lifecycleProvider.bindUntilEvent((FragmentEvent.DESTROY));
        } else {
            return lifecycleProvider.bindUntilEvent((ActivityEvent.DESTROY));
        }
    }

    public static Observable<Integer> clickStreamCount(View view) {
        Observable<Integer> clickStreamObservable = Observable.create(new ClickStream(view));
        clickStreamObservable.buffer(clickStreamObservable.debounce(600, TimeUnit.MILLISECONDS))
                .map(new Function<List<Integer>, Integer>() {
                    @Override
                    public Integer apply(List<Integer> integers) throws Exception {
                        return integers.size();
                    }
                })
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer integer) throws Exception {
                        Timber.e("integer = " + integer);
                    }
                });
        return clickStreamObservable;
    }

    public static class ClickStream implements ObservableOnSubscribe<Integer> {

        private View mView;

        public ClickStream(View view) {
            mView = view;
        }

        @Override
        public void subscribe(final ObservableEmitter<Integer> emitter) throws Exception {
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    emitter.onNext(1);
                }
            });
        }
    }

}
