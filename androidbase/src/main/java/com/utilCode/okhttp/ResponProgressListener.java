package com.utilCode.okhttp;

public interface ResponProgressListener {

    void onResponseProgress(long bytesRead, long contentLength, boolean done);
}
