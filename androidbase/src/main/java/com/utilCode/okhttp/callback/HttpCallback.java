package com.utilCode.okhttp.callback;

import com.utilCode.okhttp.RequestProgressListener;
import com.utilCode.okhttp.ResponProgressListener;

public interface HttpCallback<T> extends RequestProgressListener, ResponProgressListener {

    void handleResponse(T response);

    boolean handleException(Throwable throwable);
}
