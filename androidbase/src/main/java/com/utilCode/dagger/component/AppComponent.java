package com.utilCode.dagger.component;

import android.content.Context;

import com.utilCode.dagger.help.OkhttpHelper;
import com.utilCode.dagger.help.PreferencesHelper;
import com.utilCode.dagger.help.RetrofitHelper;
import com.utilCode.dagger.module.AppModule;
import com.utilCode.dagger.qualifier.ApplicationContext;
import com.utilCode.dagger.qualifier.GlideCache;
import com.utilCode.dagger.qualifier.OkhttpCache;

import java.io.File;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    @ApplicationContext
    Context getContext();

    File getCacheDir();

    @OkhttpCache
    File getOkhttpCacheDir();

    @GlideCache
    File getGlideCacheDir();

    OkhttpHelper okhttpHelper();

    RetrofitHelper RetrofitHelper();

    PreferencesHelper preferencesHelper();

}
