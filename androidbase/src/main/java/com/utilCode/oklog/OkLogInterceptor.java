package com.utilCode.oklog;

import com.utilCode.oklog.BaseLogDataInterceptor.RequestLogData;
import com.utilCode.oklog.BaseLogDataInterceptor.ResponseLogData;
import com.utilCode.room.DebugInfo;
import com.utilCode.room.DebugInfoDataSource;
import com.utilCode.utils.AndroidUtils;
import com.utilCode.utils.DateUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class OkLogInterceptor implements Interceptor {

    private final LogDataInterceptor logDataInterceptor;

    public OkLogInterceptor() {
        this.logDataInterceptor = new LogDataInterceptor();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        RequestLogData<Request> requestLogData = logDataInterceptor.processRequest(chain);
        LogDataBuilder logDataBuilder = requestLogData.getLogData();
        DebugInfo debugInfo = new DebugInfo();
        long startNs = System.nanoTime();
        Response response;
        try {
            response = chain.proceed(requestLogData.getRequest());
        } catch (Exception e) {
            logDataBuilder.requestFailed();
            /*------respone error--------*/
            debugInfo.setDebugtype("network");
            debugInfo.setDebuginfo(logDataBuilder.toString());
            debugInfo.setDebugtime(DateUtils.getCurrentDate(DateUtils.dateFormatYMDHMS));
            DebugInfoDataSource.getInstance(AndroidUtils.getContext())
                    .insertOrUpdateDebugInfo(debugInfo);
            throw e;
        }
        long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);
        logDataBuilder.responseDurationMs(tookMs);

        ResponseLogData<Response> responseLogData = logDataInterceptor.processResponse(logDataBuilder, response);
        /*------respone success--------*/
        debugInfo.setDebugtype("network");
        debugInfo.setDebuginfo(logDataBuilder.toString());
        debugInfo.setDebugtime(DateUtils.getCurrentDate(DateUtils.dateFormatYMDHMS));
        DebugInfoDataSource.getInstance(AndroidUtils.getContext())
                .insertOrUpdateDebugInfo(debugInfo);
        return responseLogData.getResponse();
    }

}
