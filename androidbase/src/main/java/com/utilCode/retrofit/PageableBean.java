package com.utilCode.retrofit;

import android.support.annotation.Keep;

import java.util.List;

@Keep
public class PageableBean<T> {
    public int first;
    public int next;
    public int prev;
    public int last;
    public int totalCount;
    public List<T> items;
}
