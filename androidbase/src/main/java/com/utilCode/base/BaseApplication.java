package com.utilCode.base;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;

import com.utilCode.BuildConfig;
import com.utilCode.dagger.component.AppComponent;
import com.utilCode.dagger.component.DaggerAppComponent;
import com.utilCode.dagger.module.AppModule;
import com.utilCode.download.DownloadConfiguration;
import com.utilCode.download.DownloadManager;
import com.utilCode.logger.ReleaseTree;
import com.utilCode.logger.ThreadAwareDebugTree;
import com.utilCode.utils.AndroidUtils;
import com.facebook.stetho.Stetho;
import com.orhanobut.hawk.Hawk;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.tencent.smtt.sdk.QbSdk;

import timber.log.Timber;

public class BaseApplication extends Application {

    private static AppComponent mAppComponent;
    private static BaseAppManager mAppManager;
    private static RefWatcher mRefWatcher;

    @Override
    public void onCreate() {
        super.onCreate();

        AndroidUtils.init(this);

        initAppInject(this);

        initDownloader(this);

        Hawk.init(this).build();

        //tbs x5 webview
        QbSdk.initX5Environment(getApplicationContext(), new QbSdk.PreInitCallback() {
            @Override
            public void onCoreInitFinished() {

            }

            @Override
            public void onViewInitFinished(boolean b) {
            }
        });

        if (BuildConfig.DEBUG) {
            mRefWatcher = LeakCanary.install(this);

            Stetho.initializeWithDefaults(this);

            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .penaltyLog()
                    .build());

            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build());

        }

        if (BuildConfig.DEBUG) {
            Timber.plant(new ThreadAwareDebugTree());
        } else {
            Timber.plant(new ReleaseTree());
        }
    }

    public static RefWatcher getRefWatcher(Context context) {
        // LeakCanary: Detect all memory leaks!
        // LeakCanary.install() returns a pre configured RefWatcher. It also
        // installs an ActivityRefWatcher that automatically detects if an activity is
        // leaking after Activity.onDestroy() has been called.
        return mRefWatcher;
    }

    private static void initDownloader(Context context) {
        DownloadConfiguration configuration = new DownloadConfiguration();
        configuration.setMaxThreadNum(6);
        DownloadManager.getInstance().init(context, configuration);
    }

    public static BaseAppManager getAppManager() {
        if (mAppManager == null) {
            synchronized (BaseApplication.class) {
                if (mAppManager == null) {
                    mAppManager = BaseAppManager.getAppManager();
                }
            }
        }
        return mAppManager;
    }

    public void initAppInject(Context context) {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule((Application) context))
                .build();
    }

    public static AppComponent getAppComponent() {
        return mAppComponent;
    }

    @Override
    public void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        // you must install multiDex whatever tinker is installed!
        MultiDex.install(base);
    }

    public static void finishAllActivity() {
        mAppManager.finishAllActivity();
    }

    public static void exitApp() {
        mAppManager.AppExit();
    }

    public static void restartApp(@NonNull Activity activity) {
        Intent intent = activity.getPackageManager()
                .getLaunchIntentForPackage(activity.getPackageName());
        Class<? extends Activity> resolvedActivityClass;
        if (intent != null && intent.getComponent() != null) {
            try {
                resolvedActivityClass = (Class<? extends Activity>) Class.forName(intent.getComponent()
                        .getClassName());
                intent.setClass(activity, resolvedActivityClass);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                if (intent.getComponent() != null) {
                    //If the class name has been set, we force it to simulate a Launcher launch.
                    //If we don't do this, if you restart from the error activity, then press home,
                    //and then launch the activity from the launcher, the main activity appears twice on the backstack.
                    //This will most likely not have any detrimental effect because if you set the Intent component,
                    //if will always be launched regardless of the actions specified here.
                    intent.setAction(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_LAUNCHER);
                }
                activity.finish();
                activity.startActivity(intent);
                exitApp();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                //Should not happen, print it to the log!
            }
        }

    }

}
