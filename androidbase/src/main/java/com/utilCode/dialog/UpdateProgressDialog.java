package com.utilCode.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.utilCode.R;
import com.utilCode.base.BaseDialog;

public class UpdateProgressDialog extends BaseDialog {

    LinearLayout ll_progress_id;
    TextView tv_progress_text;
    TextView tv_progress_number;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.customDialogTheme);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_progress_layout;
    }

    @Override
    protected void setupView(View rootView) {
        ll_progress_id = rootView.findViewById(R.id.ll_progress_id);
        tv_progress_text = rootView.findViewById(R.id.tv_progress_text);
        tv_progress_number = rootView.findViewById(R.id.tv_progress_number);

        getDialog().setCanceledOnTouchOutside(false);
        setCancelable(true);
    }

    public void setProgress(int progress) {
        tv_progress_text.setText("下载中(" + progress + "%)");
        ViewGroup.LayoutParams lp = tv_progress_number.getLayoutParams();
        lp.width = ll_progress_id.getWidth() * progress / 100;
        tv_progress_number.requestLayout();
    }

}
